// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "Plotter.h"
#include "Types.h"

#include "GradientDescent.h"

VanillaGradientDescent::VanillaGradientDescent( Function const &f,
                                                FunctionGradient const &gradf,
                                                Coordinate const &coords,
                                                double step_size, double gtol,
                                                size_t max_iterations )
    : f_( f )
    , gradf_( gradf )
    , coords_( coords )
    , step_size_( step_size )
    , gtol_( gtol )
    , max_iterations_( max_iterations )
{
}

CoordinateArray VanillaGradientDescent::run()
{
    CoordinateArray steps_taken{ coords_ };

    Coordinate grad = getGradient();
    double g_l2norm2 = 0;
    for ( size_t idx = 0; idx < grad.size(); ++idx )
        g_l2norm2 += grad[ idx ] * grad[ idx ];

    bool is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
    size_t num_iterations = 0ul;

    while ( !is_converged && num_iterations < max_iterations_ )
    {
        num_iterations++;

        // Update the coordinates given the current gradient information.
        for ( size_t idx = 0; idx < coords_.size(); ++idx )
            coords_[ idx ] -= step_size_ * grad[ idx ];
        steps_taken.push_back( coords_ );

        // Compute the gradient at the updated position. Also check if
        // the gradient is small enough to stop the iterations
        grad = getGradient();
        g_l2norm2 = 0;
        for ( size_t idx = 0; idx < grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];
        is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
    }

    if ( is_converged )
    {
        std::cout << "Converged after " << num_iterations << " iterations\n";
    }
    else
    {
        std::cout << "Failed to converge after " << max_iterations_
                  << " iterations\n";
    }

    return steps_taken;
}

double VanillaGradientDescent::stepSize() const noexcept { return step_size_; }
void VanillaGradientDescent::stepSize( double step_size )
{
    step_size_ = step_size;
}

Coordinate const &VanillaGradientDescent::coordinates() const
{
    return coords_;
}
void VanillaGradientDescent::coordinates( Coordinate const &coords )
{
    assert( coords.size() == coords_.size() );
    coords_ = coords; // value semantics
};
Coordinate VanillaGradientDescent::getGradient() const
{
    return gradf_( coords_ );
}

MomentumGradientDescent::MomentumGradientDescent(
    Function const &f, FunctionGradient const &gradf, Coordinate const &coords,
    double step_size, double gtol, size_t max_iterations, double alpha )
    /*Common parameters*/
    : f_( f )
    , gradf_( gradf )
    , coords_( coords )
    , step_size_( step_size )
    , gtol_( gtol )
    , max_iterations_( max_iterations )
    /*Momentum specific*/
    , alpha_( alpha )
    , momentum_( coords.size(), 0 )
{
}

CoordinateArray MomentumGradientDescent::run()
{
    CoordinateArray steps_taken{ coords_ };

    Coordinate grad = getGradient();
    double g_l2norm2 = 0;
    for ( size_t idx = 0; idx < grad.size(); ++idx )
        g_l2norm2 += grad[ idx ] * grad[ idx ];

    bool is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
    size_t num_iterations = 0;

    while ( !is_converged && num_iterations < max_iterations_ )
    {
        num_iterations++;

        // Update the coordinates given the current gradient
        // information.
        for ( size_t idx = 0; idx < coords_.size(); ++idx )
        {
            // https://en.wikipedia.org/wiki/Stochastic_gradient_descent#Momentum
            double const delta =
                momentum_[ idx ] * alpha_ - step_size_ * grad[ idx ];
            coords_[ idx ] += delta;
            momentum_[ idx ] = delta;
        }
        steps_taken.push_back( coords_ );

        // Compute the gradient at the updated position. Also check if
        // the gradient is small enough to stop the iterations.
        grad = getGradient();
        g_l2norm2 = 0;
        for ( size_t idx = 0; idx < grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];
        is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
    }

    if ( is_converged )
    {
        std::cout << "Converged after " << num_iterations << " iterations\n";
    }
    else
    {
        std::cout << "Failed to converge after " << max_iterations_
                  << " iterations\n";
    }

    return steps_taken;
}

double MomentumGradientDescent::stepSize() const noexcept { return step_size_; }
void MomentumGradientDescent::stepSize( double step_size )
{
    step_size_ = step_size;
}

Coordinate const &MomentumGradientDescent::coordinates() const
{
    return coords_;
}
void MomentumGradientDescent::coordinates( Coordinate const &coords )
{
    assert( coords.size() == coords_.size() );
    coords_ = coords; // value semantics
};

Coordinate MomentumGradientDescent::getGradient() const
{
    return gradf_( coords_ );
}