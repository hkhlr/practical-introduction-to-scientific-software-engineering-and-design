// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "Plotter.h"
#include "Types.h"

#ifndef NO_VISUALIZATION

#include <matplotlibcpp17/animation.h>
#include <matplotlibcpp17/cm.h>
#include <matplotlibcpp17/mplot3d.h>
#include <matplotlibcpp17/pyplot.h>

class MatplotlibPlotter : public Plotter
{
  public:
    static MatplotlibPlotter *getPlotter();

    // Singelton Pattern: singeltons are shared by designs, so no need for smart
    // pointers.
    static MatplotlibPlotter *instance_pointer;

    // Singelton pattern: singeltons are NOT copy-able or assignable
    MatplotlibPlotter( MatplotlibPlotter &other ) = delete;
    void operator=( const MatplotlibPlotter & ) = delete;

  private:
    // singelton pattern: constructor is private!
    MatplotlibPlotter( std::vector< double > const &lower_limits,
                       std::vector< double > const &upper_limits,
                       double plotting_step_size,
                       pybind11::scoped_interpreter python_guard,
                       matplotlibcpp17::pyplot::PyPlot plt,
                       matplotlibcpp17::figure::Figure fig,
                       matplotlibcpp17::axes::Axes ax )
        : lower_limits_( std::move( lower_limits ) )
        , upper_limits_( std::move( upper_limits ) )
        , plotting_step_size_( plotting_step_size )
        , python_guard_( std::move( python_guard ) )
        , plt_( std::move( plt ) )
        , fig_( std::move( fig ) )
        , ax_( std::move( ax ) )
    {
    }

  public:
    void plotGradientDescent( Function const &f,
                              const CoordinateArray &steps_taken ) override;

  private:
    // settings on the area to plot
    const std::vector< double > lower_limits_;
    const std::vector< double > upper_limits_;
    const double plotting_step_size_;

    // matplotlib objs
    pybind11::scoped_interpreter python_guard_;
    matplotlibcpp17::pyplot::PyPlot plt_;
    matplotlibcpp17::figure::Figure fig_;
    matplotlibcpp17::axes::Axes ax_;

    void plotFunction( Function const &f );

    matplotlibcpp17::animation::ArtistAnimation
    plotPathTaken( Function const &f, const CoordinateArray &steps_taken );
};

void MatplotlibPlotter::plotGradientDescent(
    Function const &f, const CoordinateArray &steps_taken )
{
    // reset matplotlibs output
    // ax_.cla();
    // TODO documentation say .cla it is there, compiler not :-(
    plotFunction( f );
    auto animation = plotPathTaken( f, steps_taken );
    plt_.show();
}

void MatplotlibPlotter::plotFunction( Function const &f )
{
    // plot the function surface
    std::vector< std::vector< double > > X;
    std::vector< std::vector< double > > Y;
    std::vector< std::vector< double > > Z;

    // calculate function points to visualize in a grid
    for ( int i = 0; i < std::abs( lower_limits_[ 0 ] - upper_limits_[ 0 ] ) /
                             plotting_step_size_;
          ++i )
    {
        X.push_back( {} );
        Y.push_back( {} );
        Z.push_back( {} );
        for ( int j = 0;
              j < std::abs( lower_limits_[ 1 ] - upper_limits_[ 1 ] ) /
                      plotting_step_size_;
              ++j )
        {
            double const x = lower_limits_[ 0 ] + i * plotting_step_size_;
            double const y = lower_limits_[ 1 ] + j * plotting_step_size_;
            double const z = f( { x, y } );

            X[ i ].push_back( x );
            Y[ i ].push_back( y );
            Z[ i ].push_back( z );
        }
    }
    // to numpy array
    const auto X_ = py::array( py::cast( std::move( X ) ) );
    const auto Y_ = py::array( py::cast( std::move( Y ) ) );
    const auto Z_ = py::array( py::cast( std::move( Z ) ) );
    // the actual plotting:
    const auto surf = ax_.plot_surface(
        Args( X_, Y_, Z_ ),
        Kwargs( "rstride"_a = 1, "cstride"_a = 1, "linewidth"_a = 0,
                "antialiased"_a = true, "alpha"_a = 0.75,
                "cmap"_a = matplotlibcpp17::cm::coolwarm ) );
    fig_.colorbar( Args( surf.unwrap() ),
                   Kwargs( "shrink"_a = 0.5, "aspect"_a = 5 ) );
}

matplotlibcpp17::animation::ArtistAnimation
MatplotlibPlotter::plotPathTaken( Function const &f,
                                  const CoordinateArray &steps_taken )
{

    // list of the steps to animate
    py::list artist_list;

    Coordinate current_pos = steps_taken[ 0 ];
    // 2D Functions Only
    assert( current_pos.size() == 2 );

    for ( auto const &step : steps_taken )
    {
        std::vector< double > const x{ current_pos[ 0 ] /*before*/,
                                       step[ 0 ] /*after*/ };
        std::vector< double > const y{ current_pos[ 1 ] /*before*/,
                                       step[ 1 ] /*after*/ };
        std::vector< double > const z{ f( current_pos ) /*before*/,
                                       f( step ) /*after*/ };

        // visualize this step and add it to the list of steps to animate
        ax_.plot( Args( x, y, z ),
                  Kwargs( "color"_a = "black", "antialiased"_a = true ) );
        artist_list.append( ax_.get_lines().unwrap() );

        current_pos = step;
    }

    // animate the path taken
    return matplotlibcpp17::animation::ArtistAnimation(
        Args( fig_.unwrap(), artist_list ), Kwargs( "interval"_a = 100 ) );
}

MatplotlibPlotter *MatplotlibPlotter::instance_pointer = nullptr;

MatplotlibPlotter *MatplotlibPlotter::getPlotter()
{
    if ( instance_pointer == nullptr )
    {
        std::vector< double > const lower_limits = {
            -1.25 /*x*/,
            -1.25 /*y*/,
            0 /*z*/,
        };
        std::vector< double > const upper_limits = {
            3.25,
            3.25,
            10,
        };
        double const plotting_step_size = 0.1;

        pybind11::scoped_interpreter python_guard{};
        auto plt = matplotlibcpp17::pyplot::import();
        // this is required for "projection = 3d"
        matplotlibcpp17::mplot3d::import();
        auto fig = plt.figure();
        auto ax = fig.add_subplot( py::make_tuple(),
                                   Kwargs( "projection"_a = "3d" ) );

        instance_pointer = new MatplotlibPlotter(
            lower_limits, upper_limits, plotting_step_size,
            std::move( python_guard ), std::move( plt ), std::move( fig ),
            std::move( ax ) );
    }

    return instance_pointer;
}

Plotter *plotterFactory() { return MatplotlibPlotter::getPlotter(); }

#endif
