// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#pragma once

#include "Types.h"

class Plotter
{
  public:
    virtual void plotGradientDescent( Function const &f,
                                      const CoordinateArray &steps_taken ) = 0;
};

// the Factory method creates a Plotter and hides all the plotters Internals
//
#ifndef NO_VISUALIZATION
Plotter *plotterFactory();
#else
Plotter *plotterFactory() { return nullptr; }
#endif
