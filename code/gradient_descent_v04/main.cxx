// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "Types.h"

#include "GradientDescent.h"
#include "Plotter.h"

int main( int argc, char *argv[] )
{
    // the function to minimize
    auto f = []( Coordinate const &coords ) -> double
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( Coordinate const &coords ) -> Coordinate
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    Coordinate starting_pos = { -1.0, 2.50 };
    double const step_size = 3e-1;

    // Command line argument to decide which version of gradient decent to
    // use.
    int const implementation_to_use = ( argc > 1 ) ? std::stoi( argv[ 1 ] ) : 0;

    std::unique_ptr< GradientDescent > gd;
    switch ( implementation_to_use )
    {
    case 1:
        gd = std::make_unique< MomentumGradientDescent >(
            f, grad_f, starting_pos, step_size );
        break;

    case 0:
    default:
        gd = std::make_unique< VanillaGradientDescent >(
            f, grad_f, starting_pos, step_size );
        break;
    }

    auto steps_taken = gd->run();

    std::cout << "Final coordinates: x = " << gd->coordinates()[ 0 ]
              << ", y = " << gd->coordinates()[ 1 ] << "\n";

    Plotter *plotter{ plotterFactory() };
    if ( plotter != nullptr )
    {
        plotter->plotGradientDescent( f, steps_taken );
    }

    return 0;
}
