<!--
SPDX-FileCopyrightText: © 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Development steps

## Overall Goals

### Primary

* Extend the code with a new gradient descent variant
* Maintain correctness of existing variants

### Secondary

* Refactor code to remove code redundancy
* Improve readability
* Establish (automated) tests for existing parts of the code.

## What have we accomplished so far?

### [`gradient_descent_v01`](../gradient_descent_v01)
* Placed code under version control.
* Add basic black-box tests to make sure final result is correct.
   * Refer to [`tests`](./tests/) directory.

### [`gradient_descent_v02`](../gradient_descent_v02)
* Dedicated file [`main.cxx`](./main.cxx) that contains driver code.
* Source ([`GradientDescent.cxx`](./GradientDescent.cxx)) and header file ([`GradientDescent.h`](./GradientDescent.h)) for GD code.
* Type aliases in separate header file ([`Types.h`](./Types.h)).

### [`gradient_descent_v03`](../gradient_descent_v03)

* Remove plotting functionality *only* from vanilla GD code.
    * Inspect the [`GradientDescent.cxx`](./GradientDescent.cxx) source file to convince yourself that it still is present inside the `run` method of the `MomentumGradientDescent` class.
* Move code for plotting into separate source and header file.
    * Source file: [`Plotter.cxx`](./Plotter.cxx)
    * Header file: [`Plotter.h`](./Plotter.h)
* Driver code inside `main` function will does (not yet) use the plotting code.

### [`gradient_descent_v04`](../gradient_descent_v04/)

* Remove plotting code also for momentum GD.
* Make the driver code in `main` also use the *externalised* plotting functionality.

## What's next?

Now that we have the plotting stuff out of our way it is time to refactor the GD code. Currently, in particular the `run` methods of both concrete classes still suffer from *lots of duplicate code*:

* It contains the code for running the GD iterations.
    ```cpp
    while ( !is_converged && num_iterations < max_iterations ) { /*...*/ }
    ```
* It contains the code for making the coordinate update specific to the particular GD flavour.
    ```cpp
    for ( size_t idx = 0; idx < coords_.size(); ++idx ) { /*...*/ }
    ```
* The code for computing the $\ell_2$-norm of the gradient is repetetive.
    * Can we leverage a C++ lambda expression here? &#129300;
    ```cpp
    for ( size_t idx = 0; idx < grad.size(); ++idx ) { /*...*/ }
    ```

More formally, our code lacks clear separation of responsibilties: The "Single-Responsibility-Principle" (SRP) is violated in many places. 

How can we remedy this issue?

Well, we can start with the observation that the GD flavours differ in the way how the actual coordinate update is made. As we have seen above, the code for running the actual iterations --- i.e., looping until we have reached a minimum or have exceeded the number of allowed iterations --- is *always the same*. 

Therefore, lets remove the class hierachy for the GD code and introduce a *free function* that  for running the GD iterations. The function expects quite a few parameters as input: e.g., the number of allowed iterations, the tolerance value for the $\ell_2$-norm of the gradient, etc. 

```cpp
CoordinateArray run_gradient_descent( /*...*/ );
```

At least for the moment, we make the function return an array with *all* steps taken during the iterations. We will discuss different choices that can be made in these regards later (refer to the [`supplement`](../supplement/) directory).

Further, we introduce an abstraction for an "updater": 

```cpp
class CoordinateUpdater {
public:
    virtual ~CoordinateUpdater() noexcept = default;
    virtual void update_coordinates( Coordinate &, Coordinate const& ) = 0;
};
```

*Concrete* updaters inherit from this abstract base class and implement the coordinate update as needed for the particular flavour. They also store the required member variables and provide a corresponding ctor.

```cpp
class VanillaUpdater final : public CoordinateUpdater {
public:
    /*...*/
    void update_coordinates( Coordinate&, Coordinate const& ) override;
    /*...*/
private:
    /*member variables go here*/
};

class MomentumUpdater final : public CoordinateUpdater {
public:
    /*...*/
    void update_coordinates( Coordinate&, Coordinate const& ) override;
    /*...*/
private:
    /*member variables go here*/
};
```

The `run_gradient_descent` function expects an `CoordinateUpdater` as argument in the ctor, e.g. via an `std::unique_ptr< CoordinateUpdater >`:

```cpp
CoordinateArray 
run_gradient_descent( /*...*/ std::unique_ptr< CoordinateUpdater > updater /*...*/ );
```

Inside this function the updater's `update_coordinate` method is called to change the coordinates based on the current gradient information.

Buy why does the updater have to be a class?

The answer simply is: Because some updaters have to be "stateful", i.e., most updaters --- actually all but that making a vanilla-type update --- have to maintain some inner state during the iterations. As an example, the momentum updater has to update the `momentum_` variable to make the next coordinate  update.