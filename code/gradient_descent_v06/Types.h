// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#ifndef GRADIENTDESCENT_OBSERVER_TYPES_H_
#define GRADIENTDESCENT_OBSERVER_TYPES_H_

#include <functional>
#include <vector>

using Coordinate = std::vector< double >;
using CoordinateArray = std::vector< Coordinate >;
using Function = std::function< double( Coordinate const & ) >;
using FunctionGradient = std::function< Coordinate( Coordinate const & ) >;

#endif
