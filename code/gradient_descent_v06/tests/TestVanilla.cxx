// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include "catch2/generators/catch_generators.hpp"
#include <catch2/catch_test_macros.hpp>

#include <limits> // std::numeric_limits
#include <memory>

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

/*
Check for closeness of values instead of exact equality due to rounding errors.
*/
constexpr double double_machine_epsilon{
    std::numeric_limits< double >::epsilon() };

bool isClose( double value, // Value to be tested for closeness
              double ref,   // Reference value
              double atol = 10 * double_machine_epsilon, double rtol = 1e-8 )
{
    return ( std::abs( ref - value ) < atol + rtol * std::abs( ref ) );
}
bool coordinatesAreClose( Coordinate const &coord, Coordinate const &coord_ref,
                          double atol = 10.0 * double_machine_epsilon,
                          double rtol = 1e-8 )
{
    return ( isClose( coord[ 0 ], coord_ref[ 0 ], atol, rtol ) &&
             isClose( coord[ 1 ], coord_ref[ 1 ], atol, rtol ) );
}

/*
Some test functions for which the correcte minimum shall be found.
*/
Function function0{ []( Coordinate const &c ) -> double {
    return c[ 0 ] * c[ 0 ] + c[ 1 ] * c[ 1 ];
} }; /* x ** 2 + y ** 2 */
FunctionGradient function0_gradient{ []( Coordinate const &c ) -> Coordinate {
    return { 2 * c[ 0 ], 2 * c[ 1 ] };
} }; /* { 2 * x, 2 * y } */

Function function1{ []( Coordinate const &c ) -> double {
    return ( c[ 0 ] - 2 ) * ( c[ 0 ] - 2 ) + ( c[ 1 ] - 4 ) * ( c[ 1 ] - 4 );
} };
FunctionGradient function1_gradient{ []( Coordinate const &c ) -> Coordinate {
    return { 2 * ( c[ 0 ] - 2 ), 4 * ( c[ 1 ] - 4 ) };
} };

// Booth function
// (https://en.wikipedia.org/wiki/Test_functions_for_optimization)
Function function2{ []( Coordinate const &c ) -> double
                    {
                        // ( x + 2 * y - 7 ) ** 2 + ( 2 x + y - 5 ) ** 2
                        double const term_left{ c[ 0 ] + 2 * c[ 1 ] - 7 };
                        double const term_right{ 2 * c[ 0 ] + c[ 1 ] - 5 };
                        return term_left * term_left + term_right * term_right;
                    } };
FunctionGradient function2_gradient{
    []( Coordinate const &c ) -> Coordinate
    {
        // gradient of ((x + 2 y - 7)^2 + (2 x + y - 5)^2) = (10 x + 8 y -
        // 34, 8 x + 10 y - 38)
        return { 10 * c[ 0 ] + 8 * c[ 1 ] - 34, 8 * c[ 0 ] + 10 * c[ 1 ] - 38 };
    } };

TEST_CASE( "Convergence function0", "[vanilla]" )
{
    SECTION( "converged input" )
    {
        auto learning_rate =
            GENERATE( as< double >{}, 1e-3, 5e-3, 1e-2, 5e-2, 1e-1, 5e-1, 1.0 );
        auto gtol = GENERATE( as< double >{}, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6 );
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );

        // put in already the minimum
        Coordinate start_position = { 0.0, 0.0 };
        Coordinate minimum = { 0.0, 0.0 };
        auto result_steps =
            run_gradient_decent( function0, function0_gradient, start_position,
                                 std::move( updater ), gtol, 100 );
        // No iterations!
        REQUIRE( result_steps.size() == 1 );
        REQUIRE( start_position == minimum );
        REQUIRE( result_steps[ 0 ] == minimum );
    }
    SECTION( "single iteration" )
    {
        const double learning_rate = 0.5;
        const double gtol = 1e-3;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );
        auto start_value = GENERATE( as< double >{}, 2, 4, 6, 8, 10 );
        Coordinate start_position = { start_value, start_value };
        Coordinate minimum = { 0.0, 0.0 };
        auto result_steps =
            run_gradient_decent( function0, function0_gradient, start_position,
                                 std::move( updater ), gtol, 100 );
        REQUIRE( result_steps.size() == 2 );
        // only one step to the minimum
        REQUIRE( coordinatesAreClose( start_position, minimum ) );
        REQUIRE( result_steps[ 0 ] == Coordinate{ start_value, start_value } );
        REQUIRE( result_steps[ 1 ] == start_position );
    }

    SECTION( "too few allowed iterations" )
    {
        auto start_position =
            GENERATE( as< Coordinate >{}, Coordinate{ 1, 0 },
                      Coordinate{ 0, 1 }, Coordinate{ 1, 1 } );
        auto max_iter =
            GENERATE( as< unsigned >{}, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 );
        const double learning_rate = 0.25;
        const double gtol = 1e-4;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );

        Coordinate minimum = { 0.0, 0.0 };
        auto result_steps =
            run_gradient_decent( function0, function0_gradient, start_position,
                                 std::move( updater ), gtol, max_iter );
        // not enough to reach minimum
        REQUIRE( result_steps.size() == max_iter + 1 );
        REQUIRE( not coordinatesAreClose( start_position, minimum ) );
    }

    SECTION( "finds minimum" )
    {
        auto start_position =
            GENERATE( as< Coordinate >{}, Coordinate{ 1, 0 },
                      Coordinate{ 0, 1 }, Coordinate{ 1, 1 } );
        const unsigned max_iter = 5000;
        const double learning_rate = 0.1;
        const double gtol = 1e-4;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );

        Coordinate minimum = { 0.0, 0.0 };
        auto result_steps =
            run_gradient_decent( function0, function0_gradient, start_position,
                                 std::move( updater ), gtol, max_iter );
        // enough to reach minimum
        REQUIRE( result_steps.size() < max_iter + 1 );
        // tolerance for test should be the same that was given to the algorithm
        // to determine if one should stop
        REQUIRE( coordinatesAreClose( start_position, minimum, 1e-4, 1e-4 ) );
    }
}

TEST_CASE( "Convergence function1", "[vanilla]" )
{
    SECTION( "finds minimum" )
    {
        auto start_position = GENERATE(
            as< Coordinate >{}, Coordinate{ 2, 4 }, Coordinate{ 1, 4 },
            Coordinate{ 2, 3 }, Coordinate{ 1, 3 }, Coordinate{ 3, 4 },
            Coordinate{ 2, 5 }, Coordinate{ 3, 5 } );
        const unsigned max_iter = 5000;
        const double learning_rate = 0.1;
        const double gtol = 1e-4;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );

        Coordinate minimum = { 2.0, 4.0 };
        auto result_steps =
            run_gradient_decent( function1, function1_gradient, start_position,
                                 std::move( updater ), gtol, max_iter );
        // enough to reach minimum
        REQUIRE( result_steps.size() < max_iter + 1 );
        // tolerance for test should be the same that was given to the algorithm
        // to determine if one should stop
        REQUIRE( coordinatesAreClose( start_position, minimum, 1e-4, 1e-4 ) );
    }
}
TEST_CASE( "Convergence function2", "[vanilla]" )
{
    SECTION( "finds minimum" )
    {
        auto start_position = GENERATE(
            as< Coordinate >{}, Coordinate{ 1, 3 }, Coordinate{ .0, 3 },
            Coordinate{ 1, 2 }, Coordinate{ .0, 2 }, Coordinate{ 2, 3 },
            Coordinate{ 1, 4 }, Coordinate{ 2, 4 } );
        const unsigned max_iter = 5000;
        const double learning_rate = 0.1;
        const double gtol = 1e-4;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );

        Coordinate minimum = { 1.0, 3.0 };
        auto result_steps =
            run_gradient_decent( function2, function2_gradient, start_position,
                                 std::move( updater ), gtol, max_iter );
        // enough to reach minimum
        REQUIRE( result_steps.size() < max_iter + 1 );
        // tolerance for test should be the same that was given to the algorithm
        // to determine if one should stop
        REQUIRE( coordinatesAreClose( start_position, minimum, 1e-4, 1e-4 ) );
    }
}

TEST_CASE( "Updater", "[vanilla]" )
{

    Coordinate position = { 0.0, 0.0 };
    Coordinate gradient = { -1.0, -1.0 };

    auto step_size =
        GENERATE( as< double >{}, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2 );

    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< VanillaUpdater >( step_size );
    updater->update_coordinates( position, gradient );
    REQUIRE( ( position[ 0 ] == step_size && position[ 1 ] == step_size ) );
}
