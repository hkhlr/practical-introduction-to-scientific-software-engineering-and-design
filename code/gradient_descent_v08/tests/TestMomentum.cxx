// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include "catch2/generators/catch_generators.hpp"
#include <catch2/catch_test_macros.hpp>

#include <memory>

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

#include "TestInfrastructure.h"

TEST_CASE( "Updater", "[momentum]" )
{
    Coordinate position = { 0.0, 0.0 };
    Coordinate const gradient = { -1.0, -1.0 };

    auto step_size =
        GENERATE( as< double >{}, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2 );
    auto alpha =
        GENERATE( as< double >{}, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 );

    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< MomentumUpdater >( alpha, step_size, 2 );

    /*
    After a single step the updated coordinate is equal to

    -learning_rate * gradient

    */
    SECTION( "single iteration" )
    {
        updater->update_coordinates( position, gradient );
        /*
        Generally, avoid comparing floating point numbers by equality (
        operator== )
        */
        REQUIRE( ( position[ 0 ] == step_size && position[ 1 ] == step_size ) );
    }

    // The whole initialisation will be repeated for the other section.
    auto num_iters = GENERATE( as< int >{}, 1, 2, 4, 6, 8, 16, 32, 64 );
    SECTION( "momentum grows" )
    {
        for ( int i = 0; i < num_iters; ++i )
        {
            updater->update_coordinates( position, gradient );
        } // growing the momentum
        auto position_test = position;
        updater->update_coordinates( position, gradient );
        // momentum should build up
        for ( size_t idx = 0; idx < position.size(); ++idx )
        {
            REQUIRE( std::abs( position[ idx ] - position_test[ idx ] ) >
                     step_size );
        }
    }
}

TEST_CASE( "Updater compare", "[momentum]" )
{
    Coordinate position = GENERATE(
        as< Coordinate >{}, Coordinate{ 0.0, 0.0 }, Coordinate{ 1.0, 1.0 },
        Coordinate{ 1.0, 0.0 }, Coordinate{ 0.0, 1.0 } );
    Coordinate position_reference = position;

    Coordinate gradient =
        GENERATE( as< Coordinate >{}, Coordinate{ -1.0, -1.0 },
                  Coordinate{ -1.0, 0.0 }, Coordinate{ 0.0, -1.0 } );

    auto step_size =
        GENERATE( as< double >{}, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2 );

    auto num_iters = GENERATE( as< size_t >{}, 1, 2, 4, 6, 8, 16, 32, 64 );

    SECTION( "same" )
    {
        // special case: Vanilla and momentum should behave same
        double const alpha = 0.0;

        std::unique_ptr< CoordinateUpdater > updater_vanilla =
            std::make_unique< VanillaUpdater >( step_size );
        std::unique_ptr< CoordinateUpdater > updater_momentum =
            std::make_unique< MomentumUpdater >( alpha, step_size, 2 );

        for ( size_t i = 0; i < num_iters; ++i )
        {
            updater_vanilla->update_coordinates( position_reference, gradient );
            updater_momentum->update_coordinates( position, gradient );
            REQUIRE( coordinatesAreClose( position, position_reference ) );
        }
    }

    SECTION( "momentum_grows" )
    {
        auto alpha = GENERATE( as< double >{}, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6,
                               0.7, 0.8, 0.9 );
        /*
        If alpha > 0 and we always go into the same direction momentum should
        build up and we will me "ahead" of the vanilla updater.
        */

        // clang-format off
        /*
        This can be seen as follows:

        The momentum update goes according to the following set of equations

        momentum := alpha * momentum - lr * gradient

        coord    := coord + momentum

        Letting the gradient always be *the same* in every update: gradient =
        gradient_0

        step 1: momentum_0 = -lr * gradient_0
        step 2: momentum_1 = -alpha * lr * gradient_0 - lr * gradient_0
                           = -( alpha + 1 ) * lr * gradient_0
        step 3: momentum_2 = -alpha * ( alpha + 1 ) * lr * gradient_0 - lr * gradient_0 
                           = -( alpha * ( alpha + 1 ) + 1 ) * lr * gradient_0 
        step 4: momentum_3 = - alpha * ( alpha * ( alpha + 1 ) +  1 ) * lr * gradient_0 - lr * gradient_0
                           = -( alpha * ( alpha * ( alpha + 1 ) + 1  ) + 1 ) * lr * gradient_0 
        ...and so forth...

        Assuming "common" values for alpha, e.g. alpha > 0, can see that the term in (...) > 1. As a 
        result the momentum term keeps (monotonically) decreasing.

        Comparing this to pure vanilla GD where the update goes like this:

        coord := coord - lr * gradient

        we can see that --- given the conditions described above --- momentum and vanilla behave like

        coord_momentum := coord_momentum - factor * lr * gradient_0 (momentum)
        coord_vanilla  := coord_vanilla  -          lr * gradient_0 (vanilla)

        where absvalue(factor) > 1. That is what we mean by momentum should be "ahead" of vanilla.
        Note that factor from the derivation above is usually negative. The sign of the whole 
        second term in the coordinate update depends on the sign of the gradient. If the gradient is 
        negative in *all* its component, then sign of the second term will overall be
        positive.

        */
        // clang-format on

        // auto const printCoordinates = []( auto const &c ) -> void
        // { std::cout << "x = " << c.at( 0 ) << ", y = " << c.at( 1 ) << "\n";
        // };

        std::unique_ptr< CoordinateUpdater > updater_vanilla =
            std::make_unique< VanillaUpdater >( step_size );

        std::unique_ptr< CoordinateUpdater > updater_momentum =
            std::make_unique< MomentumUpdater >( alpha, step_size, 2 );

        for ( size_t i = 0; i < num_iters; ++i )
        {
            updater_vanilla->update_coordinates( position_reference, gradient );
            // std::cout << "Vanilla update: ";
            // printCoordinates( position_reference );
            updater_momentum->update_coordinates( position, gradient );
            // std::cout << "Momentum update: ";
            // printCoordinates( position );
            for ( size_t idx = 0; idx < position.size(); ++idx )
            {
                // Either same (close) or the momentum should be in the lead
                REQUIRE(
                    ( isClose( position[ idx ], position_reference[ idx ] ) ||
                      position[ idx ] > position_reference[ idx ] ) );
            }
        }
    }
}

/*
TODO: This code largely duplicates that for the vanilla updater.
      --> Can we do better?
*/

constexpr double alpha = 0.1;

TEST_CASE( "Convergence function0", "[momentum]" )
{
    SECTION( "converged input" )
    {
        auto learning_rate =
            GENERATE( as< double >{}, 1e-3, 5e-3, 1e-2, 5e-2, 1e-1, 5e-1, 1.0 );
        auto gtol = GENERATE( as< double >{}, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6 );
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );

        // put in already the minimum
        Coordinate start_position = { 0.0, 0.0 };
        Coordinate minimum = { 0.0, 0.0 };
        auto result_steps =
            run_gradient_decent( function0, function0_gradient, start_position,
                                 std::move( updater ), gtol, 100 );
        REQUIRE( result_steps.size() == 1 );
        // only the first iteration where we concluded that we found the minimum
        REQUIRE( start_position == minimum );
        REQUIRE( result_steps[ 0 ] == minimum );
    }
    SECTION( "single iteration" )
    {
        const double learning_rate = 0.5;
        const double gtol = 1e-3;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );
        auto start_value = GENERATE( as< double >{}, 2, 4, 6, 8, 10 );
        Coordinate start_position = { start_value, start_value };
        Coordinate minimum = { 0.0, 0.0 };
        auto result_steps =
            run_gradient_decent( function0, function0_gradient, start_position,
                                 std::move( updater ), gtol, 100 );
        REQUIRE( result_steps.size() == 2 );
        // only one step to the minimum
        REQUIRE( coordinatesAreClose( start_position, minimum ) );
        REQUIRE( result_steps[ 0 ] == Coordinate{ start_value, start_value } );
        REQUIRE( result_steps[ 1 ] == start_position );
    }

    SECTION( "too few allowed iterations" )
    {
        auto start_position =
            GENERATE( as< Coordinate >{}, Coordinate{ 1, 0 },
                      Coordinate{ 0, 1 }, Coordinate{ 1, 1 } );
        // as momentum can arrive at minimum faster we can only test less iters
        auto max_iter = GENERATE( as< unsigned >{}, 0, 1, 2, 3, 4 );
        const double learning_rate = 0.25;
        const double gtol = 1e-4;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );

        Coordinate minimum = { 0.0, 0.0 };
        auto result_steps =
            run_gradient_decent( function0, function0_gradient, start_position,
                                 std::move( updater ), gtol, max_iter );
        // not enough to reach minimum
        REQUIRE( result_steps.size() == max_iter + 1 );
        REQUIRE( not coordinatesAreClose( start_position, minimum ) );
    }

    SECTION( "finds minimum" )
    {
        auto start_position =
            GENERATE( as< Coordinate >{}, Coordinate{ 1, 0 },
                      Coordinate{ 0, 1 }, Coordinate{ 1, 1 } );
        const unsigned max_iter = 5000;
        const double learning_rate = 0.1;
        const double gtol = 1e-4;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );

        Coordinate minimum = { 0.0, 0.0 };
        auto result_steps =
            run_gradient_decent( function0, function0_gradient, start_position,
                                 std::move( updater ), gtol, max_iter );
        // enough to reach minimum
        REQUIRE( result_steps.size() < max_iter + 1 );
        // tolerance for test should be the same that was given to the algorithm
        // to determine if one should stop
        REQUIRE( coordinatesAreClose( start_position, minimum, 1e-4, 1e-4 ) );
    }
}

TEST_CASE( "Convergence function1", "[momentum]" )
{
    SECTION( "finds minimum" )
    {
        auto start_position = GENERATE(
            as< Coordinate >{}, Coordinate{ 2, 4 }, Coordinate{ 1, 4 },
            Coordinate{ 2, 3 }, Coordinate{ 1, 3 }, Coordinate{ 3, 4 },
            Coordinate{ 2, 5 }, Coordinate{ 3, 5 } );
        const unsigned max_iter = 5000;
        const double learning_rate = 0.1;
        const double gtol = 1e-4;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );

        Coordinate minimum = { 2.0, 4.0 };
        auto result_steps =
            run_gradient_decent( function1, function1_gradient, start_position,
                                 std::move( updater ), gtol, max_iter );
        // enough to reach minimum
        REQUIRE( result_steps.size() < max_iter + 1 );
        // tolerance for test should be the same that was given to the algorithm
        // to determine if one should stop
        REQUIRE( coordinatesAreClose( start_position, minimum, 1e-4, 1e-4 ) );
    }
}
TEST_CASE( "Convergence function2", "[momentum]" )
{
    SECTION( "finds minimum" )
    {
        auto start_position = GENERATE(
            as< Coordinate >{}, Coordinate{ 1, 3 }, Coordinate{ .0, 3 },
            Coordinate{ 1, 2 }, Coordinate{ .0, 2 }, Coordinate{ 2, 3 },
            Coordinate{ 1, 4 }, Coordinate{ 2, 4 } );
        const unsigned max_iter = 5000;
        const double learning_rate = 0.1;
        const double gtol = 1e-4;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );

        Coordinate minimum = { 1.0, 3.0 };
        auto result_steps =
            run_gradient_decent( function2, function2_gradient, start_position,
                                 std::move( updater ), gtol, max_iter );
        // enough to reach minimum
        REQUIRE( result_steps.size() < max_iter + 1 );
        // tolerance for test should be the same that was given to the algorithm
        // to determine if one should stop
        REQUIRE( coordinatesAreClose( start_position, minimum, 1e-4, 1e-4 ) );
    }
}
