// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cmath>
#include <iostream>
#include <memory>
#include <numeric>
#include <vector>

#include "Types.h"
#include "Updater.h"

CoordinateArray
run_gradient_decent( const Function &f, const FunctionGradient &gradf,
                     Coordinate &coords,
                     std::unique_ptr< CoordinateUpdater > updater, double gtol,
                     unsigned long max_iterations )
{
    auto isConverged = [ &gtol ]( auto const &g ) -> bool
    {
        using value_type = typename std::decay_t< decltype( g ) >::value_type;
        return std::inner_product( g.begin(), g.end(), g.begin(),
                                   value_type( 0 ) ) < ( gtol * gtol );
    };

    CoordinateArray steps_taken{};
    steps_taken.push_back( coords );

    auto grad = gradf( coords );
    bool is_converged = isConverged( grad );
    unsigned long num_iterations = 0ul;

    while ( !is_converged && num_iterations < max_iterations )
    {
        num_iterations++;
        // Update the coordinates based on the current gradient
        updater->update_coordinates( coords, grad );
        // save current position
        steps_taken.push_back( coords );
        // Compute the gradient at the updated position
        grad = gradf( coords );
        // Check if the gradient is small enough to stop the iterations
        is_converged = isConverged( grad );
    }

    if ( is_converged )
    {
        std::cout << "Converged after " << num_iterations << " iterations\n";
    }
    else
    {
        std::cout << "Failed to converge after " << max_iterations
                  << " iterations\n";
    }

    return steps_taken;
}
