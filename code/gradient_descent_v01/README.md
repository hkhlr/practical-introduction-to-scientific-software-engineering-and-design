<!--
SPDX-FileCopyrightText: © 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Development steps

## Overall Goals

### Primary

* Extend the code with a new gradient descent variant
* Maintain correctness of existing variants

### Secondary

* Refactor code to remove code redundancy
* Improve readability
* Establish (automated) tests for existing parts of the code.

## What have we accomplished so far?

* Placed code under version control.
* Add basic black-box tests to make sure final result is correct.
   * Refer to [`tests`](./tests/) directory.

## What's next?

- Move `main` function from [`GradientDescent.cxx`](./GradientDescent.cxx)  to a dedicated source file (`main.cxx`).
- Separate code for GD in source and header file.
   * Header file (`GradientDescent.h`): Contains class definitions.
      * Base class
      * Derived classes
      * Do not forget the header (include) guard.
   * Source file (`GradientDescent.cxx`): Contains method implementations of derived classes. Also includes header file.
- Move type aliases (type definitions) to a separate header file (e.g. `Types.h`) that will be included as needed.