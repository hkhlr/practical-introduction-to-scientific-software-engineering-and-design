// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

/*
 * Time for some type aliases
 */
using Coordinate = std::vector< double >;
using CoordinateArray = std::vector< Coordinate >;
using Function = std::function< typename Coordinate::value_type( Coordinate ) >;
using GradientFunction = std::function< Coordinate( Coordinate ) >;

#ifndef NO_VISUALIZATION
// if NO_VISUALIZATION macro is defined: we compile without the code for
// visualization may be necessary on machines where matplotlib is not installed
#include <matplotlibcpp17/animation.h>
#include <matplotlibcpp17/cm.h>
#include <matplotlibcpp17/mplot3d.h>
#include <matplotlibcpp17/pyplot.h>

// settings for the visualization
const std::vector< double > lower_limits{ -1, -1, 0 };
const std::vector< double > upper_limits{ 3, 3, 10 };
double const plotting_step_size = 0.1;
#endif

/*
 * Interface to be implemented by *all* GradientDescent-alike classes.
 */
class GradientDescent
{
  public:
    virtual ~GradientDescent() noexcept = default;

    virtual void run() = 0;

    virtual double stepSize() const = 0;
    virtual void stepSize( double step_size ) = 0;

    virtual Coordinate const &coordinates() const = 0;
    virtual void coordinates( Coordinate const &coords ) = 0;

    // Returns by values because gradient will always be computed anew.
    virtual Coordinate getGradient() const = 0;
};

class VanillaGradientDescent final : public GradientDescent
{
  public:
    ~VanillaGradientDescent() noexcept override = default;

    VanillaGradientDescent( Function const &f, GradientFunction const &gradf,
                            Coordinate const &coords, double step_size = 1e-1,
                            double gtol = 1e-4, size_t max_iterations = 1000ul )
        : f_( f )
        , gradf_( gradf )
        , coords_( coords )
        , step_size_( step_size )
        , gtol_( gtol )
        , max_iterations_( max_iterations )
    {
    }

    void run() override
    {
        assert( coords_.size() > 0 ); // TODO do we want to keep that assertion
        // visualization code only works properly for 2D
        // it is hard to visualize more dimensions anyway ;-)
        if ( coords_.size() > 2 )
        {
#ifndef NO_VISUALIZATION
            std::cout << "WARNING: Only visualizing the function with regard "
                         "to the first two dimensions\n";
#endif
        }
        if ( coords_.size() == 1 )
        {
#ifndef NO_VISUALIZATION
            // add a second dummy dimension for visualization if necessary
            coords_.push_back( 1 );
#endif
        }

#ifndef NO_VISUALIZATION
        // set up visualization
        pybind11::scoped_interpreter guard{};
        auto plt = matplotlibcpp17::pyplot::import();
        // this is required for "projection = 3d"
        matplotlibcpp17::mplot3d::import();
        auto fig = plt.figure();
        auto ax = fig.add_subplot( py::make_tuple(),
                                   Kwargs( "projection"_a = "3d" ) );

        // plot the function surface
        std::vector< std::vector< double > > X;
        std::vector< std::vector< double > > Y;
        std::vector< std::vector< double > > Z;

        // calculate function points to visualize in a grid
        for ( int i = 0; i < std::abs( lower_limits[ 0 ] - upper_limits[ 0 ] ) /
                                 plotting_step_size;
              ++i )
        {
            X.push_back( {} );
            Y.push_back( {} );
            Z.push_back( {} );
            for ( int j = 0;
                  j < std::abs( lower_limits[ 1 ] - upper_limits[ 1 ] ) /
                          plotting_step_size;
                  ++j )
            {
                double const x = lower_limits[ 0 ] + i * plotting_step_size;
                double const y = lower_limits[ 1 ] + j * plotting_step_size;
                std::vector< double > function_in{ x, y };
                // Fill the vector with dummy values for other dimensions if
                // needed.
                std::fill_n( std::back_inserter( function_in ),
                             coords_.size() - 2, 1 );
                double const z = f_( function_in );
                X[ i ].push_back( x );
                Y[ i ].push_back( y );
                Z[ i ].push_back( z );
            }
        }
        // to numpy array
        const auto X_ = py::array( py::cast( std::move( X ) ) );
        const auto Y_ = py::array( py::cast( std::move( Y ) ) );
        const auto Z_ = py::array( py::cast( std::move( Z ) ) );
        // the actual plotting:
        const auto surf = ax.plot_surface(
            Args( X_, Y_, Z_ ),
            Kwargs( "rstride"_a = 1, "cstride"_a = 1, "linewidth"_a = 0,
                    "antialiased"_a = true, "alpha"_a = 0.75,
                    "cmap"_a = matplotlibcpp17::cm::coolwarm ) );
        fig.colorbar( Args( surf.unwrap() ),
                      Kwargs( "shrink"_a = 0.5, "aspect"_a = 5 ) );
        // end of plotting the function

        // list of the steps to animate
        py::list artist_list;
#endif

        Coordinate grad = getGradient();
        double g_l2norm2 = 0;
        for ( size_t idx = 0; idx < grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];

        bool is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
        size_t num_iterations = 0ul;

        while ( !is_converged && num_iterations < max_iterations_ )
        {
            num_iterations++;

            std::vector< double > x{ coords_[ 0 ] };
            std::vector< double > y{ coords_[ 1 ] };
            std::vector< double > z{ f_( coords_ ) };

            // Update the coordinates given the current gradient information.
            for ( size_t idx = 0; idx < coords_.size(); ++idx )
            {
                coords_[ idx ] -= step_size_ * grad[ idx ];
            }

            // Position after the update.
            x.push_back( coords_[ 0 ] );
            y.push_back( coords_[ 1 ] );
            z.push_back( f_( coords_ ) );
#ifndef NO_VISUALIZATION
            // Visualize this step and add it to the list of steps to
            // animate.
            ax.plot( Args( x, y, z ),
                     Kwargs( "color"_a = "black", "antialiased"_a = true ) );
            artist_list.append( ax.get_lines().unwrap() );
#endif
            // Compute the gradient at the updated position. Also check if
            // the gradient is small enough to stop the iterations
            grad = getGradient();
            g_l2norm2 = 0;
            for ( size_t idx = 0; idx < grad.size(); ++idx )
                g_l2norm2 += grad[ idx ] * grad[ idx ];
            is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
        }

        if ( is_converged )
        {
            std::cout << "Converged after " << num_iterations
                      << " iterations\n";
        }
        else
        {
            std::cout << "Failed to converge after " << max_iterations_
                      << " iterations\n";
        }

#ifndef NO_VISUALIZATION
        // Animate the iterations by plotting all visited points on the path
        // towards the local minimum.
        auto ani = matplotlibcpp17::animation::ArtistAnimation(
            Args( fig.unwrap(), artist_list ), Kwargs( "interval"_a = 100 ) );

        plt.show();
#endif
    }

    double stepSize() const noexcept override { return step_size_; }
    void stepSize( double step_size ) override { step_size_ = step_size; }

    Coordinate const &coordinates() const override { return coords_; }
    void coordinates( Coordinate const &coords ) override
    {
        assert( coords.size() == coords_.size() );
        coords_ = coords;
    };
    Coordinate getGradient() const override { return gradf_( coords_ ); }

  private:
    Function const f_{};
    GradientFunction const gradf_{};
    Coordinate coords_{};
    double step_size_{ 1e-1 };
    double const gtol_{ 1e-4 };
    size_t const max_iterations_{ 1000ul };
};

class MomentumGradientDescent final : public GradientDescent
{
  public:
    ~MomentumGradientDescent() noexcept override = default;

    MomentumGradientDescent( Function const &f, GradientFunction const &gradf,
                             Coordinate const &coords, double step_size = 1e-1,
                             double gtol = 1e-3, size_t max_iterations = 1000ul,
                             double alpha = 9e-1 )
        : f_( f )
        , gradf_( gradf )
        , coords_( coords )
        , step_size_( step_size )
        , gtol_( gtol )
        , max_iterations_( max_iterations )
        , alpha_( alpha )
        , momentum_( coords.size(), 0 )
    {
    }
    void run() override
    {
        assert( coords_.size() > 0 ); // TODO do we want to keep that assertion
        // visualization code only works properly for 2D
        // it is hard to visualize more dimensions anyway ;-)
        if ( coords_.size() > 2 )
        {
#ifndef NO_VISUALIZATION
            std::cout << "WARNING: Only visualizing the function with regard "
                         "to the first two dimensions\n";
#endif
        }
        if ( coords_.size() == 1 )
        {
#ifndef NO_VISUALIZATION
            // add a second dummy dimension for visualization if necessary
            coords_.push_back( 1 );
            momentum_.push_back( 0 );
#endif
        }

#ifndef NO_VISUALIZATION
        // set up visualization
        pybind11::scoped_interpreter guard{};
        auto plt = matplotlibcpp17::pyplot::import();
        // this is required for "projection = 3d"
        matplotlibcpp17::mplot3d::import();
        auto fig = plt.figure();
        auto ax = fig.add_subplot( py::make_tuple(),
                                   Kwargs( "projection"_a = "3d" ) );

        // plot the function surface
        std::vector< std::vector< double > > X;
        std::vector< std::vector< double > > Y;
        std::vector< std::vector< double > > Z;

        // calculate function points to visualize in a grid
        for ( int i = 0; i < std::abs( lower_limits[ 0 ] - upper_limits[ 0 ] ) /
                                 plotting_step_size;
              ++i )
        {
            X.push_back( {} );
            Y.push_back( {} );
            Z.push_back( {} );
            for ( int j = 0;
                  j < std::abs( lower_limits[ 1 ] - upper_limits[ 1 ] ) /
                          plotting_step_size;
                  ++j )
            {
                double const x = lower_limits[ 0 ] + i * plotting_step_size;
                double const y = lower_limits[ 1 ] + j * plotting_step_size;
                std::vector< double > function_in{ x, y };
                // Fill the vector with dummy values for other dimensions if
                // needed.
                std::fill_n( std::back_inserter( function_in ),
                             coords_.size() - 2, 1 );
                double const z = f_( function_in );
                X[ i ].push_back( x );
                Y[ i ].push_back( y );
                Z[ i ].push_back( z );
            }
        }
        // to numpy array
        const auto X_ = py::array( py::cast( std::move( X ) ) );
        const auto Y_ = py::array( py::cast( std::move( Y ) ) );
        const auto Z_ = py::array( py::cast( std::move( Z ) ) );
        // the actual plotting:
        const auto surf = ax.plot_surface(
            Args( X_, Y_, Z_ ),
            Kwargs( "rstride"_a = 1, "cstride"_a = 1, "linewidth"_a = 0,
                    "antialiased"_a = true, "alpha"_a = 0.75,
                    "cmap"_a = matplotlibcpp17::cm::coolwarm ) );
        fig.colorbar( Args( surf.unwrap() ),
                      Kwargs( "shrink"_a = 0.5, "aspect"_a = 5 ) );
        // end of plotting the function

        // list of the steps to animate
        py::list artist_list;
#endif

        Coordinate grad = getGradient();
        double g_l2norm2 = 0;
        for ( size_t idx = 0; idx < grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];

        bool is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
        size_t num_iterations = 0;

        while ( !is_converged && num_iterations < max_iterations_ )
        {
            num_iterations++;

            // Position before the update.
            std::vector< double > x{ coords_[ 0 ] };
            std::vector< double > y{ coords_[ 1 ] };
            std::vector< double > z{ f_( coords_ ) };

            // Update the coordinates given the current gradient
            // information.
            for ( size_t idx = 0; idx < coords_.size(); ++idx )
            {
                // https://en.wikipedia.org/wiki/Stochastic_gradient_descent#Momentum
                double const delta =
                    momentum_[ idx ] * alpha_ - step_size_ * grad[ idx ];
                coords_[ idx ] += delta;
                momentum_[ idx ] = delta;
            }
            // Position after the update.
            x.push_back( coords_[ 0 ] );
            y.push_back( coords_[ 1 ] );
            z.push_back( f_( coords_ ) );
#ifndef NO_VISUALIZATION
            // Visualize this step and add it to the list of steps to
            // animate.
            ax.plot( Args( x, y, z ),
                     Kwargs( "color"_a = "black", "antialiased"_a = true ) );
            artist_list.append( ax.get_lines().unwrap() );
#endif

            // Compute the gradient at the updated position. Also check if
            // the gradient is small enough to stop the iterations.
            grad = getGradient();
            g_l2norm2 = 0;
            for ( size_t idx = 0; idx < grad.size(); ++idx )
                g_l2norm2 += grad[ idx ] * grad[ idx ];
            is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
        }

        if ( is_converged )
        {
            std::cout << "Converged after " << num_iterations
                      << " iterations\n";
        }
        else
        {
            std::cout << "Failed to converge after " << max_iterations_
                      << " iterations\n";
        }
#ifndef NO_VISUALIZATION
        // Animate the iterations by plotting all visited points on the path
        // towards the local minimum.
        auto ani = matplotlibcpp17::animation::ArtistAnimation(
            Args( fig.unwrap(), artist_list ), Kwargs( "interval"_a = 100 ) );

        plt.show();
#endif
    }

    double stepSize() const noexcept override { return step_size_; }
    void stepSize( double step_size ) override { step_size_ = step_size; }

    Coordinate const &coordinates() const override { return coords_; }
    void coordinates( Coordinate const &coords ) override
    {
        assert( coords.size() == coords_.size() );
        coords_ = coords;
    };

    Coordinate getGradient() const override { return gradf_( coords_ ); }

  private:
    /*Common parameters*/
    Function const f_{};
    GradientFunction const gradf_{};
    Coordinate coords_{};
    double step_size_{ 1e-1 };
    double const gtol_{ 1e-4 };
    size_t const max_iterations_{ 1000u };
    /*Momentum specific parameters*/
    double const alpha_{ 9e-1 };
    Coordinate momentum_{};
};

int main( int argc, char *argv[] )
{
    /*
     * The function to be minimized.
     */
    auto f = []( Coordinate coords ) -> typename Coordinate::value_type
    {
        auto const x{ coords.at( 0 ) };
        auto const y{ coords.at( 1 ) };
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    /*
     * Gradient fo the function to be minimized.
     */
    auto grad_f = []( Coordinate coords ) -> Coordinate
    {
        auto const x{ coords.at( 0 ) };
        auto const y{ coords.at( 1 ) };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    /*
     * Initial coordinates and learning rate.
     */
    Coordinate const starting_pos = { -1.0, 2.50 };
    double const step_size = 3e-1;

    /*
     * Use command line argument to specify which GD "flavour" to use.
     * Default (no argument passed): 0 --> "vanilla" GD
     */
    int const implementation_to_use = ( argc > 1 ) ? std::stoi( argv[ 1 ] ) : 0;

    std::unique_ptr< GradientDescent > gd;
    switch ( implementation_to_use )
    {
    case 1:
        gd = std::make_unique< MomentumGradientDescent >( f, grad_f, //
                                                          starting_pos,
                                                          step_size );
        break;
    case 0:
    default:
        gd = std::make_unique< VanillaGradientDescent >( f, grad_f, //
                                                         starting_pos,
                                                         step_size );
        break;
    }

    gd->run();

    std::cout << "Final coordinates: x = " << gd->coordinates()[ 0 ]
              << ", y = " << gd->coordinates()[ 1 ] << "\n";

    return EXIT_SUCCESS;
}
