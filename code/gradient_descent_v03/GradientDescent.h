// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#ifndef GRADIENT_DESCENT_H_INCLUDE_GUARD
#define GRADIENT_DESCENT_H_INCLUDE_GUARD

#include <vector>

/*
 * Time for some type aliases
 */
#include "Types.h"

#ifndef NO_VISUALIZATION
// if NO_VISUALIZATION macro is defined: we compile without the code for
// visualization may be necessary on machines where matplotlib is not installed

// settings for the visualization
const std::vector< double > lower_limits{ -1, -1, 0 };
const std::vector< double > upper_limits{ 3, 3, 10 };
double const plotting_step_size = 0.1;
#endif

/*
 * Interface to be implemented by *all* GradientDescent-alike classes.
 */
class GradientDescent
{
  public:
    virtual ~GradientDescent() noexcept = default;

    virtual CoordinateArray run() = 0;

    virtual double stepSize() const = 0;
    virtual void stepSize( double step_size ) = 0;

    virtual Coordinate const &coordinates() const = 0;
    virtual void coordinates( Coordinate const &coords ) = 0;

    // Returns by values because gradient will always be computed anew.
    virtual Coordinate getGradient() const = 0;
};

class VanillaGradientDescent final : public GradientDescent
{
  public:
    ~VanillaGradientDescent() noexcept override = default;

    VanillaGradientDescent( Function const &f, FunctionGradient const &gradf,
                            Coordinate const &coords, double step_size = 1e-1,
                            double gtol = 1e-4,
                            size_t max_iterations = 1000ul );
    CoordinateArray run() override;
    double stepSize() const noexcept override;
    void stepSize( double step_size ) override;
    Coordinate const &coordinates() const override;
    void coordinates( Coordinate const &coords ) override;
    Coordinate getGradient() const override;

  private:
    Function const f_{};
    FunctionGradient const gradf_{};
    Coordinate coords_{};
    double step_size_{ 1e-1 };
    double const gtol_{ 1e-4 };
    size_t const max_iterations_{ 1000ul };
};

class MomentumGradientDescent final : public GradientDescent
{
  public:
    ~MomentumGradientDescent() noexcept override = default;

    MomentumGradientDescent( Function const &f, FunctionGradient const &gradf,
                             Coordinate const &coords, double step_size = 1e-1,
                             double gtol = 1e-3, size_t max_iterations = 1000ul,
                             double alpha = 9e-1 );
    CoordinateArray run() override;
    double stepSize() const noexcept override;
    void stepSize( double step_size ) override;
    Coordinate const &coordinates() const override;
    void coordinates( Coordinate const &coords ) override;
    Coordinate getGradient() const override;

  private:
    /*Common parameters*/
    Function const f_{};
    FunctionGradient const gradf_{};
    Coordinate coords_{};
    double step_size_{ 1e-1 };
    double const gtol_{ 1e-4 };
    size_t const max_iterations_{ 1000u };
    /*Momentum specific parameters*/
    double const alpha_{ 9e-1 };
    Coordinate momentum_{};
};

#endif