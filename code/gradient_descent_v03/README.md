<!--
SPDX-FileCopyrightText: © 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Development steps

## Overall Goals

### Primary

* Extend the code with a new gradient descent variant
* Maintain correctness of existing variants

### Secondary

* Refactor code to remove code redundancy
* Improve readability
* Establish (automated) tests for existing parts of the code.

## What have we accomplished so far?

### [`gradient_descent_v01`](../gradient_descent_v01)
* Placed code under version control.
* Add basic black-box tests to make sure final result is correct.
   * Refer to [`tests`](./tests/) directory.

### [`gradient_descent_v02`](../gradient_descent_v02)
* Dedicated file [`main.cxx`](./main.cxx) that contains driver code.
* Source ([`GradientDescent.cxx`](./GradientDescent.cxx)) and header file ([`GradientDescent.h`](./GradientDescent.h)) for GD code.
* Type aliases in separate header file ([`Types.h`](./Types.h)).

### [`gradient_descent_v03`](../gradient_descent_v03)

* Remove plotting functionality *only* from vanilla GD code.
    * Inspect the [`GradientDescent.cxx`](./GradientDescent.cxx) source file to convince yourself that it still is present inside the `run` method of the `MomentumGradientDescent` class.
* Move code for plotting into separate source and header file.
    * Source file: [`Plotter.cxx`](./Plotter.cxx)
    * Header file: [`Plotter.h`](./Plotter.h)
* Driver code inside `main` function will does (not yet) use the plotting code.


## What's next?

* Remove plotting code also for momentum GD.
* Make the driver code in `main` also use the *externalised* plotting functionality.