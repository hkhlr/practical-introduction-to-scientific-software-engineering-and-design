// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#ifndef UPDATER_H_INCLUDE_GUARD
#define UPDATER_H_INCLUDE_GUARD

#include <vector>

#include "Types.h"

class CoordinateUpdater
{
  public:
    virtual ~CoordinateUpdater() noexcept = default;
    // coordinates are given by reference, therefore no return value is needed,
    // as the coords will be updated
    virtual void update_coordinates( Coordinate &coordinates,
                                     Coordinate const &gradient ) = 0;
};

class VanillaUpdater final : public CoordinateUpdater
{
  public:
    explicit VanillaUpdater( double step_size = 1e-2 )
        : step_size_{ step_size }
    {
    }
    ~VanillaUpdater() noexcept override = default;

    void update_coordinates( Coordinate &coordinates,
                             Coordinate const &gradient ) override
    {
        std::transform( coordinates.begin(), coordinates.end(),
                        gradient.begin(), coordinates.begin(),
                        [ this ]( auto x, auto g )
                        { return x - this->step_size_ * g; } );
    }

  private:
    double const step_size_{};
};

class MomentumUpdater final : public CoordinateUpdater
{
  public:
    MomentumUpdater( double alpha, double step_size, unsigned dimensions )
        : alpha_{ alpha }
        , step_size_{ step_size }
        , momentum_( dimensions,
                     0 ) // initializes a vector with dimensions '0' entries
    {
    }
    ~MomentumUpdater() noexcept override = default;

    void update_coordinates( Coordinate &coordinates,
                             Coordinate const &gradient ) override
    {
        for ( unsigned long i = 0; i < coordinates.size(); ++i )
        {
            auto delta = momentum_[ i ] * alpha_ - step_size_ * gradient[ i ];
            coordinates[ i ] += delta;
            momentum_[ i ] = delta;
        }
    }

  private:
    double const alpha_{};
    double const step_size_{};
    Coordinate momentum_{};
};

#endif // UPDATER_H_INCLUDE_GUARD
