// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#ifndef TEST_INFRASTRUCTURE_H_INCLUDE_GUARD
#define TEST_INFRASTRUCTURE_H_INCLUDE_GUARD

#include <limits> // std::numeric_limits

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

// check for closeness of values instead of exact equality due to rounding
// errors
constexpr double double_machine_epsilon{
    std::numeric_limits< double >::epsilon() };
inline bool isClose( double value, // Value to be tested for closeness
                     double ref,   // Reference value
                     double atol = 10 * double_machine_epsilon,
                     double rtol = 1e-8 )
{
    return ( std::abs( ref - value ) < atol + rtol * std::abs( ref ) );
}

inline bool coordinatesAreClose( Coordinate const &coord,
                                 Coordinate const &coord_ref,
                                 double atol = 10.0 * double_machine_epsilon,
                                 double rtol = 1e-8 )
{
    return ( isClose( coord[ 0 ], coord_ref[ 0 ], atol, rtol ) &&
             isClose( coord[ 1 ], coord_ref[ 1 ], atol, rtol ) );
}

const Function function0{ []( Coordinate const &c ) -> double {
    return c[ 0 ] * c[ 0 ] + c[ 1 ] * c[ 1 ];
} }; /* x ** 2 + y ** 2 */
const FunctionGradient function0_gradient{
    []( Coordinate const &c ) -> Coordinate {
        return { 2 * c[ 0 ], 2 * c[ 1 ] };
    } }; /* { 2 * x, 2 * y } */

const Function function1{ []( Coordinate const &c ) -> double {
    return ( c[ 0 ] - 2 ) * ( c[ 0 ] - 2 ) + ( c[ 1 ] - 4 ) * ( c[ 1 ] - 4 );
} };
const FunctionGradient function1_gradient{
    []( Coordinate const &c ) -> Coordinate {
        return { 2 * ( c[ 0 ] - 2 ), 4 * ( c[ 1 ] - 4 ) };
    } };

// Booth function
// (https://en.wikipedia.org/wiki/Test_functions_for_optimization)
const Function function2{
    []( Coordinate const &c ) -> double
    {
        // ( x + 2 * y - 7 ) ** 2 + ( 2 x + y - 5 ) ** 2
        double const term_left{ c[ 0 ] + 2 * c[ 1 ] - 7 };
        double const term_right{ 2 * c[ 0 ] + c[ 1 ] - 5 };
        return term_left * term_left + term_right * term_right;
    } };
const FunctionGradient function2_gradient{
    []( Coordinate const &c ) -> Coordinate
    {
        // gradient of ((x + 2 y - 7)^2 + (2 x + y - 5)^2) = (10 x + 8 y -
        // 34, 8 x + 10 y - 38)
        return { 10 * c[ 0 ] + 8 * c[ 1 ] - 34, 8 * c[ 0 ] + 10 * c[ 1 ] - 38 };
    } };

#endif // TEST_INFRASTRUCTURE_H_INCLUDE_GUARD