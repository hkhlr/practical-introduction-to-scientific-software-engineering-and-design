<!--
SPDX-FileCopyrightText: © 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Development steps

## Overall Goals

### Primary

* Extend the code with a new gradient descent variant
* Maintain correctness of existing variants

### Secondary

* Refactor code to remove code redundancy
* Improve readability
* Establish (automated) tests for existing parts of the code.

## What have we accomplished so far?

### [`gradient_descent_v01`](../gradient_descent_v01)
* Placed code under version control.
* Add basic black-box tests to make sure final result is correct.
   * Refer to [`tests`](./tests/) directory.

### [`gradient_descent_v02`](../gradient_descent_v02)
* Dedicated file [`main.cxx`](./main.cxx) that contains driver code.
* Source ([`GradientDescent.cxx`](./GradientDescent.cxx)) and header file ([`GradientDescent.h`](./GradientDescent.h)) for GD code.
* Type aliases in separate header file ([`Types.h`](./Types.h)).


## What's next?

* Remove plotting functionality from vanilla GD code.
   * For the moment, we'll leave it in the code for momentum GD.
* Move code for plotting into separate source and header file.
* Driver code inside `main` function will --- at least temporarily --- not plot the iteration path.