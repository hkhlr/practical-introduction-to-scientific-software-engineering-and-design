#!/bin/bash

# SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
#
# SPDX-License-Identifier: MIT

# minimum: -(2 x y + 2 x - x^2 - 2 y^2) = 2,1

EXECUTABLE=$1
if [ -z "${EXECUTABLE}" ]; then
echo "No executable given"
exit 1
fi

echo $EXECUTABLE

OUTPUT=$($EXECUTABLE)

echo $OUTPUT

if echo "$OUTPUT" | grep -q "Converged after"; then
  # cas converged
  :  # no-op (do nothing)
else
  #has not converged
  echo "not converged"
  exit 1
fi

if echo "$OUTPUT" | grep -q "x = 1.99"; then
  # close x value
  :  # no-op (do nothing)
else
  #wrong x value
  echo "Wrong X value"
  exit 1
fi

if echo "$OUTPUT" | grep -q "y = 0.99"; then
  # close y value
  :  # no-op (do nothing)
else
  #wrong y value
  echo "Wrong Y value"
  exit 1
fi

