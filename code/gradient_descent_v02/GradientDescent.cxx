// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "GradientDescent.h"

#ifndef NO_VISUALIZATION
// if NO_VISUALIZATION macro is defined: we compile without the code for
// visualization may be necessary on machines where matplotlib is not installed
#include <matplotlibcpp17/animation.h>
#include <matplotlibcpp17/cm.h>
#include <matplotlibcpp17/mplot3d.h>
#include <matplotlibcpp17/pyplot.h>
#endif

VanillaGradientDescent::VanillaGradientDescent( Function const &f,
                                                FunctionGradient const &gradf,
                                                Coordinate const &coords,
                                                double step_size, double gtol,
                                                size_t max_iterations )
    : f_( f )
    , gradf_( gradf )
    , coords_( coords )
    , step_size_( step_size )
    , gtol_( gtol )
    , max_iterations_( max_iterations )
{
}

void VanillaGradientDescent::run()
{
    assert( coords_.size() > 0 ); // TODO do we want to keep that assertion
    // visualization code only works properly for 2D
    // it is hard to visualize more dimensions anyway ;-)
    if ( coords_.size() > 2 )
    {
#ifndef NO_VISUALIZATION
        std::cout << "WARNING: Only visualizing the function with regard "
                     "to the first two dimensions\n";
#endif
    }
    if ( coords_.size() == 1 )
    {
#ifndef NO_VISUALIZATION
        // add a second dummy dimension for visualization if necessary
        coords_.push_back( 1 );
#endif
    }

#ifndef NO_VISUALIZATION
    // set up visualization
    pybind11::scoped_interpreter guard{};
    auto plt = matplotlibcpp17::pyplot::import();
    // this is required for "projection = 3d"
    matplotlibcpp17::mplot3d::import();
    auto fig = plt.figure();
    auto ax =
        fig.add_subplot( py::make_tuple(), Kwargs( "projection"_a = "3d" ) );

    // plot the function surface
    std::vector< std::vector< double > > X;
    std::vector< std::vector< double > > Y;
    std::vector< std::vector< double > > Z;

    // calculate function points to visualize in a grid
    for ( int i = 0; i < std::abs( lower_limits[ 0 ] - upper_limits[ 0 ] ) /
                             plotting_step_size;
          ++i )
    {
        X.push_back( {} );
        Y.push_back( {} );
        Z.push_back( {} );
        for ( int j = 0; j < std::abs( lower_limits[ 1 ] - upper_limits[ 1 ] ) /
                                 plotting_step_size;
              ++j )
        {
            double const x = lower_limits[ 0 ] + i * plotting_step_size;
            double const y = lower_limits[ 1 ] + j * plotting_step_size;
            std::vector< double > function_in{ x, y };
            // Fill the vector with dummy values for other dimensions if
            // needed.
            std::fill_n( std::back_inserter( function_in ), coords_.size() - 2,
                         1 );
            double const z = f_( function_in );
            X[ i ].push_back( x );
            Y[ i ].push_back( y );
            Z[ i ].push_back( z );
        }
    }
    // to numpy array
    const auto X_ = py::array( py::cast( std::move( X ) ) );
    const auto Y_ = py::array( py::cast( std::move( Y ) ) );
    const auto Z_ = py::array( py::cast( std::move( Z ) ) );
    // the actual plotting:
    const auto surf = ax.plot_surface(
        Args( X_, Y_, Z_ ),
        Kwargs( "rstride"_a = 1, "cstride"_a = 1, "linewidth"_a = 0,
                "antialiased"_a = true, "alpha"_a = 0.75,
                "cmap"_a = matplotlibcpp17::cm::coolwarm ) );
    fig.colorbar( Args( surf.unwrap() ),
                  Kwargs( "shrink"_a = 0.5, "aspect"_a = 5 ) );
    // end of plotting the function

    // list of the steps to animate
    py::list artist_list;
#endif

    Coordinate grad = getGradient();
    double g_l2norm2 = 0;
    for ( size_t idx = 0; idx < grad.size(); ++idx )
        g_l2norm2 += grad[ idx ] * grad[ idx ];

    bool is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
    size_t num_iterations = 0ul;

    while ( !is_converged && num_iterations < max_iterations_ )
    {
        num_iterations++;

        std::vector< double > x{ coords_[ 0 ] };
        std::vector< double > y{ coords_[ 1 ] };
        std::vector< double > z{ f_( coords_ ) };

        // Update the coordinates given the current gradient information.
        for ( size_t idx = 0; idx < coords_.size(); ++idx )
        {
            coords_[ idx ] -= step_size_ * grad[ idx ];
        }

        // Position after the update.
        x.push_back( coords_[ 0 ] );
        y.push_back( coords_[ 1 ] );
        z.push_back( f_( coords_ ) );
#ifndef NO_VISUALIZATION
        // Visualize this step and add it to the list of steps to
        // animate.
        ax.plot( Args( x, y, z ),
                 Kwargs( "color"_a = "black", "antialiased"_a = true ) );
        artist_list.append( ax.get_lines().unwrap() );
#endif
        // Compute the gradient at the updated position. Also check if
        // the gradient is small enough to stop the iterations
        grad = getGradient();
        g_l2norm2 = 0;
        for ( size_t idx = 0; idx < grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];
        is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
    }

    if ( is_converged )
    {
        std::cout << "Converged after " << num_iterations << " iterations\n";
    }
    else
    {
        std::cout << "Failed to converge after " << max_iterations_
                  << " iterations\n";
    }

#ifndef NO_VISUALIZATION
    // Animate the iterations by plotting all visited points on the path
    // towards the local minimum.
    auto ani = matplotlibcpp17::animation::ArtistAnimation(
        Args( fig.unwrap(), artist_list ), Kwargs( "interval"_a = 100 ) );

    plt.show();
#endif
}

double VanillaGradientDescent::stepSize() const noexcept { return step_size_; }
void VanillaGradientDescent::stepSize( double step_size )
{
    step_size_ = step_size;
}

Coordinate const &VanillaGradientDescent::coordinates() const
{
    return coords_;
}
void VanillaGradientDescent::coordinates( Coordinate const &coords )
{
    assert( coords.size() == coords_.size() );
    coords_ = coords;
};
Coordinate VanillaGradientDescent::getGradient() const
{
    return gradf_( coords_ );
}

MomentumGradientDescent::MomentumGradientDescent(
    Function const &f, FunctionGradient const &gradf, Coordinate const &coords,
    double step_size, double gtol, size_t max_iterations, double alpha )
    : f_( f )
    , gradf_( gradf )
    , coords_( coords )
    , step_size_( step_size )
    , gtol_( gtol )
    , max_iterations_( max_iterations )
    , alpha_( alpha )
    , momentum_( coords.size(), 0 )
{
}
void MomentumGradientDescent::run()
{
    assert( coords_.size() > 0 ); // TODO do we want to keep that assertion
    // visualization code only works properly for 2D
    // it is hard to visualize more dimensions anyway ;-)
    if ( coords_.size() > 2 )
    {
#ifndef NO_VISUALIZATION
        std::cout << "WARNING: Only visualizing the function with regard "
                     "to the first two dimensions\n";
#endif
    }
    if ( coords_.size() == 1 )
    {
#ifndef NO_VISUALIZATION
        // add a second dummy dimension for visualization if necessary
        coords_.push_back( 1 );
        momentum_.push_back( 0 );
#endif
    }

#ifndef NO_VISUALIZATION
    // set up visualization
    pybind11::scoped_interpreter guard{};
    auto plt = matplotlibcpp17::pyplot::import();
    // this is required for "projection = 3d"
    matplotlibcpp17::mplot3d::import();
    auto fig = plt.figure();
    auto ax =
        fig.add_subplot( py::make_tuple(), Kwargs( "projection"_a = "3d" ) );

    // plot the function surface
    std::vector< std::vector< double > > X;
    std::vector< std::vector< double > > Y;
    std::vector< std::vector< double > > Z;

    // calculate function points to visualize in a grid
    for ( int i = 0; i < std::abs( lower_limits[ 0 ] - upper_limits[ 0 ] ) /
                             plotting_step_size;
          ++i )
    {
        X.push_back( {} );
        Y.push_back( {} );
        Z.push_back( {} );
        for ( int j = 0; j < std::abs( lower_limits[ 1 ] - upper_limits[ 1 ] ) /
                                 plotting_step_size;
              ++j )
        {
            double const x = lower_limits[ 0 ] + i * plotting_step_size;
            double const y = lower_limits[ 1 ] + j * plotting_step_size;
            std::vector< double > function_in{ x, y };
            // Fill the vector with dummy values for other dimensions if
            // needed.
            std::fill_n( std::back_inserter( function_in ), coords_.size() - 2,
                         1 );
            double const z = f_( function_in );
            X[ i ].push_back( x );
            Y[ i ].push_back( y );
            Z[ i ].push_back( z );
        }
    }
    // to numpy array
    const auto X_ = py::array( py::cast( std::move( X ) ) );
    const auto Y_ = py::array( py::cast( std::move( Y ) ) );
    const auto Z_ = py::array( py::cast( std::move( Z ) ) );
    // the actual plotting:
    const auto surf = ax.plot_surface(
        Args( X_, Y_, Z_ ),
        Kwargs( "rstride"_a = 1, "cstride"_a = 1, "linewidth"_a = 0,
                "antialiased"_a = true, "alpha"_a = 0.75,
                "cmap"_a = matplotlibcpp17::cm::coolwarm ) );
    fig.colorbar( Args( surf.unwrap() ),
                  Kwargs( "shrink"_a = 0.5, "aspect"_a = 5 ) );
    // end of plotting the function

    // list of the steps to animate
    py::list artist_list;
#endif

    Coordinate grad = getGradient();
    double g_l2norm2 = 0;
    for ( size_t idx = 0; idx < grad.size(); ++idx )
        g_l2norm2 += grad[ idx ] * grad[ idx ];

    bool is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
    size_t num_iterations = 0;

    while ( !is_converged && num_iterations < max_iterations_ )
    {
        num_iterations++;

        // Position before the update.
        std::vector< double > x{ coords_[ 0 ] };
        std::vector< double > y{ coords_[ 1 ] };
        std::vector< double > z{ f_( coords_ ) };

        // Update the coordinates given the current gradient
        // information.
        for ( size_t idx = 0; idx < coords_.size(); ++idx )
        {
            // https://en.wikipedia.org/wiki/Stochastic_gradient_descent#Momentum
            double const delta =
                momentum_[ idx ] * alpha_ - step_size_ * grad[ idx ];
            coords_[ idx ] += delta;
            momentum_[ idx ] = delta;
        }
        // Position after the update.
        x.push_back( coords_[ 0 ] );
        y.push_back( coords_[ 1 ] );
        z.push_back( f_( coords_ ) );
#ifndef NO_VISUALIZATION
        // Visualize this step and add it to the list of steps to
        // animate.
        ax.plot( Args( x, y, z ),
                 Kwargs( "color"_a = "black", "antialiased"_a = true ) );
        artist_list.append( ax.get_lines().unwrap() );
#endif

        // Compute the gradient at the updated position. Also check if
        // the gradient is small enough to stop the iterations.
        grad = getGradient();
        g_l2norm2 = 0;
        for ( size_t idx = 0; idx < grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];
        is_converged = ( std::sqrt( g_l2norm2 ) < gtol_ );
    }

    if ( is_converged )
    {
        std::cout << "Converged after " << num_iterations << " iterations\n";
    }
    else
    {
        std::cout << "Failed to converge after " << max_iterations_
                  << " iterations\n";
    }
#ifndef NO_VISUALIZATION
    // Animate the iterations by plotting all visited points on the path
    // towards the local minimum.
    auto ani = matplotlibcpp17::animation::ArtistAnimation(
        Args( fig.unwrap(), artist_list ), Kwargs( "interval"_a = 100 ) );

    plt.show();
#endif
}

double MomentumGradientDescent::stepSize() const noexcept { return step_size_; }
void MomentumGradientDescent::stepSize( double step_size )
{
    step_size_ = step_size;
}

Coordinate const &MomentumGradientDescent::coordinates() const
{
    return coords_;
}
void MomentumGradientDescent::coordinates( Coordinate const &coords )
{
    assert( coords.size() == coords_.size() );
    coords_ = coords;
};

Coordinate MomentumGradientDescent::getGradient() const
{
    return gradf_( coords_ );
}