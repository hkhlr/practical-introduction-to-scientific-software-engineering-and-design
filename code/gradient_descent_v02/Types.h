// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#pragma once

#include <functional>
#include <vector>

using Coordinate = std::vector< double >;
using CoordinateArray = std::vector< Coordinate >;
using Function = std::function< double( Coordinate const & ) >;
using FunctionGradient = std::function< Coordinate( Coordinate const & ) >;
