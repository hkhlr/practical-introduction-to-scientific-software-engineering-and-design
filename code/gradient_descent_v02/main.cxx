// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "Types.h"

#include "GradientDescent.h"

int main( int argc, char *argv[] )
{
    /*
     * The function to be minimized.
     */
    auto f = []( Coordinate coords ) -> typename Coordinate::value_type
    {
        auto const x{ coords.at( 0 ) };
        auto const y{ coords.at( 1 ) };
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    /*
     * Gradient fo the function to be minimized.
     */
    auto grad_f = []( Coordinate coords ) -> Coordinate
    {
        auto const x{ coords.at( 0 ) };
        auto const y{ coords.at( 1 ) };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    /*
     * Initial coordinates and learning rate.
     */
    Coordinate const starting_pos = { -1.0, 2.50 };
    double const step_size = 3e-1;

    /*
     * Use command line argument to specify which GD "flavour" to use.
     * Default (no argument passed): 0 --> "vanilla" GD
     */
    int const implementation_to_use = ( argc > 1 ) ? std::stoi( argv[ 1 ] ) : 0;

    std::unique_ptr< GradientDescent > gd;
    switch ( implementation_to_use )
    {
    case 1:
        gd = std::make_unique< MomentumGradientDescent >( f, grad_f, //
                                                          starting_pos,
                                                          step_size );
        break;
    case 0:
    default:
        gd = std::make_unique< VanillaGradientDescent >( f, grad_f, //
                                                         starting_pos,
                                                         step_size );
        break;
    }

    gd->run();

    std::cout << "Final coordinates: x = " << gd->coordinates()[ 0 ]
              << ", y = " << gd->coordinates()[ 1 ] << "\n";

    return EXIT_SUCCESS;
}
