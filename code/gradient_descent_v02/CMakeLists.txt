# SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
#
# SPDX-License-Identifier: MIT

set(THIS_STEP_VERSION_SUFFIX -v02)

add_executable( GradientDescent${THIS_STEP_VERSION_SUFFIX}.exe main.cxx GradientDescent.cxx )
target_link_libraries( GradientDescent${THIS_STEP_VERSION_SUFFIX}.exe PRIVATE matplotlibcpp17::matplotlibcpp17)
target_include_directories( GradientDescent${THIS_STEP_VERSION_SUFFIX}.exe         PRIVATE
        ${PYTHON_INCLUDE_DIRS}
        ${pybind11_INCLUDE_DIRS}
)

# pybind11 code should be compiled with -fvisibility=hidden
target_compile_options(GradientDescent${THIS_STEP_VERSION_SUFFIX}.exe PRIVATE -fvisibility=hidden)

add_subdirectory( tests )