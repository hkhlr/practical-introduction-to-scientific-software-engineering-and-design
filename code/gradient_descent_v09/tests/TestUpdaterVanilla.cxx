// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include "catch2/generators/catch_generators.hpp"
#include <catch2/catch_test_macros.hpp>

#include <memory>

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

#include "TestInfrastructure.h"

TEST_CASE( "Updater", "[vanilla]" )
{

    Coordinate position = { 0.0, 0.0 };
    Coordinate gradient = { -1.0, -1.0 };

    auto step_size =
        GENERATE( as< double >{}, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2 );

    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< VanillaUpdater >( step_size );
    updater->update_coordinates( position, gradient );
    REQUIRE( ( position[ 0 ] == step_size && position[ 1 ] == step_size ) );
}
