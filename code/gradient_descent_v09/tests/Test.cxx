// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include "catch2/generators/catch_generators.hpp"
#include <catch2/catch_test_macros.hpp>

#include <memory>
#include <tuple>

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

#include "TestInfrastructure.h"

/*
Convergence behaviour (helpers)
*/
static void test_converged_input(
    std::unique_ptr< CoordinateUpdater > updater,
    std::tuple< Function, FunctionGradient, Coordinate > function_info )
/*
The algorithm should *not* make any iteration since the input coordinates are
already the minimum position.
*/
{
    auto const &[ f, f_grad, minimum ] = function_info;
    {
        auto gtol = GENERATE( as< double >{}, 1e-2, 1e-3, 1e-4 );
        // put in already the minimum
        Coordinate start_position = minimum;
        auto const result_steps = run_gradient_decent(
            f, f_grad, start_position, std::move( updater ), gtol, 100 );
        REQUIRE( result_steps.size() == 1 );
        // only the first iteration where we concluded that we found the minimum
        REQUIRE( start_position == minimum );
        REQUIRE( result_steps[ 0 ] == minimum );
    }
}

static void test_find_minimum(
    std::unique_ptr< CoordinateUpdater > updater, Coordinate start_position,
    std::tuple< Function, FunctionGradient, Coordinate > function_info )
/*
Check if the correct minimum is found for a given function and its gradient.
*/
{
    size_t const max_iter = 5000ul;
    auto const &[ f, f_grad, minimum ] = function_info;

    auto gtol = GENERATE( as< double >{}, 1e-2, 1e-3, 1e-4 );

    auto const result_steps = run_gradient_decent(
        f, f_grad, start_position, std::move( updater ), gtol, max_iter );
    /*See comment above why compare to max_iter + 1*/
    REQUIRE( result_steps.size() < max_iter + 1 );
    /*
    Choosing the tolerance here is not a easy task. Because we use the l2-norm
    of the gradient to stop the GD iterations we have no direct measure how
    accurate the coordinates actually should be.
    */
    REQUIRE( coordinatesAreClose( start_position, minimum, gtol, gtol ) );
}

/*
COMMENT: Instead of using prefixes like "testfn0" (and alike for the other test
functions) we could also work with namespaces, e.g.

namespace function0
{
    // Code for all tests related to this test function.
}
*/

/*
Test function 0
*/
static void testfn0_single_iter( std::unique_ptr< CoordinateUpdater > updater )
{
    const double gtol = 1e-3;
    auto start_value = GENERATE( as< double >{}, 2, 4, 6, 8, 10 );
    Coordinate start_position = { start_value, start_value };
    Coordinate minimum = { 0.0, 0.0 };
    auto result_steps =
        run_gradient_decent( function0, function0_gradient, start_position,
                             std::move( updater ), gtol, 100 );
    REQUIRE( result_steps.size() == 2 );
    // only one step to the minimum
    REQUIRE( coordinatesAreClose( start_position, minimum ) );
    REQUIRE( result_steps[ 0 ] == Coordinate{ start_value, start_value } );
    REQUIRE( result_steps[ 1 ] == start_position );
}

static void
testfn0_not_enough_iter( std::unique_ptr< CoordinateUpdater > updater )
{
    auto start_position =
        GENERATE( as< Coordinate >{}, Coordinate{ 1.5, 0 },
                  Coordinate{ 0, 1.5 }, Coordinate{ 1.125, 4 } );
    auto max_iter = GENERATE( as< unsigned >{}, 0, 1, 2, 3, 4 );
    const double gtol = 1e-4;

    Coordinate minimum = { 0.0, 0.0 };
    auto result_steps =
        run_gradient_decent( function0, function0_gradient, start_position,
                             std::move( updater ), gtol, max_iter );
    /*
    Here we compare the size of the output array that holds the coordinates of
    all visited points. Since the initial (starting) coordinates are also
    contained inside this array we have to test if the size == max_iter + 1.
    */
    REQUIRE( result_steps.size() == max_iter + 1 );
    REQUIRE( not coordinatesAreClose( start_position, minimum ) );
}

static void test_func_0( std::unique_ptr< CoordinateUpdater > updater )
{
    SECTION( "find minimum" )
    {
        auto start_position =
            GENERATE( as< Coordinate >{}, Coordinate{ 1, 0 },
                      Coordinate{ 0, 1 }, Coordinate{ 1, 1 } );
        test_find_minimum( std::move( updater ), start_position,
                           { function0, function0_gradient, { 0.0, 0.0 } } );
    }
    SECTION( "converged input" )
    {
        test_converged_input( std::move( updater ),
                              { function0, function0_gradient, { 0.0, 0.0 } } );
    }
}

/*
Test function 1
*/
static void test_func_1( std::unique_ptr< CoordinateUpdater > updater )
{
    SECTION( "find minimum" )
    {
        auto start_position = GENERATE(
            as< Coordinate >{}, Coordinate{ 2, 4 }, Coordinate{ 1, 4 },
            Coordinate{ 2, 3 }, Coordinate{ 1, 3 }, Coordinate{ 3, 4 },
            Coordinate{ 2, 5 }, Coordinate{ 3, 5 } );
        test_find_minimum( std::move( updater ), start_position,
                           { function1, function1_gradient, { 2.0, 4.0 } } );
    }
    SECTION( "converged input" )
    {
        test_converged_input( std::move( updater ),
                              { function1, function1_gradient, { 2.0, 4.0 } } );
    }
}

/*
Test function 2
*/
static void test_func_2( std::unique_ptr< CoordinateUpdater > updater )
{
    SECTION( "find minimum" )
    {
        auto start_position = GENERATE(
            as< Coordinate >{}, Coordinate{ 1, 3 }, Coordinate{ .0, 3 },
            Coordinate{ 1, 2 }, Coordinate{ .0, 2 }, Coordinate{ 2, 3 },
            Coordinate{ 1, 4 }, Coordinate{ 2, 4 } );
        test_find_minimum( std::move( updater ), start_position,
                           { function2, function2_gradient, { 1.0, 3.0 } } );
    }
    SECTION( "converged input" )
    {
        test_converged_input( std::move( updater ),
                              { function2, function2_gradient, { 1.0, 3.0 } } );
    }
}

/*
Convergence behaviour
*/
TEST_CASE( "Convergence", "[vanilla]" )
{
    auto const learning_rate = 0.1;
    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< VanillaUpdater >( learning_rate );
    SECTION( "func 0" ) { test_func_0( std::move( updater ) ); }
    SECTION( "func 1" ) { test_func_1( std::move( updater ) ); }
    SECTION( "func 2" ) { test_func_2( std::move( updater ) ); }
}

TEST_CASE( "Convergence", "[momentum]" )
{
    auto const learning_rate = 0.1;
    auto const alpha = 0.1;

    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );
    SECTION( "func 0" ) { test_func_0( std::move( updater ) ); }
    SECTION( "func 1" ) { test_func_1( std::move( updater ) ); }
    SECTION( "func 2" ) { test_func_2( std::move( updater ) ); }
}

/*
Some special cases chosen for particular methods
*/
TEST_CASE( "hand picked special cases", "[vanilla]" )
{
    SECTION( "Single iteration" )
    {
        auto const learning_rate = 0.5;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );
        testfn0_single_iter( std::move( updater ) );
    }
    SECTION( "not enough iterations" )
    {
        auto const learning_rate = 0.25;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );
        testfn0_not_enough_iter( std::move( updater ) );
    }
}

TEST_CASE( "hand picked special cases", "[momentum]" )
{

    SECTION( "Single iteration" )
    {
        auto const learning_rate = 0.5;
        auto const alpha = 0.1;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );
        testfn0_single_iter( std::move( updater ) );
    }
    SECTION( "not enough iterations" )
    {
        auto const learning_rate = 0.25;
        auto const alpha = 0.1;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );
        testfn0_not_enough_iter( std::move( updater ) );
    }
}
