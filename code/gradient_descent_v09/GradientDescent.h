// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#ifndef GRADIENT_DECENT_H_INCLUDE_GUARD
#define GRADIENT_DECENT_H_INCLUDE_GUARD

#include <cmath>
#include <iostream>
#include <memory>
#include <numeric>
#include <vector>

#include "Types.h"
#include "Updater.h"

CoordinateArray run_gradient_decent(
    const Function &f, const FunctionGradient &gradf, Coordinate &coords,
    std::unique_ptr< CoordinateUpdater > updater, double gtol = 1e-3,
    unsigned long max_iterations = 1000ul );
#endif // GRADIENT_DECENT_H_INCLUDE_GUARD
