<!--
SPDX-FileCopyrightText: © 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Development steps

## Overall Goals

### Primary

* Extend the code with a new gradient descent variant
* Maintain correctness of existing variants

### Secondary

* Refactor code to remove code redundancy
* Improve readability
* Establish (automated) tests for existing parts of the code.

## What have we accomplished so far?

### [`gradient_descent_v01`](../gradient_descent_v01)
* Placed code under version control.
* Add basic black-box tests to make sure final result is correct.
   * Refer to [`tests`](./tests/) directory.

### [`gradient_descent_v02`](../gradient_descent_v02)
* Dedicated file [`main.cxx`](./main.cxx) that contains driver code.
* Source ([`GradientDescent.cxx`](./GradientDescent.cxx)) and header file ([`GradientDescent.h`](./GradientDescent.h)) for GD code.
* Type aliases in separate header file ([`Types.h`](./Types.h)).

### [`gradient_descent_v03`](../gradient_descent_v03)

* Remove plotting functionality *only* from vanilla GD code.
    * Inspect the [`GradientDescent.cxx`](./GradientDescent.cxx) source file to convince yourself that it still is present inside the `run` method of the `MomentumGradientDescent` class.
* Move code for plotting into separate source and header file.
    * Source file: [`Plotter.cxx`](./Plotter.cxx)
    * Header file: [`Plotter.h`](./Plotter.h)
* Driver code inside `main` function will does (not yet) use the plotting code.

### [`gradient_descent_v04`](../gradient_descent_v04/)

* Remove plotting code also for momentum GD.
* Make the driver code in `main` also use the *externalised* plotting functionality.

### [`gradient_descent_v05`](../gradient_descent_v05/)

* Introduce a *free function* for running the GD iterations (`run_gradient_descent`) instead of the class hierachy.
    * Refer to the files [`GradientDescent.h`](./GradientDescent.h) and [`GradientDescent.cxx`](./GradientDescent.cxx).
* Introduce an "updater" abstraction `CoordinateUpdater` to manage the logic of the coordinate updates. The following *concrete* classes have been provided (refer to the file [`Updater.h`](./Updater.h)):
    * `class VanillaUpdater final : public CoordinateUpdater;`
    * `class MomentumUpdater final : public CoordinateUpdater;`
* The `run_gradient_descent` function receives a `CoordinateUpdater` and calls it at the appropriate position inside the `while` loop.
* The duplicate computing the $\ell_2$-norm of the gradient and checking for convergence has been removed by using a lamba expression.

### [`gradient_descent_v06`](../gradient_descent_v06)

* Establish some tests for vanilla GD
    * Refer to source file [`tests/TestVanilla.cxx`](tests/TestVanilla.cxx)
    * Modify the build system to compile the test file (refer to [`tests/CMakeLists.txt`](./tests/CMakeLists.txt))
        * Build test executable `TestVanilla-v06`
    * Convergence behaviour for different test functions 
        * Position of minimum as input $\Rightarrow$ No iterations!
        * Choose starting position such that minimum is found in a single iteration
        * Number of allowed iterations too small $\Rightarrow$ Iterations not converged
        * Algorithm finds correct local minimum
    * Test single update steps

### [`gradient_descent_v07`](../gradient_descent_v07)

* Add new test skeleton (source file and entry in build system) to prepare the tests for momentum GD
* Extract some common functionality that are probably required for both tests:
    * Definitions of the Functions and gradient Functions used for testing
    * Helper functions to compare coordinates within rounding errors

### [`gradient_descent_v08`](../gradient_descent_v08/)

* Set up tests for momentum GD
    * Refer to source file [`tests/TestMomentum.cxx`](./tests/TestMomentum.cxx)
* Add tests for momentum updater
    * Refer to source file [`tests/TestMomentum.cxx`](./tests/TestMomentum.cxx)

### [`gradient_descent_v09`](../gradient_descent_v09)

* Separated tests for *updaters* in dedicated files:
    * [`tests/TestUpdaterVanilla.cxx`](tests/TestUpdaterVanilla.cxx)
    * [`tests/TestUpdaterMomentum.cxx`](tests/TestUpdaterMomentum.cxx)
* Tests for the GD iterations (or those that equally apply to all updaters) have been collected inside [`tests/Test.cxx`](tests/Test.cxx)
    * Provided test helper functions taking an updater as input parameters


### [`gradient_descent_v10`](../gradient_descent_v10)

* Add another coordinate update method: AdaGrad (refer to slides on AdaGrad method)
    * Implement an additional updater to represent this method (updated the [`Updater.h`](./Updater.h) header file)
* Tests for AdaGrad updater (refer to source file [`tests/TestUpdaterAdaGrad.cxx`](./tests/TestUpdaterAdaGrad.cxx))
* Add tests for GD iterations with AdaGrad coordinate updater (refer to source file [`tests/Test.cxx`](./tests/Test.cxx))
    * Used `namespace`s instead of longer function names (compare to [`gradient_descent_v09/tests/Test.cxx`](../gradient_descent_v09/tests/Test.cxx) )


## What else could we do?

* Extend tests to higher dimensions
    * Currently we only test 2-dimensional functions
* If required, add another coordinate update flavour