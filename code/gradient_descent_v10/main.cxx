// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "GradientDescent.h"
#include "Plotter.h"
#include "Types.h"
#include "Updater.h"

int main( int argc, char *argv[] )
{
    // the function to minimize
    auto f = []( std::vector< double > coords ) -> double
    {
        double const x = coords[ 0 ];
        double const y = coords[ 1 ];
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( std::vector< double > coords ) -> std::vector< double >
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    Coordinate position = { -1.0, 2.50 };
    double step_size = 3e-1;

    /*
    Use command line argument to switch between different coordinate update
    flavours.
    */
    int implementation_to_use = ( argc > 1 ) ? std::stoi( argv[ 1 ] ) : 0;

    std::unique_ptr< CoordinateUpdater > updater;
    switch ( implementation_to_use )
    {
    case 2:
        updater = std::make_unique< AdaGradUpdater >(
            /*step_size=*/1.1e+1 * step_size, 2ul );
        std::cout << "\tWill be using AdaGrad coordinate update...\n";
        break;
    case 1:
        updater = std::make_unique< MomentumUpdater >( /*alpha=*/0.25,
                                                       step_size, 2ul );
        std::cout << "\tWill be using Momentum coordinate update...\n";
        break;
    case 0:
    default:
        updater = std::make_unique< VanillaUpdater >( step_size );
        std::cout << "\tWill be using Vanilla coordinate update...\n";
        break;
    }

    // we "move" the updater into the gradient decent function
    // meaning that the gradient decent function now has ownership
    // as the updater may hold some internal state, we can not use it again
    auto const steps_taken =
        run_gradient_decent( f, grad_f, position, std::move( updater ) );
    assert( updater == nullptr ); // as we used std::move, we lost ownership

    std::cout << "Final coordinates: x = " << position[ 0 ]
              << ", y = " << position[ 1 ] << "\n";

    auto plotter = PlotterFactory();

    if ( plotter != nullptr )
    {
        plotter->plot_gradient_decent( f, steps_taken );
    }

    return 0;
}
