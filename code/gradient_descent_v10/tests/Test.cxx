// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include "catch2/generators/catch_generators.hpp"
#include <catch2/catch_test_macros.hpp>

#include <memory>

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

#include "TestInfrastructure.h"

/*
Helper functions needed below.
*/
namespace Details
{
void testConvergedInput( std::unique_ptr< CoordinateUpdater > updater,
                         const Function f, const FunctionGradient f_grad,
                         const Coordinate minimum )
{
    {
        auto gtol = GENERATE( as< double >{}, 1e-2, 1e-3, 1e-4 );
        // put in already the minimum
        Coordinate start_position = minimum;
        auto result_steps = run_gradient_decent(
            f, f_grad, start_position, std::move( updater ), gtol, 100 );
        REQUIRE( result_steps.size() == 1 );
        // only the first iteration where we concluded that we found the minimum
        REQUIRE( start_position == minimum );
        REQUIRE( result_steps[ 0 ] == minimum );
    }
}

void testFindMinimum( std::unique_ptr< CoordinateUpdater > updater,
                      Coordinate start_position, const Function f,
                      const FunctionGradient f_grad, const Coordinate minimum )
{
    const unsigned max_iter = 5000;
    auto gtol = GENERATE( as< double >{}, 1e-2, 1e-3, 1e-4 );

    auto result_steps = run_gradient_decent(
        f, f_grad, start_position, std::move( updater ), gtol, max_iter );
    // enough to reach minimum
    REQUIRE( result_steps.size() < max_iter + 1 );
    // tolerance for test should be the same that was given to the algorithm
    // to determine if one should stop
    REQUIRE( coordinatesAreClose( start_position, minimum, gtol, gtol ) );
}
} // namespace Details

namespace Function0
{
void test_single_iter( std::unique_ptr< CoordinateUpdater > updater )
{
    const double gtol = 1e-3;
    auto start_value = GENERATE( as< double >{}, 2, 4, 6, 8, 10 );
    Coordinate start_position = { start_value, start_value };
    Coordinate minimum = { 0.0, 0.0 };
    auto result_steps =
        run_gradient_decent( function0, function0_gradient, start_position,
                             std::move( updater ), gtol, 100 );
    REQUIRE( result_steps.size() == 2 );
    // only one step to the minimum
    REQUIRE( coordinatesAreClose( start_position, minimum ) );
    REQUIRE( result_steps[ 0 ] == Coordinate{ start_value, start_value } );
    REQUIRE( result_steps[ 1 ] == start_position );
}

void testNotEnoughIterations( std::unique_ptr< CoordinateUpdater > updater )
{
    auto start_position =
        GENERATE( as< Coordinate >{}, Coordinate{ 1.5, 0 },
                  Coordinate{ 0, 1.5 }, Coordinate{ 1.125, 4 } );
    auto max_iter = GENERATE( as< unsigned >{}, 0, 1, 2, 3, 4 );
    const double gtol = 1e-4;

    Coordinate minimum = { 0.0, 0.0 };
    auto result_steps =
        run_gradient_decent( function0, function0_gradient, start_position,
                             std::move( updater ), gtol, max_iter );
    // not enough to reach minimum
    REQUIRE( result_steps.size() == max_iter + 1 );
    REQUIRE( not coordinatesAreClose( start_position, minimum ) );
}

void testMinimumAndConvergedInput(
    std::unique_ptr< CoordinateUpdater > updater )
{
    using namespace Details;
    SECTION( "find minimum" )
    {
        auto start_position =
            GENERATE( as< Coordinate >{}, Coordinate{ 1, 0 },
                      Coordinate{ 0, 1 }, Coordinate{ 1, 1 } );
        testFindMinimum( std::move( updater ), start_position, function0,
                         function0_gradient, { 0.0, 0.0 } );
    }
    SECTION( "converged input" )
    {
        testConvergedInput( std::move( updater ), function0, function0_gradient,
                            { 0.0, 0.0 } );
    }
}
} // namespace Function0

namespace Function1
{
void testMinimumAndConvergedInput(
    std::unique_ptr< CoordinateUpdater > updater )
{
    using namespace Details;
    SECTION( "find minimum" )
    {
        auto start_position = GENERATE(
            as< Coordinate >{}, Coordinate{ 2, 4 }, Coordinate{ 1, 4 },
            Coordinate{ 2, 3 }, Coordinate{ 1, 3 }, Coordinate{ 3, 4 },
            Coordinate{ 2, 5 }, Coordinate{ 3, 5 } );
        testFindMinimum( std::move( updater ), start_position, function1,
                         function1_gradient, { 2.0, 4.0 } );
    }
    SECTION( "converged input" )
    {
        testConvergedInput( std::move( updater ), function1, function1_gradient,
                            { 2.0, 4.0 } );
    }
}
} // namespace Function1

namespace Function2
{
void testMinimumAndConvergedInput(
    std::unique_ptr< CoordinateUpdater > updater )
{
    using namespace Details;
    SECTION( "find minimum" )
    {
        auto start_position = GENERATE(
            as< Coordinate >{}, Coordinate{ 1, 3 }, Coordinate{ .0, 3 },
            Coordinate{ 1, 2 }, Coordinate{ .0, 2 }, Coordinate{ 2, 3 },
            Coordinate{ 1, 4 }, Coordinate{ 2, 4 } );
        testFindMinimum( std::move( updater ), start_position, function2,
                         function2_gradient, { 1.0, 3.0 } );
    }
    SECTION( "converged input" )
    {
        testConvergedInput( std::move( updater ), function2, function2_gradient,
                            { 1.0, 3.0 } );
    }
}
} // namespace Function2

TEST_CASE( "Convergence", "[vanilla]" )
{
    auto learning_rate = 0.1;
    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< VanillaUpdater >( learning_rate );
    SECTION( "func 0" )
    {
        Function0::testMinimumAndConvergedInput( std::move( updater ) );
    }
    SECTION( "func 1" )
    {
        Function1::testMinimumAndConvergedInput( std::move( updater ) );
    }
    SECTION( "func 2" )
    {
        Function2::testMinimumAndConvergedInput( std::move( updater ) );
    }
}

TEST_CASE( "Convergence", "[momentum]" )
{
    auto learning_rate = 0.1;
    auto alpha = 0.1;

    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );
    SECTION( "func 0" )
    {
        Function0::testMinimumAndConvergedInput( std::move( updater ) );
    }
    SECTION( "func 1" )
    {
        Function1::testMinimumAndConvergedInput( std::move( updater ) );
    }
    SECTION( "func 2" )
    {
        Function2::testMinimumAndConvergedInput( std::move( updater ) );
    }
}

TEST_CASE( "hand picked special cases", "[vanilla]" )
{
    SECTION( "Single iteration" )
    {
        auto const learning_rate = 0.5;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );
        Function0::test_single_iter( std::move( updater ) );
    }
    SECTION( "not enough iterations" )
    {
        auto const learning_rate = 0.25;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< VanillaUpdater >( learning_rate );
        Function0::testNotEnoughIterations( std::move( updater ) );
    }
}

TEST_CASE( "hand picked special cases", "[momentum]" )
{

    SECTION( "Single iteration" )
    {
        auto const learning_rate = 0.5;
        auto const alpha = 0.1;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );
        Function0::test_single_iter( std::move( updater ) );
    }
    SECTION( "not enough iterations" )
    {
        auto const learning_rate = 0.25;
        auto const alpha = 0.1;
        std::unique_ptr< CoordinateUpdater > updater =
            std::make_unique< MomentumUpdater >( alpha, learning_rate, 2 );
        Function0::testNotEnoughIterations( std::move( updater ) );
    }
}

TEST_CASE( "Convergence", "[AdaGrad]" )
{
    auto const learning_rate = 0.1;
    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< AdaGradUpdater >( learning_rate, 2 );
    SECTION( "func 0" )
    {
        Function0::testMinimumAndConvergedInput( std::move( updater ) );
    }
    SECTION( "func 1" )
    {
        Function1::testMinimumAndConvergedInput( std::move( updater ) );
    }
    SECTION( "func 2" )
    {
        Function2::testMinimumAndConvergedInput( std::move( updater ) );
    }
}

// the hand picked special cases are picked for vanilla gradient decent
// theey are also aplicable for momentum if no momentum had build up yet
// but they cannot be used for other strategies
