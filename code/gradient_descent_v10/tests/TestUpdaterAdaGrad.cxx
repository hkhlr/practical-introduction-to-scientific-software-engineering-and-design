// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <catch2/catch_test_macros.hpp>

#include <catch2/generators/catch_generators.hpp>
#include <catch2/generators/catch_generators_adapters.hpp>

#include <cmath>
#include <iomanip>
#include <memory>
#include <random>

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

#include "TestInfrastructure.h"

namespace
{
class RandomCoordinateGenerator final
    : public Catch::Generators::IGenerator< Coordinate >
{
  public:
    explicit RandomCoordinateGenerator(
        size_t size, std::pair< int, int > bounds = { -1000, 1000 } )
        : random_device_{ std::random_device{} }
        , gen_{ random_device_() }
        , random_distr_{ bounds.first, bounds.second }
        , current_coord_( size, 1 )
    {
    }

    Coordinate const &get() const override { return current_coord_; }
    bool next() override
    {
        std::generate( current_coord_.begin(), current_coord_.end(),
                       [ this ]()
                       { return this->random_distr_( this->gen_ ); } );
        return true;
    }

  private:
    std::random_device random_device_{};
    std::mt19937 gen_{};
    std::uniform_int_distribution<> random_distr_{ -1000, 1000 };
    Coordinate current_coord_{};
};

Catch::Generators::GeneratorWrapper< Coordinate >
randomCoordinates( size_t size, std::pair< int, int > bounds = { -1000, 1000 } )
{
    return Catch::Generators::GeneratorWrapper< Coordinate >(
        Catch::Detail::make_unique< RandomCoordinateGenerator >(
            size, std::move( bounds ) ) );
}

} // namespace

TEST_CASE( "Updater steps", "[adaGrad]" )
{

    /*
    Coordinate update goes like

    x := x - learning_rate / ( sqrt( g2_sum ) + epsilon ) * g

    */
    SECTION( "single update step" )
    {
        /*
        To keep things simple here we will use a gradient with uniform
        components most of the time.
        */
        Coordinate const grad{ 1, 1 };

        SECTION( "zero learning rate" )
        {
            constexpr double step_size = 0;
            Coordinate coord{ 1, 1 };
            auto updater = std::make_unique< AdaGradUpdater >(
                step_size, /*dimension*/ 2ul );
            updater->update_coordinates( coord, grad );
            /*
            Coordinates should not have changed.
            */
            REQUIRE( std::all_of( coord.begin(), coord.end(),
                                  [ ref = 1.0 ]( auto x )
                                  { return isClose( x, ref ); } ) );
        }

        SECTION( "variable learning rates" )
        {
            auto step_size = GENERATE( as< double >{}, 1 /*special value*/,
                                       1e-1, 2e-1, 3e-1, 4e-1, 5e-1 );
            constexpr double epsilon = 0;
            Coordinate coord_initial{ 1, 1 }, coord{ coord_initial };
            auto updater =
                std::make_unique< AdaGradUpdater >( step_size, 2ul, epsilon );
            /*Single update step*/
            updater->update_coordinates( coord, grad );
            REQUIRE( (
                [ = ]() mutable /*to be allowed to mutate coord_initial*/
                -> bool
                {
                    /*
                    With a single step and g == 1 in all components the term to
                    be subtracted from the coordinates in the first step is
                    (assuming epsilon == 0):

                    // coordinate index does not matter
                      - learning_rate * g / ( sqrt( g^2 ) + 0 );
                    = - learning_rate * g / g
                    = - learning_rate
                    */
                    Coordinate const delta( coord.size(), step_size /*== 1*/ );
                    std::transform( coord_initial.begin(), coord_initial.end(),
                                    delta.begin(), coord_initial.begin(),
                                    []( auto x, auto d ) { return x - d; } );
                    return std::equal( coord.begin(), coord.end(),
                                       coord_initial.begin(),
                                       []( auto x, auto xref ) -> bool
                                       { return isClose( x, xref ); } );
                }() ) );
        }
    }

    SECTION( "multiple update steps" )
    {
        auto num_iterations =
            GENERATE( as< size_t >{}, 1, 2, 3, 5, 10, 50, 100 );

        SECTION( "unit learning rate" )
        {
            constexpr double step_size = 1, epsilon = 0;
            Coordinate coord_initial{ 1, 1 }, coord{ coord_initial },
                grad{ 1, 1 };
            auto updater =
                std::make_unique< AdaGradUpdater >( step_size, 2ul, epsilon );
            /*
            Make the update steps
            */
            for ( size_t iter = 1; iter <= num_iterations; ++iter )
            {
                for ( auto idx = 0ul; auto &x : coord_initial )
                {
                    /*
                    When iterations progress the sqrt expression at the Nth
                    iteration is (gradient == 1 in all components):

                    sqrt( 1^2 + 1^2 + 1^2 + ... +  )
                    = sqrt( N * 1  ) = sqrt( N )
                    */
                    x -= 1.0 /*grad == 1, step_size == 1*/ / std::sqrt( iter );
                }
                updater->update_coordinates( coord, grad );
                REQUIRE( std::equal( coord_initial.begin(), coord_initial.end(),
                                     coord.begin(),
                                     []( auto ref, auto x ) -> bool
                                     { return isClose( x, ref ); } ) );
            }
        }

        SECTION( "different learning rates" )
        {
            auto step_size =
                GENERATE( as< double >{}, 1e-1, 2e-1, 3e-1, 4e-1, 5e-1 );
            constexpr double epsilon = 0;
            Coordinate coord_initial{ 1, 1 }, coord{ coord_initial },
                grad{ 1, 1 };
            auto updater =
                std::make_unique< AdaGradUpdater >( step_size, 2ul, epsilon );
            /*
            Make the update steps
            */
            for ( size_t iter = 1; iter <= num_iterations; ++iter )
            {
                for ( auto idx = 0ul; auto &x : coord_initial )
                {
                    x -= /*grad == 1*/ step_size / std::sqrt( iter );
                }
                updater->update_coordinates( coord, grad );
                REQUIRE( std::equal( coord_initial.begin(), coord_initial.end(),
                                     coord.begin(),
                                     []( auto ref, auto x ) -> bool
                                     { return isClose( x, ref ); } ) );
            }
        }

        auto grad =
            GENERATE( take( 10, randomCoordinates( 2ul, { -1000, 1000 } ) ) );
        auto coord_initial = GENERATE( take( 5, randomCoordinates( 2ul ) ) );
        SECTION( "vary gradient" )
        {
            constexpr double step_size = 1, epsilon = 0;
            /*
            Floating point arithmetic can be really annoying from time to
            time...
            */
            constexpr double atol = 1e-7, rtol = 1e-6;
            Coordinate coord{ coord_initial };
            // std::cout << "\tgrad_x = " << grad.at( 0 )
            //           << ", grad_y = " << grad.at( 1 ) << "\n";
            auto updater =
                std::make_unique< AdaGradUpdater >( step_size, grad.size() );
            /*
            Make the update steps
            */
            for ( auto iter = 1ul; iter <= num_iterations; ++iter )
            {
                for ( auto idx = 0ul; auto &x : coord_initial )
                {
                    x -= grad.at( idx ) /*step_size == 1*/ /
                         std::sqrt( iter * grad.at( idx ) * grad.at( idx ) );
                    ++idx;
                }
                updater->update_coordinates( coord, grad );
                // if ( !std::equal( coord_initial.begin(), coord_initial.end(),
                //                   coord.begin(),
                //                   [ &atol, &rtol ]( auto ref, auto x ) ->
                //                   bool { return isClose( x, ref, atol, rtol
                //                   ); } ) )
                // {
                //     std::printf( "coord        : %.12e, %.12e\n", coord.at( 0
                //     ),
                //                  coord.at( 1 ) );
                //     std::printf( "coord_initial: %.12e, %.12e\n",
                //                  coord_initial.at( 0 ), coord_initial.at( 1 )
                //                  );
                // }
                REQUIRE( std::equal(
                    coord_initial.begin(), coord_initial.end(), coord.begin(),
                    [ &atol, &rtol ]( auto ref, auto x ) -> bool
                    { return isClose( x, ref, atol, rtol ); } ) );
            }
        }
    }
}

TEST_CASE( "Updater", "[adaGrad]" )
{
    Coordinate position = { 0.0, 0.0 };
    Coordinate gradient = { -1.0, 0 };

    auto step_size =
        GENERATE( as< double >{}, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2 );
    auto num_iters = GENERATE( as< int >{}, 1, 2, 4, 6, 8, 16, 32, 64 );

    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< AdaGradUpdater >( step_size, 2 );

    // "saturate" the gradient in one direction
    for ( int i = 0; i < num_iters; ++i )
    {
        updater->update_coordinates( position, gradient );
    }
    REQUIRE( isClose( position[ 1 ], 0 ) );
    // should not move in y direction

    // change direction of gradient: also add the y component
    gradient = { -1.0, -1.0 };
    Coordinate prev_position = position;
    updater->update_coordinates( position, gradient );

    REQUIRE( std::abs( position[ 0 ] - prev_position[ 0 ] ) <
             std::abs( position[ 1 ] - prev_position[ 1 ] ) );
    // gradient component in Y direction is more important as we already
    // have moved in x
}

TEST_CASE( "Updater stands still", "[adaGrad]" )
{
    Coordinate position = { 0.0, 0.0 };
    Coordinate gradient = { -1.0, -1.0 };

    auto step_size = 0;
    auto num_iters = GENERATE( as< int >{}, 1, 2, 4, 6, 8, 16, 32, 64 );

    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< AdaGradUpdater >( step_size, 2 );

    for ( int i = 0; i < num_iters; ++i )
    {
        updater->update_coordinates( position, gradient );
    }
    REQUIRE( isClose( position[ 0 ], 0 ) );
    REQUIRE( isClose( position[ 1 ], 0 ) );
    // should not move
}
