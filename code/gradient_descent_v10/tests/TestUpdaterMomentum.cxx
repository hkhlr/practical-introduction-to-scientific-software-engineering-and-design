// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include "catch2/generators/catch_generators.hpp"
#include <catch2/catch_test_macros.hpp>

#include <memory>

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

#include "TestInfrastructure.h"

TEST_CASE( "Updater", "[momentum]" )
{
    Coordinate position = { 0.0, 0.0 };
    Coordinate gradient = { -1.0, -1.0 };

    auto step_size =
        GENERATE( as< double >{}, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2 );
    auto alpha =
        GENERATE( as< double >{}, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 );

    std::unique_ptr< CoordinateUpdater > updater =
        std::make_unique< MomentumUpdater >( alpha, step_size, 2 );

    SECTION( "single iteration" )
    {
        updater->update_coordinates( position, gradient );
        REQUIRE( ( position[ 0 ] == step_size && position[ 1 ] == step_size ) );
    }
    // the whole initailzation will be repeated for the other section

    auto num_iters = GENERATE( as< int >{}, 1, 2, 4, 6, 8, 16, 32, 64 );

    SECTION( "momentum grows" )
    {
        for ( int i = 0; i < num_iters; ++i )
        {
            updater->update_coordinates( position, gradient );
        } // growing the momentum
        auto position_test = position;
        updater->update_coordinates( position, gradient );
        // momentum should build up
        REQUIRE(
            ( std::abs( position[ 0 ] - position_test[ 0 ] ) > step_size &&
              std::abs( position[ 1 ] - position_test[ 1 ] ) > step_size ) );
    }
}

TEST_CASE( "Updater compare", "[momentum]" )
{
    Coordinate position = GENERATE(
        as< Coordinate >{}, Coordinate{ 0.0, 0.0 }, Coordinate{ 1.0, 1.0 },
        Coordinate{ 1.0, 0.0 }, Coordinate{ 0.0, 1.0 } );
    Coordinate position_reference = position;
    Coordinate gradient =
        GENERATE( as< Coordinate >{}, Coordinate{ -1.0, -1.0 },
                  Coordinate{ -1.0, 0.0 }, Coordinate{ 0.0, -1.0 } );

    auto step_size =
        GENERATE( as< double >{}, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2 );
    auto num_iters = GENERATE( as< int >{}, 1, 2, 4, 6, 8, 16, 32, 64 );

    SECTION( "same" )
    {
        double alpha =
            0; // special case: Vanilla and momentum should behave same

        std::unique_ptr< CoordinateUpdater > updater_vanilla =
            std::make_unique< VanillaUpdater >( step_size );

        std::unique_ptr< CoordinateUpdater > updater_momentum =
            std::make_unique< MomentumUpdater >( alpha, step_size, 2 );

        for ( int i = 0; i < num_iters; ++i )
        {
            updater_vanilla->update_coordinates( position_reference, gradient );
            updater_momentum->update_coordinates( position, gradient );
            REQUIRE( coordinatesAreClose( position, position_reference ) );
        }
    }

    SECTION( "momentum_grows" )
    {
        auto alpha = GENERATE( as< double >{}, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6,
                               0.7, 0.8, 0.9 );
        // if alpha is larger than one and we always go into the same direction
        // momentum should build up and we will me "ahead" of the vanilla
        // updater

        std::unique_ptr< CoordinateUpdater > updater_vanilla =
            std::make_unique< VanillaUpdater >( step_size );

        std::unique_ptr< CoordinateUpdater > updater_momentum =
            std::make_unique< MomentumUpdater >( alpha, step_size, 2 );

        for ( int i = 0; i < num_iters; ++i )
        {
            updater_vanilla->update_coordinates( position_reference, gradient );
            updater_momentum->update_coordinates( position, gradient );
            REQUIRE(
                // either same(close) or the momentum should be in the lead
                ( ( isClose( position[ 0 ], position_reference[ 0 ] ) ||
                    position[ 0 ] > position_reference[ 0 ] ) &&
                  ( isClose( position[ 1 ], position_reference[ 1 ] ) ||
                    position[ 1 ] > position_reference[ 1 ] ) ) );
        }
    }
}
