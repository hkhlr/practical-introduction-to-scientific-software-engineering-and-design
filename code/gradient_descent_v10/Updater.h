// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#ifndef UPDATER_H_INCLUDE_GUARD
#define UPDATER_H_INCLUDE_GUARD

#include <cmath>
#include <vector>

#include "Types.h"

class CoordinateUpdater
{
  public:
    virtual ~CoordinateUpdater() noexcept = default;
    // coordinates are given by reference, therefore no return value is needed,
    // as the coords will be updated
    virtual void update_coordinates( Coordinate &coordinates,
                                     Coordinate const &gradient ) = 0;
};

class VanillaUpdater final : public CoordinateUpdater
{
  public:
    explicit VanillaUpdater( double step_size = 1e-2 )
        : step_size_{ step_size }
    {
    }
    ~VanillaUpdater() noexcept override = default;

    void update_coordinates( Coordinate &coordinates,
                             Coordinate const &gradient ) override
    {
        std::transform( coordinates.begin(), coordinates.end(),
                        gradient.begin(), coordinates.begin(),
                        [ this ]( auto x, auto g )
                        { return x - this->step_size_ * g; } );
    }

  private:
    double const step_size_{};
};

class MomentumUpdater final : public CoordinateUpdater
{
  public:
    MomentumUpdater( double alpha, double step_size, unsigned dimensions )
        : alpha_{ alpha }
        , step_size_{ step_size }
        , momentum_( dimensions,
                     0 ) // initializes a vector with dimensions '0' entries
    {
    }
    ~MomentumUpdater() noexcept override = default;

    void update_coordinates( Coordinate &coordinates,
                             Coordinate const &gradient ) override
    {
        for ( unsigned long i = 0; i < coordinates.size(); ++i )
        {
            auto const delta =
                momentum_[ i ] * alpha_ - step_size_ * gradient[ i ];
            coordinates[ i ] += delta;
            momentum_[ i ] = delta;
        }
    }

  private:
    double const alpha_{};
    double const step_size_{};
    Coordinate momentum_{};
};

class AdaGradUpdater final : public CoordinateUpdater
{
  public:
    AdaGradUpdater( double step_size, unsigned dimensions,
                    double epsilon = 1e-7 )
        : step_size_{ step_size }
        , sqr_gradient_sum_( dimensions, 0 )
        , epsilon_( epsilon )
    {
    }
    ~AdaGradUpdater() noexcept override = default;

    void update_coordinates( Coordinate &coordinates,
                             Coordinate const &gradient ) override
    {
        // (https://www.deeplearningbook.org/contents/optimization.html)
        // Equations from 8.4 listing in chapter 8 of the Deep Learning Book
        for ( size_t idx = 0; idx < coordinates.size(); ++idx )
        {
            sqr_gradient_sum_[ idx ] += gradient[ idx ] * gradient[ idx ];
            double const delta =
                -step_size_ * gradient[ idx ] /
                ( std::sqrt( sqr_gradient_sum_[ idx ] ) + epsilon_ );
            coordinates[ idx ] += delta;
        }
        /*
        This is what the update looks like when using an STL algorithm.
        */
        // std::transform( coordinates.cbegin(), coordinates.cend(),
        //                 gradient.begin(), coordinates.begin(),
        //                 [ idx = 0ul, this ]( auto x, auto g ) mutable
        //                 {
        //                     this->sqr_gradient_sum_[ idx ] += g * g;
        //                     auto const delta =
        //                         -this->step_size_ * g /
        //                         ( std::sqrt( this->sqr_gradient_sum_[ idx ] )
        //                         +
        //                           epsilon_ );
        //                     ++idx;
        //                     return x + delta;
        //                 } );
    }

  private:
    double const step_size_{};
    Coordinate sqr_gradient_sum_{};
    double const epsilon_{ 1e-7 };
};

#endif // UPDATER_H_INCLUDE_GUARD
