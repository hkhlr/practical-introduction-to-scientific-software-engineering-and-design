// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <optional>
#include <vector>

class PublicPyPlotInterfaceConstructor
{
};
class PublicPyPlotInterface
{
};

class StatefullUpdater
{
    size_t maxHistory;
    std::vector< std::vector< double > > history;

  public:
    StatefullUpdater(){};
    std::vector< double > operator()( std::vector< double > input )
    {
        return input;
    }
    auto getTrailCopy()
    {
        // explicit copy here
        return history;
    }
    auto &trail() const { return history; }
};

template < class T >
class myVisualizer : public T
{
  public:
    std::vector< double > operator()( std::vector< double > input )
    {
        auto tmp = T::operator()( input );
        // Visualize here
        return tmp;
    }
};

template < typename t, class Updater, class Decider >
std::optional< t > iteration_driver( t start, Updater u, Decider d,
                                     size_t maxIter = 100 )
{
    t current = start;
    size_t iteration = 0;
    while ( !d( current ) )
    {
        if ( iteration > maxIter )
            return {};
        current = u( current );
    }
    return current;
}

int main()
{
    std::vector< double > start{ 1.0, 2.0, 3.0 };
    auto result = iteration_driver(
        start,
        myVisualizer< decltype( []( std::vector< double > pos )
                                { return pos; } ) >(),
        []( std::vector< double > pos ) { return 1; } );
    return 0;
}