// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

template < typename t, class Updater, class Decider >
std::pair< t, size_t > iteration_driver( t start, Updater u, Decider d,
                                         size_t maxIter = 100 )
{
    t current = start;
    size_t iteration = 0;
    while ( !d( current ) )
    {
        if ( iteration > maxIter )
            return {};
        current = u( current );
    }
    return std::make_pair( current, iteration );
}

template < class T >
class myVisualizer : public T
{
  public:
    template < typename ARGS >
    myVisualizer( ARGS &&args )
        : T( static_cast< ARGS && >( args ) )
    {
    }
    std::vector< double > operator()( std::vector< double > input )
    {
        auto tmp = T::operator()( input );
        // Visualize here
        return tmp;
    }
};

class NormalGradientUpdate
{
  public:
    NormalGradientUpdate(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        double step_size )
        : f_( f )
        , gradf_( gradf )
        , step_size_( step_size )
    {
    }

    std::vector< double > operator()( std::vector< double > in )
    {
        auto grad = gradf_( in );
        for ( unsigned idx = 0; idx < (unsigned)in.size(); ++idx )
        {
            in[ idx ] -= step_size_ * grad[ idx ];
        }
        return in;
    }

  protected:
    std::function< double( std::vector< double > ) > f_;
    std::function< std::vector< double >( std::vector< double > ) > gradf_;
    double step_size_;
};

class MomentumGradientUpdate
{
  public:
    MomentumGradientUpdate(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        double step_size, double alpha = 1.0 )
        : f_( f )
        , gradf_( gradf )
        , step_size_( step_size )
        , alpha_( alpha )
    {
    }

    std::vector< double > operator()( std::vector< double > coords_ )
    {
        // auto momentumGradientUpdater=
        // [getGradient=grad_f,alpha_=alpha,&momentum,step_size_](std::vector<double>
        // coords_){
        auto grad = gradf_( coords_ );
        for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
        {
            auto delta = momentum[ idx ] * alpha_ + step_size_ * grad[ idx ];
            coords_[ idx ] -= delta;
            momentum[ idx ] = delta;
        }
        return coords_;
    }

  protected:
    std::function< double( std::vector< double > ) > f_;
    std::function< std::vector< double >( std::vector< double > ) > gradf_;
    double step_size_, alpha_;
    std::vector< double > momentum;
};
using IterativeGradientSolver = iteration_driver;

int main( int argc, char *argv[] )
{
    // the function to minimize
    auto f = []( std::vector< double > coords ) -> double
    {
        double const x = coords[ 0 ];
        double const y = coords[ 1 ];
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( std::vector< double > coords ) -> std::vector< double >
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    auto isConverged = [ getGradient =
                             grad_f ]( std::vector< double > coords_ ) -> bool
    {
        double g_l2norm2 = 0;
        auto grad = getGradient( coords_ );
        for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];

        bool is_converged = std::sqrt( g_l2norm2 ) < 1e-3;
        return is_converged;
    };
    double step_size_ = 3e-1;
    auto normalGradientUpdater =
        [ getGradient = grad_f,
          step_size_ ]( std::vector< double > coords_ ) -> std::vector< double >
    {
        auto grad = getGradient( coords_ );
        for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
        {
            coords_[ idx ] -= step_size_ * grad[ idx ];
        }
        return coords_;
    };
    double alpha = 1e-1;
    std::vector< double > momentum;
    auto momentumGradientUpdater =
        [ getGradient = grad_f, alpha_ = alpha, &momentum,
          step_size_ ]( std::vector< double > coords_ )
    {
        auto grad = getGradient( coords_ );
        for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
        {
            auto delta = momentum[ idx ] * alpha_ + step_size_ * grad[ idx ];
            coords_[ idx ] -= delta;
            momentum[ idx ] = delta;
        }
        return coords_;
    };

    std::vector< double > starting_pos = { -1.0, 2.50 };

    // use command line argument to decide which version of gradient decent to
    // use
    int implementation_to_use = 0;
    if ( argc > 1 )
    {
        implementation_to_use = std::stoi( argv[ 1 ] );
    }

    //  std::unique_ptr< GradientDescent > gd;
    std::pair< std::vector< double >, double > result;
    switch ( implementation_to_use )
    {
    case 1:
    {
        NormalGradientUpdate ngu( f, grad_f, step_size_ );
        //   auto foo=ngu(std::vector<double>());
        //  auto updater=[&ngu](std::vector<double> coords){return
        //  ngu(coords);};

        // myVisualizer<decltype([&ngu](std::vector<double> coords){return
        // ngu(coords);})>(),
        // result = iteration_driver(starting_pos, [&ngu](std::vector<double>
        // coords){return ngu(coords);},  isConverged,     1000u);
        result = iteration_driver( starting_pos,
                                   myVisualizer< NormalGradientUpdate >( ngu ),
                                   isConverged, 1000u );
        break;
    }
    case 0:
    default:
    {
        result = iteration_driver( starting_pos, momentumGradientUpdater,
                                   isConverged, 1000u );
        MomentumGradientUpdate mgu( f, grad_f, step_size_ );
        // result =
        // iteration_driver(starting_pos,myVisualizer<decltype([&mgu](std::vector<double>
        // coords){return mgu(coords);})>,isConverged,1000u); result =
        // iteration_driver(starting_pos,[&mgu](std::vector<double>
        // coords){return mgu(coords);},isConverged,1000u);
        result = iteration_driver(
            starting_pos, myVisualizer< MomentumGradientUpdate >( mgu ),
            isConverged, 1000u );
    }
    break;
    }

    if ( result.second < 1000 )
        std::cout << "Final coordinates: x = " << result.first[ 0 ]
                  << ", y = " << result.first[ 1 ] << "\n";
    else
        std::cout << "Gradient Method did not converge in Max iter"
                  << std::endl;

    return 0;
}