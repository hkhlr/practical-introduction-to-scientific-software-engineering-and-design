// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

class GradientDescent
{
  public:
    virtual ~GradientDescent() noexcept = default;
    virtual void run() = 0;
    virtual double getStepSize() const = 0;
    virtual void setStepSize( double step_size ) = 0;
    virtual std::vector< double > getCoordinates() const = 0;
    virtual void setCoordinates( std::vector< double > coords ) = 0;
    virtual std::vector< double > getGradient() const = 0;

  private:
    // virtual void updateCoordinates( std::vector< double > grad ) = 0;
};

class GradientDescentImpl : public GradientDescent
{
  public:
    GradientDescentImpl(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        std::vector< double > coords, double step_size, double gtol = 1e-3,
        unsigned max_iterations = 1000u )
        : f_( f )
        , gradf_( gradf )
        , coords_( coords )
        , step_size_( step_size )
        , gtol_( gtol )
        , max_iterations_( max_iterations )
    {
    }
    double getStepSize() const noexcept { return step_size_; }
    void setStepSize( double step_size ) { step_size_ = step_size; }
    std::vector< double > getCoordinates() const override
    {
        return coords_;
    } // returns a copy!
    void setCoordinates( std::vector< double > coords ) override
    {
        assert( coords.size() == coords_.size() );
        for ( unsigned idx = 0; idx < (unsigned)coords.size(); ++idx )
            coords_[ idx ] = coords[ idx ];
    };

    std::vector< double > getGradient() const override
    {
        return gradf_( coords_ );
    }

  protected:
    std::function< double( std::vector< double > ) > f_;
    std::function< std::vector< double >( std::vector< double > ) > gradf_;
    std::vector< double > coords_;
    double step_size_;
    double gtol_;
    unsigned max_iterations_;
};

class VanillaGradientDescent final : public GradientDescentImpl
{
  public:
    VanillaGradientDescent() = default;
    VanillaGradientDescent(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        std::vector< double > coords, double step_size, double gtol = 1e-3,
        unsigned max_iterations = 1000u )
        : GradientDescentImpl( f, gradf, coords, step_size, gtol,
                               max_iterations )
    {
    }
    ~VanillaGradientDescent() noexcept = default;

    void run() override
    {
        assert( coords_.size() > 0 ); // TODO do we want to keep that assertion
        // visualization code only works properly for 2D
        // it is hard to visualize more dimensions anyway ;-)
        std::vector< double > grad = getGradient();
        double g_l2norm2 = 0;
        for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];

        bool is_converged = std::sqrt( g_l2norm2 ) < gtol_;
        unsigned num_iterations = 0;

        while ( !is_converged && num_iterations <= max_iterations_ )
        {
            num_iterations++;
            // Update the coordinates based on the current gradient

            for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
            {
                coords_[ idx ] -= step_size_ * grad[ idx ];
            }

            grad = getGradient();
            // Compute the gradient at the updated position
            // Check if the gradient is small enough to stop the iterations
            g_l2norm2 = 0;
            for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
                g_l2norm2 += grad[ idx ] * grad[ idx ];
            is_converged = std::sqrt( g_l2norm2 ) < gtol_;
        }

        if ( num_iterations <= max_iterations_ )
        {
            std::cout << "Converged after " << num_iterations
                      << " iterations\n";
        }
        else
        {
            std::cout << "Failed to converge after " << max_iterations_
                      << " iterations\n";
        }
    }
};
class MomentumGradientDescent final : public GradientDescentImpl
{
  public:
    MomentumGradientDescent() = default;

    MomentumGradientDescent(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        std::vector< double > coords, double step_size, double gtol = 1e-3,
        unsigned max_iterations = 1000u, double alpha = 1e-1 )
        : GradientDescentImpl( f, gradf, coords, step_size, gtol,
                               max_iterations )
    {
    }

    ~MomentumGradientDescent() noexcept = default;

    void run() override
    {
        assert( coords_.size() > 0 ); // TODO do we want to keep that assertion
        // visualization code only works properly for 2D
        // it is hard to visualize more dimensions anyway ;-)

        std::vector< double > grad = getGradient();
        double g_l2norm2 = 0;
        for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];

        bool is_converged = std::sqrt( g_l2norm2 ) < gtol_;
        unsigned num_iterations = 0;

        while ( !is_converged && num_iterations <= max_iterations_ )
        {
            num_iterations++;
            // Update the coordinates based on the current gradient

            // position before
            std::vector< double > x{ coords_[ 0 ] };
            std::vector< double > y{ coords_[ 1 ] };
            std::vector< double > z{ f_( coords_ ) };

            for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
            {
                auto delta =
                    momentum_[ idx ] * alpha_ + step_size_ * grad[ idx ];
                coords_[ idx ] -= delta;
                momentum_[ idx ] = delta;
            }
            // position after
            x.push_back( coords_[ 0 ] );
            y.push_back( coords_[ 1 ] );
            z.push_back( f_( coords_ ) );

            grad = getGradient();
            // Compute the gradient at the updated position
            // Check if the gradient is small enough to stop the iterations
            g_l2norm2 = 0;
            for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
                g_l2norm2 += grad[ idx ] * grad[ idx ];
            is_converged = std::sqrt( g_l2norm2 ) < gtol_;
        }

        if ( num_iterations <= max_iterations_ )
        {
            std::cout << "Converged after " << num_iterations
                      << " iterations\n";
        }
        else
        {
            std::cout << "Failed to converge after " << max_iterations_
                      << " iterations\n";
        }
    }

    double getStepSize() const noexcept { return step_size_; }

    void setStepSize( double step_size ) { step_size_ = step_size; }

  private:
    std::vector< double > momentum_;
    double alpha_;
};

int main( int argc, char *argv[] )
{
    // the function to minimize
    auto f = []( std::vector< double > coords ) -> double
    {
        double const x = coords[ 0 ];
        double const y = coords[ 1 ];
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( std::vector< double > coords ) -> std::vector< double >
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    std::vector< double > starting_pos = { -1.0, 2.50 };
    double step_size = 3e-1;

    // use command line argument to decide which version of gradient decent to
    // use
    int implementation_to_use = 0;
    if ( argc > 1 )
    {
        implementation_to_use = std::stoi( argv[ 1 ] );
    }

    std::unique_ptr< GradientDescent > gd;
    switch ( implementation_to_use )
    {
    case 1:
        gd = std::make_unique< MomentumGradientDescent >(
            f, grad_f, starting_pos, step_size );
        break;

    case 0:
    default:
        gd = std::make_unique< VanillaGradientDescent >(
            f, grad_f, starting_pos, step_size );
        break;
    }

    gd->run();

    std::cout << "Final coordinates: x = " << gd->getCoordinates()[ 0 ]
              << ", y = " << gd->getCoordinates()[ 1 ] << "\n";

    return 0;
}