// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#ifndef NO_VISUALIZATION
// if NO_VISUALIZATION macro is defined: we compile without the code for
// visualization may be necessary on machines where matplotlib is not installed
#include <matplotlibcpp17/animation.h>
#include <matplotlibcpp17/cm.h>
#include <matplotlibcpp17/mplot3d.h>
#include <matplotlibcpp17/pyplot.h>
// settings for the visualization
const std::vector< double > lower_limits = { -1, -1, 0 };
const std::vector< double > upper_limits = { 3, 3, 10 };
double step_size = 0.1;
#endif

// #include <expected>
/*
template<Typename t,Class iterator,Class function> std::pair<t,bool>
iteration_driver(t start, Updater u,Decider d,size_t maxIter=100){ t
current=start; size_t iteration=0; while (!d(current)){ if (iteration>maxIter)
return {}; current = u(current);
    }
    return current;
}*/

class GradientDescent
{
  public:
    virtual ~GradientDescent() noexcept = default;

    virtual void run() = 0;

    virtual double getStepSize() const = 0;

    virtual void setStepSize( double step_size ) = 0;

    virtual std::vector< double > getCoordinates() const = 0;

    virtual void setCoordinates( std::vector< double > coords ) = 0;

    virtual std::vector< double > getGradient() const = 0;

  private:
    // virtual void updateCoordinates( std::vector< double > grad ) = 0;
};

class VanillaGradientDescent final : public GradientDescent
{
  public:
    VanillaGradientDescent() = default;

    VanillaGradientDescent(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        std::vector< double > coords, double step_size, double gtol = 1e-3,
        unsigned max_iterations = 1000u )
        : f_( f )
        , gradf_( gradf )
        , coords_( coords )
        , step_size_( step_size )
        , gtol_( gtol )
        , max_iterations_( max_iterations )
    {
    }

    ~VanillaGradientDescent() noexcept = default;

    void run() override
    {
        assert( coords_.size() > 0 ); // TODO do we want to keep that assertion
        // visualization code only works properly for 2D
        // it is hard to visualize more dimensions anyway ;-)
        if ( coords_.size() > 2 )
        {
#ifndef NO_VISUALIZATION
            std::cout << "WARNING: Only visualizing the function with regard "
                         "to the first two dimensions\n";
#endif
        }
        if ( coords_.size() == 1 )
        {
#ifndef NO_VISUALIZATION
            // add a second dummy dimension for visualization if necessary
            coords_.push_back( 1 );
#endif
        }

#ifndef NO_VISUALIZATION
        // set up visualization
        pybind11::scoped_interpreter guard{};
        auto plt = matplotlibcpp17::pyplot::import();
        // this is required for "projection = 3d"
        matplotlibcpp17::mplot3d::import();
        auto fig = plt.figure();
        auto ax = fig.add_subplot( py::make_tuple(),
                                   Kwargs( "projection"_a = "3d" ) );

        // plot the function surface
        std::vector< std::vector< double > > X;
        std::vector< std::vector< double > > Y;
        std::vector< std::vector< double > > Z;

        // calculate function points to visualize in a grid
        for ( int i = 0;
              i < std::abs( lower_limits[ 0 ] - upper_limits[ 0 ] ) / step_size;
              ++i )
        {
            X.push_back( {} );
            Y.push_back( {} );
            Z.push_back( {} );
            for ( int j = 0;
                  j <
                  std::abs( lower_limits[ 1 ] - upper_limits[ 1 ] ) / step_size;
                  ++j )
            {
                double x = lower_limits[ 0 ] + i * step_size;
                double y = lower_limits[ 1 ] + j * step_size;
                std::vector< double > function_in{ x, y };
                for ( int i = 2; i < coords_.size(); ++i )
                {
                    function_in.push_back(
                        1 ); // fill the vector with dummy values for other
                    // dimensions if necessary
                }
                auto z = f_( function_in );
                X[ i ].push_back( x );
                Y[ i ].push_back( y );
                Z[ i ].push_back( z );
            }
        }
        // to numpy array
        const auto X_ = py::array( py::cast( std::move( X ) ) );
        const auto Y_ = py::array( py::cast( std::move( Y ) ) );
        const auto Z_ = py::array( py::cast( std::move( Z ) ) );
        // the actual plotting:
        const auto surf = ax.plot_surface(
            Args( X_, Y_, Z_ ),
            Kwargs( "rstride"_a = 1, "cstride"_a = 1, "linewidth"_a = 0,
                    "antialiased"_a = true, "alpha"_a = 0.75,
                    "cmap"_a = matplotlibcpp17::cm::coolwarm ) );
        fig.colorbar( Args( surf.unwrap() ),
                      Kwargs( "shrink"_a = 0.5, "aspect"_a = 5 ) );
        // end of plotting the function

        // list of the steps to animate
        py::list artist_list;
#endif

        std::vector< double > grad = getGradient();
        double g_l2norm2 = 0;
        for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];

        bool is_converged = std::sqrt( g_l2norm2 ) < gtol_;
        unsigned num_iterations = 0;

        while ( !is_converged && num_iterations <= max_iterations_ )
        {
            num_iterations++;
            // Update the coordinates based on the current gradient

            // position before
            std::vector< double > x{ coords_[ 0 ] };
            std::vector< double > y{ coords_[ 1 ] };
            std::vector< double > z{ f_( coords_ ) };

            for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
            {
                coords_[ idx ] -= step_size_ * grad[ idx ];
            }
            // position after
            x.push_back( coords_[ 0 ] );
            y.push_back( coords_[ 1 ] );
            z.push_back( f_( coords_ ) );
#ifndef NO_VISUALIZATION
            // visualize this step and add it to the list of steps to animate
            ax.plot( Args( x, y, z ),
                     Kwargs( "color"_a = "black", "antialiased"_a = true ) );
            artist_list.append( ax.get_lines().unwrap() );
#endif

            grad = getGradient();
            // Compute the gradient at the updated position
            // Check if the gradient is small enough to stop the iterations
            g_l2norm2 = 0;
            for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
                g_l2norm2 += grad[ idx ] * grad[ idx ];
            is_converged = std::sqrt( g_l2norm2 ) < gtol_;
        }

        if ( num_iterations <= max_iterations_ )
        {
            std::cout << "Converged after " << num_iterations
                      << " iterations\n";
        }
        else
        {
            std::cout << "Failed to converge after " << max_iterations_
                      << " iterations\n";
        }
#ifndef NO_VISUALIZATION
        // animate the path taken
        auto ani = matplotlibcpp17::animation::ArtistAnimation(
            Args( fig.unwrap(), artist_list ), Kwargs( "interval"_a = 100 ) );

        plt.show();
#endif
    }

    double getStepSize() const noexcept { return step_size_; }

    void setStepSize( double step_size ) { step_size_ = step_size; }

    std::vector< double > getCoordinates() const override
    {
        return coords_;
    } // returns a copy!
    void setCoordinates( std::vector< double > coords ) override
    {
        assert( coords.size() == coords_.size() );
        for ( unsigned idx = 0; idx < (unsigned)coords.size(); ++idx )
            coords_[ idx ] = coords[ idx ];
    };

    std::vector< double > getGradient() const override
    {
        return gradf_( coords_ );
    }

  private:
    std::function< double( std::vector< double > ) > f_;
    std::function< std::vector< double >( std::vector< double > ) > gradf_;
    std::vector< double > coords_;
    double step_size_;
    double gtol_;
    unsigned max_iterations_;
};

class MomentumGradientDescent final : public GradientDescent
{
  public:
    MomentumGradientDescent() = default;

    MomentumGradientDescent(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        std::vector< double > coords, double step_size, double gtol = 1e-3,
        unsigned max_iterations = 1000u, double alpha = 1e-1 )
        : f_( f )
        , gradf_( gradf )
        , coords_( coords )
        , step_size_( step_size )
        , gtol_( gtol )
        , max_iterations_( max_iterations )
        , alpha_( alpha )
        , momentum_( coords.size(), 0 )
    {
    }

    ~MomentumGradientDescent() noexcept = default;

    void run() override
    {
        assert( coords_.size() > 0 ); // TODO do we want to keep that assertion
        // visualization code only works properly for 2D
        // it is hard to visualize more dimensions anyway ;-)
        if ( coords_.size() > 2 )
        {
#ifndef NO_VISUALIZATION
            std::cout << "WARNING: Only visualizing the function with regard "
                         "to the first two dimensions\n";
#endif
        }
        if ( coords_.size() == 1 )
        {
#ifndef NO_VISUALIZATION
            // add a second dummy dimension for visualization if necessary
            coords_.push_back( 1 );
            momentum_.push_back( 0 );
#endif
        }

#ifndef NO_VISUALIZATION
        // set up visualization
        pybind11::scoped_interpreter guard{};
        auto plt = matplotlibcpp17::pyplot::import();
        // this is required for "projection = 3d"
        matplotlibcpp17::mplot3d::import();
        auto fig = plt.figure();
        auto ax = fig.add_subplot( py::make_tuple(),
                                   Kwargs( "projection"_a = "3d" ) );

        // plot the function surface
        std::vector< std::vector< double > > X;
        std::vector< std::vector< double > > Y;
        std::vector< std::vector< double > > Z;

        // calculate function points to visualize in a grid
        for ( int i = 0;
              i < std::abs( lower_limits[ 0 ] - upper_limits[ 0 ] ) / step_size;
              ++i )
        {
            X.push_back( {} );
            Y.push_back( {} );
            Z.push_back( {} );
            for ( int j = 0;
                  j <
                  std::abs( lower_limits[ 1 ] - upper_limits[ 1 ] ) / step_size;
                  ++j )
            {
                double x = lower_limits[ 0 ] + i * step_size;
                double y = lower_limits[ 1 ] + j * step_size;
                std::vector< double > function_in{ x, y };
                for ( int i = 2; i < coords_.size(); ++i )
                {
                    function_in.push_back(
                        1 ); // fill the vector with dummy values for other
                    // dimensions if necessary
                }
                auto z = f_( function_in );
                X[ i ].push_back( x );
                Y[ i ].push_back( y );
                Z[ i ].push_back( z );
            }
        }
        // to numpy array
        const auto X_ = py::array( py::cast( std::move( X ) ) );
        const auto Y_ = py::array( py::cast( std::move( Y ) ) );
        const auto Z_ = py::array( py::cast( std::move( Z ) ) );
        // the actual plotting:
        const auto surf = ax.plot_surface(
            Args( X_, Y_, Z_ ),
            Kwargs( "rstride"_a = 1, "cstride"_a = 1, "linewidth"_a = 0,
                    "antialiased"_a = true, "alpha"_a = 0.75,
                    "cmap"_a = matplotlibcpp17::cm::coolwarm ) );
        fig.colorbar( Args( surf.unwrap() ),
                      Kwargs( "shrink"_a = 0.5, "aspect"_a = 5 ) );
        // end of plotting the function

        // list of the steps to animate
        py::list artist_list;
#endif

        std::vector< double > grad = getGradient();
        double g_l2norm2 = 0;
        for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];

        bool is_converged = std::sqrt( g_l2norm2 ) < gtol_;
        unsigned num_iterations = 0;

        while ( !is_converged && num_iterations <= max_iterations_ )
        {
            num_iterations++;
            // Update the coordinates based on the current gradient

            // position before
            std::vector< double > x{ coords_[ 0 ] };
            std::vector< double > y{ coords_[ 1 ] };
            std::vector< double > z{ f_( coords_ ) };

            for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
            {
                auto delta =
                    momentum_[ idx ] * alpha_ + step_size_ * grad[ idx ];
                coords_[ idx ] -= delta;
                momentum_[ idx ] = delta;
            }
            // position after
            x.push_back( coords_[ 0 ] );
            y.push_back( coords_[ 1 ] );
            z.push_back( f_( coords_ ) );
#ifndef NO_VISUALIZATION
            // visualize this step and add it to the list of steps to animate
            ax.plot( Args( x, y, z ),
                     Kwargs( "color"_a = "black", "antialiased"_a = true ) );
            artist_list.append( ax.get_lines().unwrap() );
#endif

            grad = getGradient();
            // Compute the gradient at the updated position
            // Check if the gradient is small enough to stop the iterations
            g_l2norm2 = 0;
            for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
                g_l2norm2 += grad[ idx ] * grad[ idx ];
            is_converged = std::sqrt( g_l2norm2 ) < gtol_;
        }

        if ( num_iterations <= max_iterations_ )
        {
            std::cout << "Converged after " << num_iterations
                      << " iterations\n";
        }
        else
        {
            std::cout << "Failed to converge after " << max_iterations_
                      << " iterations\n";
        }
#ifndef NO_VISUALIZATION
        // animate the path taken
        auto ani = matplotlibcpp17::animation::ArtistAnimation(
            Args( fig.unwrap(), artist_list ), Kwargs( "interval"_a = 100 ) );

        plt.show();
#endif
    }

    double getStepSize() const noexcept { return step_size_; }

    void setStepSize( double step_size ) { step_size_ = step_size; }

    std::vector< double > getCoordinates() const override
    {
        return coords_;
    } // returns a copy!
    void setCoordinates( std::vector< double > coords ) override
    {
        assert( coords.size() == coords_.size() );
        for ( unsigned idx = 0; idx < (unsigned)coords.size(); ++idx )
            coords_[ idx ] = coords[ idx ];
    };

    std::vector< double > getGradient() const override
    {
        return gradf_( coords_ );
    }

  private:
    std::function< double( std::vector< double > ) > f_;
    std::function< std::vector< double >( std::vector< double > ) > gradf_;
    std::vector< double > coords_;
    std::vector< double > momentum_;
    double step_size_;
    double gtol_;
    double alpha_;
    unsigned max_iterations_;
};

int main( int argc, char *argv[] )
{
    // the function to minimize
    auto f = []( std::vector< double > coords ) -> double
    {
        double const x = coords[ 0 ];
        double const y = coords[ 1 ];
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( std::vector< double > coords ) -> std::vector< double >
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    std::vector< double > starting_pos = { -1.0, 2.50 };
    double step_size = 3e-1;

    // use command line argument to decide which version of gradient decent to
    // use
    int implementation_to_use = 0;
    if ( argc > 1 )
    {
        implementation_to_use = std::stoi( argv[ 1 ] );
    }

    std::unique_ptr< GradientDescent > gd;
    switch ( implementation_to_use )
    {
    case 1:
        gd = std::make_unique< MomentumGradientDescent >(
            f, grad_f, starting_pos, step_size );
        break;

    case 0:
    default:
        gd = std::make_unique< VanillaGradientDescent >(
            f, grad_f, starting_pos, step_size );
        break;
    }

    gd->run();

    std::cout << "Final coordinates: x = " << gd->getCoordinates()[ 0 ]
              << ", y = " << gd->getCoordinates()[ 1 ] << "\n";

    return 0;
}