// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#ifndef NO_VISUALIZATION
// if NO_VISUALIZATION macro is defined: we compile without the code for
// visualization may be necessary on machines where matplotlib is not installed
#include <matplotlibcpp17/animation.h>
#include <matplotlibcpp17/cm.h>
#include <matplotlibcpp17/mplot3d.h>
#include <matplotlibcpp17/pyplot.h>
// settings for the visualization
const std::vector< double > lower_limits = { -1, -1, 0 };
const std::vector< double > upper_limits = { 3, 3, 10 };
double step_size = 0.1;
#endif

template < typename t, class Updater, class Decider >
std::pair< t, size_t > iteration_driver( t start, Updater &u, Decider &d,
                                         size_t maxIter = 100 )
{
    t current = start;
    size_t iteration = 0;
    while ( !d( current ) )
    {
        if ( iteration > maxIter )
            break;
        current = u( current );
        iteration++;
    }
    return std::make_pair( current, iteration );
}

template < class T >
class myVisualizer : public T
{
  public:
    template < typename... Args >
    myVisualizer( Args &&...args )
        : T( std::forward< Args >( args )... ){};
    myVisualizer( T &base )
        : T( base ){}; /*
template <typename ARGS> myVisualizer(ARGS && args):T(static_cast<ARGS&&>(args))
#ifndef NO_VISUALIZATION
,pPLT(new matplotlibcpp17::pyplot::PyPlot(matplotlibcpp17::pyplot::import()))
#endif
{

}*/
                       // myVisualizer(const T&) = default;
                       // myVisualizer(T & t):
    void startPosition( std::vector< double > pos )
    {
        x.push_back( pos[ 0 ] );
        y.push_back( pos[ 1 ] );
        z.push_back( T::f_( pos ) );
    }
    void finishVisualization()
    {
#ifndef NO_VISUALIZATION
        // animate the path taken
        auto ani = matplotlibcpp17::animation::ArtistAnimation(
            Args( pFIG->unwrap(), artist_list ), Kwargs( "interval"_a = 100 ) );
        //  std::cout << "Plotting " << x.size() << " steps" << std::endl;
        //   pAX->plot( Args( x, y, z ),
        //               Kwargs( "color"_a = "black", "antialiased"_a = true )
        //               );
        pPLT->show();
#endif
    }
    void setupVisualization()
    {

#ifndef NO_VISUALIZATION
        // set up visualization
        pPLT = new matplotlibcpp17::pyplot::PyPlot(
            matplotlibcpp17::pyplot::import() );
        // this is required for "projection = 3d"
        matplotlibcpp17::mplot3d::import();
        pFIG =
            new matplotlibcpp17::figure::Figure( std::move( pPLT->figure() ) );
        pAX = new matplotlibcpp17::axes::Axes( std::move( pFIG->add_subplot(
            py::make_tuple(), Kwargs( "projection"_a = "3d" ) ) ) );
        /*
        auto ax = fig.add_subplot( py::make_tuple(),
                                    Kwargs( "projection"_a = "3d" ) );*/

        // plot the function surface
        std::vector< std::vector< double > > X;
        std::vector< std::vector< double > > Y;
        std::vector< std::vector< double > > Z;

        // calculate function points to visualize in a grid
        for ( int i = 0;
              i < std::abs( lower_limits[ 0 ] - upper_limits[ 0 ] ) / step_size;
              ++i )
        {
            X.push_back( {} );
            Y.push_back( {} );
            Z.push_back( {} );
            for ( int j = 0;
                  j <
                  std::abs( lower_limits[ 1 ] - upper_limits[ 1 ] ) / step_size;
                  ++j )
            {
                double x = lower_limits[ 0 ] + i * step_size;
                double y = lower_limits[ 1 ] + j * step_size;
                std::vector< double > function_in{ x, y }; /*
                 for ( int i = 2; i < dim_; ++i )
                 {
                     function_in.push_back(
                         1 ); // fill the vector with dummy values for other
                     // dimensions if necessary
                 }*/
                auto z = T::f_( function_in );
                X[ i ].push_back( x );
                Y[ i ].push_back( y );
                Z[ i ].push_back( z );
            }
        }
        // to numpy array
        const auto X_ = py::array( py::cast( std::move( X ) ) );
        const auto Y_ = py::array( py::cast( std::move( Y ) ) );
        const auto Z_ = py::array( py::cast( std::move( Z ) ) );
        // the actual plotting:
        const auto surf = pAX->plot_surface(
            Args( X_, Y_, Z_ ),
            Kwargs( "rstride"_a = 1, "cstride"_a = 1, "linewidth"_a = 0,
                    "antialiased"_a = true, "alpha"_a = 0.75,
                    "cmap"_a = matplotlibcpp17::cm::coolwarm ) );
        pFIG->colorbar( Args( surf.unwrap() ),
                        Kwargs( "shrink"_a = 0.5, "aspect"_a = 5 ) );
        // end of plotting the function

        // list of the steps to animate

#endif
    }

    std::vector< double > operator()( std::vector< double > input )
    {
        auto tmp = T::operator()( input );
// Visualize here
#ifndef NO_VISUALIZATION

        x.push_back( tmp[ 0 ] );
        y.push_back( tmp[ 1 ] );
        z.push_back( T::f_( input ) );
        //   std::cout << "Plotting x="<<x.back()<< " y="<<y.back()<<" z="
        //   <<z.back()<< std::endl;
        // visualize this step and add it to the list of steps to animate
        pAX->plot( Args( x, y, z ),
                   Kwargs( "color"_a = "black", "antialiased"_a = true ) );
        artist_list.append( pAX->get_lines().unwrap() );
#endif
        return tmp;
    }

  protected:
#ifndef NO_VISUALIZATION
    matplotlibcpp17::pyplot::PyPlot *pPLT;
    matplotlibcpp17::axes::Axes *pAX;
    matplotlibcpp17::figure::Figure *pFIG;
    py::list artist_list;
#endif
    std::function< double( std::vector< double > ) > f_;
    size_t dim_;
    std::vector< double > x, y, z;
};

class NormalGradientUpdate
{
  public:
    NormalGradientUpdate(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        double step_size )
        : f_( f )
        , gradf_( gradf )
        , step_size_( step_size )
    {
    }

    std::vector< double > operator()( std::vector< double > in )
    {
        auto grad = gradf_( in );
        for ( unsigned idx = 0; idx < (unsigned)in.size(); ++idx )
        {
            in[ idx ] -= step_size_ * grad[ idx ];
        }
        return in;
    }

  protected:
    std::function< double( std::vector< double > ) > f_;
    std::function< std::vector< double >( std::vector< double > ) > gradf_;
    double step_size_;
};

class MomentumGradientUpdate
{
  public:
    MomentumGradientUpdate(
        std::function< double( std::vector< double > ) > f,
        std::function< std::vector< double >( std::vector< double > ) > gradf,
        double step_size, double alpha = 1.0 )
        : f_( f )
        , gradf_( gradf )
        , step_size_( step_size )
        , alpha_( alpha )
    {
    }

    std::vector< double > operator()( std::vector< double > coords_ )
    {
        // auto momentumGradientUpdater=
        // [getGradient=grad_f,alpha_=alpha,&momentum,step_size_](std::vector<double>
        // coords_){
        auto grad = gradf_( coords_ );
        momentum.resize( coords_.size() );
        for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
        {
            auto delta = momentum[ idx ] * alpha_ + step_size_ * grad[ idx ];
            coords_[ idx ] -= delta;
            momentum[ idx ] = delta;
        }
        return coords_;
    }

  protected:
    std::function< double( std::vector< double > ) > f_;
    std::function< std::vector< double >( std::vector< double > ) > gradf_;
    double step_size_, alpha_;
    std::vector< double > momentum;
};

// using  IterativeGradientSolver = iteration_driver;

int main( int argc, char *argv[] )
{

#ifndef NO_VISUALIZATION
    // Initialize the global python interpreter. This is necessary for the
    // reoccuring use of the plotter
    pybind11::initialize_interpreter();
#endif

    // the function to minimize
    auto f = []( std::vector< double > coords ) -> double
    {
        double const x = coords[ 0 ];
        double const y = coords[ 1 ];
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( std::vector< double > coords ) -> std::vector< double >
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    auto isConverged = [ getGradient =
                             grad_f ]( std::vector< double > coords_ ) -> bool
    {
        double g_l2norm2 = 0;
        auto grad = getGradient( coords_ );
        for ( unsigned idx = 0; idx < (unsigned)grad.size(); ++idx )
            g_l2norm2 += grad[ idx ] * grad[ idx ];

        bool is_converged = std::sqrt( g_l2norm2 ) < 1e-3;
        return is_converged;
    };
    double step_size_ = 3e-1;
    auto normalGradientUpdater =
        [ getGradient = grad_f,
          step_size_ ]( std::vector< double > coords_ ) -> std::vector< double >
    {
        auto grad = getGradient( coords_ );
        for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
        {
            coords_[ idx ] -= step_size_ * grad[ idx ];
        }
        return coords_;
    };
    double alpha = 1e-1;
    std::vector< double > momentum( 3, 0.0 );
    auto momentumGradientUpdater =
        [ getGradient = grad_f, alpha_ = alpha, &momentum,
          step_size_ ]( std::vector< double > coords_ )
    {
        auto grad = getGradient( coords_ );
        for ( unsigned idx = 0; idx < (unsigned)coords_.size(); ++idx )
        {
            auto delta = momentum[ idx ] * alpha_ + step_size_ * grad[ idx ];
            coords_[ idx ] -= delta;
            momentum[ idx ] = delta;
        }
        return coords_;
    };

    std::vector< double > starting_pos = { -1.0, 2.50 };

    // use command line argument to decide which version of gradient decent to
    // use
    int implementation_to_use = 0;
    if ( argc > 1 )
    {
        implementation_to_use = std::stoi( argv[ 1 ] );
    }

    //  std::unique_ptr< GradientDescent > gd;
    std::pair< std::vector< double >, double > result;
    switch ( implementation_to_use )
    {
    case 1:
    {
#ifndef NO_VISUALIZATION
        auto updater =
            myVisualizer< NormalGradientUpdate >( f, grad_f, step_size_ );
        updater.setupVisualization();
        updater.startPosition( starting_pos );
#else
        auto updater = NormalGradientUpdate( f, grad_f, step_size_ );
#endif

        //   auto foo=ngu(std::vector<double>());
        //  auto updater=[&ngu](std::vector<double> coords){return
        //  ngu(coords);};

        // myVisualizer<decltype([&ngu](std::vector<double> coords){return
        // ngu(coords);})>(),
        // result = iteration_driver(starting_pos, [&ngu](std::vector<double>
        // coords){return ngu(coords);},  isConverged,     1000u);
        result = iteration_driver( starting_pos, updater, isConverged, 1000u );
#ifndef NO_VISUALIZATION
        updater.finishVisualization();
#endif
        break;
    }
    case 0:
    default:
    {
        result = iteration_driver( starting_pos, momentumGradientUpdater,
                                   isConverged, 1000u );
        MomentumGradientUpdate mgu( f, grad_f, step_size_ );
        auto vis =
            myVisualizer< MomentumGradientUpdate >( f, grad_f, step_size_ );
#ifndef NO_VISUALIZATION
        vis.setupVisualization();
        vis.startPosition( starting_pos );
#endif
        // result =
        // iteration_driver(starting_pos,myVisualizer<decltype([&mgu](std::vector<double>
        // coords){return mgu(coords);})>,isConverged,1000u); result =
        // iteration_driver(starting_pos,[&mgu](std::vector<double>
        // coords){return mgu(coords);},isConverged,1000u);
        result = iteration_driver( starting_pos, vis, isConverged, 1000u );
#ifndef NO_VISUALIZATION
        vis.finishVisualization();
#endif
    }
    break;
    }

    if ( result.second < 1000 )
        std::cout << "Final coordinates: x = " << result.first[ 0 ]
                  << ", y = " << result.first[ 1 ] << "\n";
    else
        std::cout << "Gradient Method did not converge in Max-Iter (" << 1000
                  << "), current result is x = " << result.first[ 0 ]
                  << ", y = " << result.first[ 1 ] << "\n"
                  << std::endl;
    return 0;
}