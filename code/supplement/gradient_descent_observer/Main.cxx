// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <iostream>
#include <numeric>
#include <type_traits>
#include <vector>

#include "Plotter.h"
#include "Types.h"

/*
>>>> NOTE        <<<<
For compactness we declare and define all required entities in a single source
file.

Of course, in a real software project, logically-related entities should be kept
in dedicated source and header files.
>>>> END OF NOTE <<<<
*/

class CoordinateUpdater
{
  public:
    using ValueType = typename Coordinate::value_type;
    using SizeType = typename Coordinate::size_type;

    virtual ~CoordinateUpdater() noexcept = default;
    virtual void operator()( Coordinate &coords, Coordinate const &grad ) = 0;
};

class VanillaUpdater final : public CoordinateUpdater
{
  public:
    explicit VanillaUpdater( ValueType learning_rate = 1e-2 )
        : learning_rate_{ learning_rate }
    {
    }
    ~VanillaUpdater() noexcept override = default;

    void operator()( Coordinate &coords, Coordinate const &grad ) override
    {
        for ( auto idx{ 0ul }; auto &x : coords )
            x -= learning_rate_ * grad.at( idx++ );
    }

  private:
    ValueType const learning_rate_{};
};

class MomentumUpdater final : public CoordinateUpdater
{
  public:
    MomentumUpdater( ValueType alpha, ValueType learning_rate, SizeType size )
        : alpha_{ alpha }
        , learning_rate{ learning_rate }
        , delta_omega_( size, 0 ) // Initial vector filled with 0s.
    {
    }
    ~MomentumUpdater() noexcept override = default;

    void operator()( Coordinate &coords, Coordinate const &grad ) override
    {
        for ( auto idx = 0ul; auto &d : delta_omega_ )
        {
            d *= alpha_;
            d -= learning_rate * grad.at( idx );
            coords.at( idx++ ) += d;
        }
    }

  private:
    ValueType const alpha_{};
    ValueType const learning_rate{};
    Coordinate delta_omega_{};
};

/*
------ files: Observer.h / Observer.cpp ------
*/
class Observer
{
  public:
    using State = std::tuple< bool, Coordinate, Coordinate >;

    Observer() = default;
    explicit Observer( bool trace_history )
        : trace_history_{ trace_history }
    {
    }
    ~Observer() noexcept = default;
    Observer( Observer const & ) = default;
    Observer &operator=( Observer const & ) = default;
    Observer( Observer && ) noexcept = default;
    Observer &operator=( Observer && ) noexcept = default;

    void update( State state )
    {
        if ( trace_history_ )
        {
            states_.push_back( std::move( state ) );
        }
        else
        {
            if ( states_.empty() )
                states_.push_back( std::move( state ) );
            else /*only replace previous state with new one*/
                states_.at( 0 ) = std::move( state );
        }
    }

    State const &result() const { return states_.back(); }
    std::vector< State > const &states() const { return states_; }

  private:
    bool trace_history_{ false };
    std::vector< State > states_{};
};

/*
------ file: GradientDescentImpl.cpp ------

This is the driver function for running the GD iterations.

Note how an Observer and Updater object are passed (both are defaulted).
*/
template < typename Updater = VanillaUpdater >
// requires std::is_base_of_v< CoordinateUpdater, Updater >
// the requires statement requires a newer compiler
Observer gradientDescent( FunctionGradient gradf, Coordinate coord,
                          Observer observer = {}, Updater updater = {},
                          double learning_rate = 1e-2,
                          double gradient_tolerance = 1e-3,
                          size_t max_iterations = 1000ul )
{
    auto const isConverged =
        [ &gradient_tolerance ]( Coordinate const &g ) -> bool
    {
        return std::inner_product( g.begin(), g.end(), g.begin(),
                                   ValueType( 0 ) ) <
               ( gradient_tolerance * gradient_tolerance );
    };

    auto num_iterations{ 0ul };
    auto grad{ gradf( coord ) };
    bool is_converged{ isConverged( grad ) };

    /*
    Record the initial state before the actual iterations have started.
    */
    observer.update( { is_converged, coord, grad } );

    while ( !is_converged && num_iterations < max_iterations )
    {
        num_iterations++;
        updater( coord, grad );
        grad = gradf( coord );
        is_converged = isConverged( grad );
        /*
        Update / append the current state.
        */
        observer.update( { is_converged, coord, grad } );
    }

    if ( num_iterations <= max_iterations )
    {
        std::cout << "Converged after " << num_iterations << " iterations\n";
    }
    else
    {
        std::cout << "Failed to converge after " << max_iterations
                  << " iterations\n";
    }

    return observer;
}

int main()
{
    // the function to minimize
    auto f = []( Coordinate const &coord ) -> double
    {
        double const x{ coord.at( 0 ) };
        double const y{ coord.at( 1 ) };
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( Coordinate const &coord ) -> Coordinate
    {
        double const x{ coord.at( 0 ) };
        double const y{ coord.at( 1 ) };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    Coordinate const coords_initial{ -1, 2.5 };
    constexpr double learning_rate{ 3e-1 };

    /*
    Helper function to extract the coordinates "visited" by the GD algorithm
    during the iterations.
    */
    auto getCoordsFromObserver = []( auto const &observer )
    {
        CoordinateArray steps{};
        auto const &states = observer.states();
        std::transform( states.begin(), states.end(),
                        std::back_inserter( steps ),
                        []( auto const &s ) { return std::get< 1 >( s ); } );
        return steps;
    };

    {
        std::cout << "\tUsing Vanilla Gradient Descent\n";
        auto const gd_observer{
            gradientDescent( grad_f, coords_initial,
                             Observer{ true /*trace_history*/ },
                             VanillaUpdater{ learning_rate } ),
        };

        [ &obs = gd_observer ]()
        {
            auto const final_coords = std::get< 1 >( obs.result() );
            std::cout << "Vanilla GD: Final coordinates after finishing "
                         "iterations:\n";
            std::cout << "x = " << final_coords.at( 0 )
                      << ", y = " << final_coords.at( 1 ) << "\n";
        }();

        /*
        Visualise the function and the path taken by the gradient descent
        algorithm.
        */
        // auto plotter = PlotterFactory();
        // plotter->plot_gradient_descent( f,
        //                                 getCoordsFromObserver( gd_observer )
        //                                 );
    }

    {
        std::cout << "\tUsing Momentum Gradient Descent\n";
        auto const gd_observer{
            gradientDescent( grad_f, coords_initial,
                             Observer{ true /*trace_history*/ },
                             MomentumUpdater{ 2.5e-1, learning_rate, 2ul } ),
        };

        [ &obs = gd_observer ]() -> void
        {
            auto const final_coords = std::get< 1 >( obs.result() );
            std::cout << "Momentum GD: Final coordinates after finishing "
                         "iterations:\n";
            std::cout << "x = " << final_coords.at( 0 )
                      << ", y = " << final_coords.at( 1 ) << "\n";
        }();

        /*
        Visualise the function and the path taken by the gradient descent
        algorithm.
        */
        // auto *plotter{ PlotterFactory() };
        // plotter->plot_gradient_descent( f,
        //                                 getCoordsFromObserver( gd_observer )
        //                                 );
    }

    return 0;
}
