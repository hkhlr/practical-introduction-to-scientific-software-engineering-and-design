# Observer

## Objective 

In this example we will refactor the original version of the code in order to be able to easily access the data produced during the GD iterations.

The idea is to have an "observer" that will record the iterations history on demand. This gives us the opportunity to visually inspect the results if desired. If the single coordinates are not of interest, we just make the "observer" update to the most recent position during the iterations. When the iterations are finished because

* the minimum has been found, or
* the number of allowed iterations has been exceeded,

the "observer" is returned and we can work with the result.

## Refactoring

The current version of the code (e.g. refer to the [`gradient_descent_original`](../../gradient_descent_original/) directory) is rather "messy", with lots of unneeded code duplication. Hence, let's think for a moment how we can remedy the issues.

We will use the plotting infrastructure developed until checkpoint [`gradient_descent_04`](../../gradient_descent_v04/) (refer to the files [`Plotter.h`](../../gradient_descent_v04/Plotter.h) and [`Plotter.cxx`](../../gradient_descent_v04/Plotter.cxx)). That is, we will focus on refactoring the code converned with the GD method (i.e., iterations and coordinate update).


### Coordinate update

We first observe that it is the *coordinate update* that distinguishes the different GD flavours. The code running the GD iterations repeatedly updates coordinates based on the current gradient information. In fact, this code does not really "care" about the details of the coordinate update as long as the updating entity "does the right thing". 

Hence, we can factor out the updating part and leave just a single entity for running the GD iterations. This entity will consume an "updater" (essentially, a class that implements some particular coordinate update strategy) and calls it as needed. 

But what does all this look like in code?

>**Note**: The details of the implementation can be found in [`01-Updaters/Main.cxx`](./01-Updaters/Main.cxx).

To make all "updaters" adhere to a particular interface we use an abstraction:

```c++
class CoordinateUpdater {
public:
    /*...*/
    virtual void operator()( Coordinate& coord, Coordinate const& grad ) = 0;
    /*...*/
};

// Vanilla updater
class VanillaUpdater final : public CoordinateUpdater {
public:
    /*...*/
    void operator()( Coordinate& coord, Coordinate const& grad ) override;
    /*...*/
private:
    /*declare required member variables*/
};

// Momentum updater
class MomentumUpdater final : public CoordinateUpdater {
public:
    /*...*/
    void operator()( Coordinate& coord, Coordinate const& grad ) override;
    /*...*/
private:
    /*declare required member variables*/
}
```

The call operator `operator()( /*...*/ )` member function of each concrete class implements the details of the particular coordinate update. Since each coordinate update flavour requires custom numerical parameters (e.g. the learning rate / step size) the concrete classes must declare all required member variables and offer a suitable ctor to create an instance of the classes.

You might ask, why we are using classes here? Well, while using vanilla coordinate update does not require to keep track of intermediate results many other methods do. The momentum update method keeps track of the "velocity"; hence we need entities that are *stateful*. Since we want to be able to handle *all* "updaters" uniformly, the vanilla coordinate update is also implemented by means of a class (although not strictly required).

Note how the class hierachy for the different GD flavours has been replaced in favour a single class `GradientDescent`. While this is still far from optimal, we have largely reduced code duplication as we now only have a single `run`. Now only this method contains the code implementing the GD iterations.

The class' ctor now only accepts parameters that are directly related to the GD iterations, e.g. the tolerance for the gradient 2-norm or the maximal allowed number of iterations. Most noteably, it now also has an additional argument for the "updater":

```c++
GradientDescent( /*...*/ std::unique_ptr< CoordinateUpdater > updater /*...*/ )
    : /*...*/
    , updater_{ std::move( updater ) } // unique_ptr must be moved
    /*...*/
```

Obviously, we have more cleanly separated reponsibilties amongst the different entities of our code base:

* It is the "updater's" task to perform a particular type of coordinate update given the gradient information.
* It is the `GradientDescent` class' task to perform the GD iterations.

As a result the *single-responsibilty-principle* (SRP) is better adhered to.

### Free Function

> **Note** The details of the implementation can be found in the file [`02-FreeFunction/Main.cxx`](./02-FreeFunction/Main.cxx).


Yet, we should be tempted to ask why the entity running the GD iterations still is a class? Indeed, this is not necessary! If we look at the relevant lines inside [`01-Updaters/Main.cxx`](01-Updaters/Main.cxx) we see that right after instanciating a `GradientDescent` object the `run` method is called:

```c++
// file: Main.cxx
/*...*/
auto gd = GradientDescent( grad_f, starting_pos,
                           std::make_unique< VanillaUpdater >( step_size ) );
gd.run();
/*...*/
```

Needless to say, this can be replaced with call to an ordinary function.

* No further modification of the object's inner state occurs (and is not required) after creating an instance of `GradientDescent`.
* The object's inner state changes during the iteration but these changes can also be tracked and returned from an "ordinary" function.

Let's create a *free* function (meaning a function that is not bound to a particular class instance) with declaration similar to this:

```c++
std::pair< bool /*converged?*/, Coordinate > 
gradientDescent( FunctionGradient const& gradf, 
                 Coordinate coords, 
                 std::unique_ptr< CoordinateUpdater > updater,
                 /*numerical parameters*/ );
```

Most of the code from the `run` method can just be copied over. We use a `std::pair< bool, Coordinate >` to be
able to "communicate" the state of the iterations. If the first boolean argument is `true` the GD iterations 
have converged, otherwise not. In any case, the second parameter are the coordinates at the end of the iterations
(it does not matter if the minimum has actually been found).

The call to this function then looks like so:

```c++
auto const [ _, final_coords ] = 
    gradientDescent( grad_f, starting_pos,
                     std::make_unique< VanillaUpdater >( step_size ) );
```

Indeed, the call to this function accomplishes no less than creating the class instance before and subsequently calling the `run` method!

At this point we point out that it is also possible to make the `gradientDescent` function a *template*. We make it the compiler's task to figure out the particular "updater" which is passed by *value* instead of `std::unique_ptr< CoordinateUpdater >`:

```c++
template< typename Updater = VanillaUpdater /*defaulted*/ >
std::pair< bool, Coordinate >
gradientDescent( /*...*/ Updater updater = {} /*...*/ );
```

To make sure that only "updaters" that inherit from the `CoordinateUpdater`abstract base class can be passed to this function we can use the C++20 `requires`-clause: `requires std::is_base_of_v< CoordinateUpdater, Updater >` (include this line below the line `template< /*...*/>`).


### Observer

>**Note**: The details of the implementation can be found in [`Main.cxx`](Main.cxx).


We want to have a well-defined interface to the iteration history of the GD algorithm. The straight forward and most obvious way is to add another boolean parameter to the `gradientDescent` function (template, depending on how you to implement it) and to modify the return type like so:

```c++
std::pair< bool, std::vector< Coordinate > > 
gradientDescent( /*...*/ bool record_history = false /*...*/ ){
    /*...*/
    std::vector< Coordinate > history_{};
    /*...*/    
    return { is_converged, history };
}
```

If `record_history == true` we call the `push_back` method of the `std::vector< Coordinate >` instance whenever we make a coordinate update; otherwise we only store the most recent coordinate.

Typically, a function implementing a numerical method like expects quite a few parameters as input. Oftentimes, these parameters have "reasonably chosen" default values to make final function call not look too complicated. To avoid adding another (defaulted) function parameter and to communicate out intent better, we define a dedicated "observer" object. This object's task is to record the iteration history on demand and to allow queries on the (final) state of the iterations:

```c++
class Observer {
public:
    using State = std::tuple< bool, Coordinate, Coordinate >;
    explicit Observer( bool record = false ); 
    /*...*/
    void update( State );
    /*other public methods*/
    std::vector< State > const& states() const; // results from all recorded iterations
    State const& result() const;                // result from last iteration
    // ...
private:
    bool const record_{ false };
    std::vector< State > states_{};
};
```

The `gradientDescent` function is modified to expect an `Observer` object (by value) which is also returned from the function:

```c++
Observer gradientDescente( /*...*/ Observer observer /*...*/ );
```