// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <iostream>
#include <memory>
#include <numeric>
#include <string>
#include <tuple>

#include "Types.h"

/*
>>>> NOTE        <<<<
For compactness we declare and define all required entities in a single source
file.

Of course, in a real software project, logically-related entities should be kept
in dedicated source and header files.
>>>> END OF NOTE <<<<
*/

/*
----- files: Updater.h / Updater.cpp -----
*/

class CoordinateUpdater
{
  public:
    using ValueType = typename Coordinate::value_type;
    using SizeType = typename Coordinate::size_type;

    virtual ~CoordinateUpdater() noexcept = default;
    virtual void operator()( Coordinate &coords, Coordinate const &grad ) = 0;
};

class VanillaUpdater final : public CoordinateUpdater
{
  public:
    explicit VanillaUpdater( ValueType step_size = 1e-2 )
        : step_size_{ step_size }
    {
    }
    ~VanillaUpdater() noexcept override = default;

    void operator()( Coordinate &coords, Coordinate const &grad ) override
    {
        for ( auto idx{ 0ul }; auto &x : coords )
            x -= step_size_ * grad.at( idx++ );
    }

  private:
    ValueType const step_size_{};
};

class MomentumUpdater final : public CoordinateUpdater
{
  public:
    MomentumUpdater( ValueType alpha, ValueType step_size, SizeType size )
        : alpha_{ alpha }
        , step_size_{ step_size }
        , delta_omega_( size, 0 ) // Initial vector filled with 0s.
    {
    }
    ~MomentumUpdater() noexcept override = default;

    void operator()( Coordinate &coords, Coordinate const &grad ) override
    {
        for ( auto idx = 0ul; auto &d : delta_omega_ )
        {
            d *= alpha_;
            d -= step_size_ * grad.at( idx );
            coords.at( idx++ ) += d;
        }
    }

  private:
    ValueType const alpha_{};
    ValueType const step_size_{};
    Coordinate delta_omega_{};
};

/*
----- files: GradientDescent.h / GradientDescent.cxx
*/
std::pair< bool, Coordinate >
gradientDescent( FunctionGradient const &gradf, Coordinate coords,
                 std::unique_ptr< CoordinateUpdater > updater,
                 double gtol = 1e-3, unsigned long max_iterations = 1000ul )
{

    auto computeNorm2 = []( auto const &g ) -> ValueType {
        return std::inner_product( g.begin(), g.end(), g.begin(),
                                   ValueType( 0 ) );
    };

    Coordinate grad = gradf( coords );
    double g_l2norm2 = computeNorm2( grad );

    bool is_converged = ( g_l2norm2 < gtol * gtol );
    size_t num_iterations = 0ul;

    while ( !is_converged && num_iterations < max_iterations )
    {
        num_iterations++;
        // Perform the coordinate update.
        ( *updater )( coords, grad );
        // Update the gradient and check for convergence.
        grad = gradf( coords );
        g_l2norm2 = computeNorm2( grad );
        is_converged = ( g_l2norm2 < gtol * gtol );
    }

    if ( is_converged )
    {
        std::cout << "Converged after " << num_iterations << " iterations\n";
    }
    else
    {
        std::cout << "Failed to converge after " << max_iterations
                  << " iterations\n";
    }

    return { is_converged, coords };
}

/*
----- files: Main.cxx -----
*/
int main()
{
    // the function to minimize
    auto f = []( Coordinate const &coord ) -> double
    {
        double const x{ coord.at( 0 ) };
        double const y{ coord.at( 1 ) };
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( Coordinate const &coord ) -> Coordinate
    {
        double const x{ coord.at( 0 ) };
        double const y{ coord.at( 1 ) };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    /*
     * Initial coordinates and learning rate.
     */
    Coordinate const starting_pos = { -1.0, 2.50 };
    double const step_size = 3e-1;

    auto printCoordinates = []( auto const &c ) -> void
    {
        std::cout << "Final coordinates: x = " << c.at( 0 )
                  << ", y = " << c.at( 1 ) << "\n";
    };

    {
        std::cout << "\tUsing the Vanilla updater...\n";
        auto const [ _, final_coords ] =
            gradientDescent( grad_f, starting_pos,
                             std::make_unique< VanillaUpdater >( step_size ) );
        printCoordinates( final_coords );
    }

    {
        std::cout << "\tUsing the Momentum updater...\n";
        auto const [ _, final_coords ] =
            gradientDescent( grad_f, starting_pos,
                             std::make_unique< MomentumUpdater >(
                                 2.5e-1 /*alpha*/, step_size, 2ul ) );
        printCoordinates( final_coords );
    }

    return EXIT_SUCCESS;
}
