// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#ifndef PLOTTER_H_INCLUDE_GUARD
#define PLOTTER_H_INCLUDE_GUARD

#include "Types.h"

class Plotter
{
  public:
    virtual void
    plot_gradient_descent( Function f, CoordinateArray const &steps_taken ) = 0;
};

// the Factroy method creates a Plotter and hides all the plotters Internals
#ifndef NO_VISUALIZATION

Plotter *PlotterFactory();

#else
Plotter *PlotterFactory() { return nullptr; }
#endif

#endif