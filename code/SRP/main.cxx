// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <numeric>
#include <vector>

using ValueType = double;
using Coordinate = std::vector< ValueType >;
using Function = std::function< ValueType( Coordinate const & ) >;
using FunctionGradient = std::function< Coordinate( Coordinate const & ) >;

class GradientDescent
{
  public:
    virtual ~GradientDescent() noexcept = default;

    // Getter and setter methods
    virtual Coordinate const &coordinate() const = 0;
    virtual void coordiante( Coordinate const & ) = 0;
    virtual Coordinate gradient() const = 0;

    /*
    The learning rate is common to all GD flavours
    */
    virtual ValueType learningRate() const = 0;
    virtual void learningRate( ValueType ) = 0;

    /*
    The following two parameters are required for momentum GD
    */
    virtual ValueType momentumScaleValue() const = 0;
    virtual void momentumScaleValue( ValueType ) = 0;
    virtual Coordinate const &momentum() const = 0;
    virtual void momentum( Coordinate const & ) = 0;

    /*
    The following parameter is required for AdaGrad
    */
    virtual ValueType divByZeroOffset() const = 0;
    virtual void divByZeroOffset( ValueType ) const = 0;
    virtual Coordinate const &summedSquaredGradient() const = 0;
    virtual void summedSquaredGradient( Coordinate const & ) = 0;

    virtual void run( size_t max_iterations, ValueType gtol ) = 0;

  private:
    virtual ValueType gradientL2Norm( Coordinate const &grad ) const = 0;
};

class VanillaGradientDescent final : public GradientDescent
{
  public:
    ~VanillaGradientDescent() noexcept override = default;
    VanillaGradientDescent( FunctionGradient gradf, ValueType learning_rate,
                            Coordinate const &coord )
        : gradf_{ std::move( gradf ) }
        , learning_rate_{ learning_rate }
        , coordinate_{ coord }
    {
    }

    // Getter and setter methods
    Coordinate const &coordinate() const override { return coordinate_; };
    void coordiante( Coordinate const &coord ) override { coordinate_ = coord; }

    virtual Coordinate gradient() const override
    {
        return gradf_( coordinate_ );
    };

    /*
    The learning rate is common to all GD flavours
    */
    virtual ValueType learningRate() const override { return learning_rate_; };
    virtual void learningRate( ValueType lr ) override { learning_rate_ = lr; };

    /*
    The following two parameters are required for momentum GD
    */
    virtual ValueType momentumScaleValue() const override { return 0; };
    // Implementation left empty
    virtual void momentumScaleValue( ValueType ) override{};
    virtual Coordinate const &momentum() const override
    {
        // This will issue a compiler warning about reference to temporary.
        return Coordinate( coordinate_.size(), 0 );
    };
    // Implementation left empty
    virtual void momentum( Coordinate const & ) override{};

    /*
    The following parameter is required for AdaGrad
    */
    virtual ValueType divByZeroOffset() const override { return 0; };
    // Implementation left empty
    virtual void divByZeroOffset( ValueType ) const override{};
    virtual Coordinate const &summedSquaredGradient() const override
    {
        // This will issue a compiler warning about a reference to temporary.
        return Coordinate( coordinate_.size(), 0 );
    };
    // Implementation left empty
    virtual void summedSquaredGradient( Coordinate const & ) override{};

    void run( size_t max_iterations, ValueType gtol ) override
    {
        Coordinate grad = this->gradient();

        bool is_converged = ( this->gradientL2Norm( grad ) < gtol );
        size_t num_iterations = 0;

        while ( !is_converged && num_iterations < max_iterations )
        {
            num_iterations++;

            // Update the coordinates given the current gradient
            // information.
            for ( size_t idx = 0; idx < coordinate_.size(); ++idx )
            {
                coordinate_.at( idx ) -= learning_rate_ * grad.at( idx );
            }

            // Compute the gradient at the updated position. Also check if
            // the gradient is small enough to stop the iterations.
            grad = this->gradient();
            is_converged = ( this->gradientL2Norm( grad ) < gtol );
        }

        if ( is_converged )
        {
            std::cout << "Converged after " << num_iterations
                      << " iterations\n";
        }
        else
        {
            std::cout << "Failed to converge after " << max_iterations
                      << " iterations\n";
        }
    }

  private:
    FunctionGradient gradf_{};
    ValueType learning_rate_{ 1e-2 };
    Coordinate coordinate_{};

    ValueType gradientL2Norm( Coordinate const &grad ) const override
    {
        return std::sqrt( std::inner_product( grad.begin(), grad.end(),
                                              grad.begin(), ValueType( 0 ) ) );
    }
};

int main()
{
    // the function to minimize
    auto f = []( std::vector< double > coords ) -> double
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( std::vector< double > coords ) -> std::vector< double >
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    Coordinate coord_init{ -1.0, 2.50 };
    constexpr ValueType learning_rate{ 3e-1 };
    constexpr ValueType gtol{ 1e-3 };
    constexpr unsigned long max_iterations{ 1000ul };

    auto gd{ std::make_unique< VanillaGradientDescent >( grad_f, learning_rate,
                                                         coord_init ) };

    gd->run( max_iterations, gtol );

    return EXIT_SUCCESS;
}
