# Code Examples

This directory contains the code examples discussed during the course. The starting point of the discussion is the code example inside [`gradient_descent_original`](./gradient_descent_original). As the course progresses, we will progressively update this example to improve code quality and particular its testability.

Below you can find a short summaryu of the content of each of the directories prefixed with `gradient_descent_*`. Please refer to the README files *inside* the single subdirectories for detailed information.

* [`gradient_descent_original`](./gradient_descent_original): Initial version of the code.
* [`gradient_descent_v01`](./gradient_descent_v01) Same as original but with blackbox test(s) via shell script.
* [`gradient_descent_v02`](./gradient_descent_v02) Moved the driver code in to a `main.cxx` file. Code for GD is in extra source and header file.
* [`gradient_descent_v03`](./gradient_descent_v03) Moved type aliases and plotting code into dedicated files. Plotting code has been removed from vanilla GD implementation (still left in momentum code).
* [`gradient_descent_v04`](./gradient_descent_v04) Also removed plotting code from momentum GD.
* [`gradient_descent_v05`](./gradient_descent_v05) Introduce coordinate updater abstraction.
* [`gradient_descent_v06`](./gradient_descent_v06) Add some basic tests for vanilla GD.
* [`gradient_descent_v07`](./gradient_descent_v07) Refactoring of vanilla tests and add skeleton testing momentum GD.
* [`gradient_descent_v08`](./gradient_descent_v08) Adds tests for momentum GD.
* [`gradient_descent_v09`](./gradient_descent_v09) Consolidate tests to have less code duplication.
* [`gradient_descent_v10`](./gradient_descent_v10) Included AdaGrad GD with tests.