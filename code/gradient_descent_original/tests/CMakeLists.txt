
add_executable(Test Tests.cpp)

target_link_libraries( Test PRIVATE Catch2::Catch2WithMain
        GradientDescent_lib)
target_include_directories(Test PRIVATE ..)

add_test(
        NAME Test
        COMMAND Test
)