

#include "catch2/generators/catch_generators.hpp"
#include <catch2/catch_test_macros.hpp>

#include "GradientDescent.h"
#include "Types.h"
#include "Updater.h"

constexpr double dmacheps{ std::numeric_limits< double >::epsilon() };
bool isClose( double value, // Value to be tested for closeness
              double ref,
              // Reference value
              double atol = 10 * dmacheps, double rtol = 1e-8 )
{
    return ( std::abs( ref - value ) < atol + rtol * std::abs( ref ) );
}

bool coordinatesAreClose( Coordinate const &c, Coordinate const &c_ref,
                          double atol = 10.0 * dmacheps, double rtol = 1e-8 )
{
    return ( isClose( c[ 0 ], c_ref[ 0 ], atol, rtol ) &&
             isClose( c[ 1 ], c_ref[ 1 ], atol, rtol ) );
}

// test functions:
auto f_1 = []( std::vector< double > coords ) -> double
{
    double const x = coords[ 0 ];
    double const y = coords[ 1 ];
    // 2 x y + 2 x - x^2 - 2 y^2
    return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
};
// the derivative function
auto grad_f_1 = []( std::vector< double > coords ) -> std::vector< double >
{
    double const x{ coords[ 0 ] };
    double const y{ coords[ 1 ] };
    return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
};

// the function to minimize
Function f_2{ []( Coordinate const &c ) -> double {
    return ( c[ 0 ] - 2 ) * ( c[ 0 ] - 2 ) + ( c[ 1 ] - 4 ) * ( c[ 1 ] - 4 );
} };
FunctionGradient grad_f_2{ []( Coordinate const &c ) -> Coordinate {
    return { 2 * ( c[ 0 ] - 2 ), 4 * ( c[ 1 ] - 4 ) };
} };

/*
Group of tests that "belong together".
*/
TEST_CASE( "Test Vanilla Updater" )
{
    Coordinate starting_pos = { 0, 0 };
    Coordinate current_position = starting_pos;
    auto grad_base = GENERATE( as< Coordinate >(), Coordinate{ -1, -1 },
                               Coordinate{ 0, -1 }, Coordinate{ -1, 0 } );
    auto gradient_magnitude = GENERATE( as< double >(), 1, 2, 3 );
    Coordinate grad = Coordinate{ grad_base[ 0 ] * gradient_magnitude,
                                  grad_base[ 1 ] * gradient_magnitude };
    Coordinate expected_pos = Coordinate{ grad[ 0 ] * -1, grad[ 1 ] * -1 };

    SECTION( "Step win a given direction" )
    {
        double step_size = GENERATE( as< double >(), 1.0 );

        auto updater = std::make_unique< VanillaUpdater >( step_size );
        for ( int i = 0; i < 100; ++i )
        {
            current_position = starting_pos;
            updater->update_coordinates( current_position, grad );
            REQUIRE( coordinatesAreClose( current_position, expected_pos ) );
            // REQUIRE(not coordinatesAreClose( current_position, starting_pos )
            // );
        }
    }
    SECTION( "Step win 0 step_size" )
    {
        double step_size = 0;
        auto updater = std::make_unique< VanillaUpdater >( step_size );
        for ( int i = 0; i < 100; ++i )
        {
            current_position = starting_pos;
            updater->update_coordinates( current_position, grad );
            REQUIRE( coordinatesAreClose( current_position, starting_pos ) );
        }
    }
    // section2 -...
}

void test_gradient_decent( std::unique_ptr< CoordinateUpdater > updater, auto f,
                           auto grad_f, const Coordinate &expected_minimum )
{
    Coordinate position = { -1.0, 2.50 };

    CoordinateArray steps_taken =
        run_gradient_decent( f, grad_f, position, std::move( updater ) );

    size_t number_of_steps = steps_taken.size() - 1;
    REQUIRE( number_of_steps >= 1 );
    REQUIRE( coordinatesAreClose( position, expected_minimum, 1e-8, 1e-3 ) );
}

void test_gradient_decent_implementation_against_each_other(
    std::unique_ptr< CoordinateUpdater > updater1,
    std::unique_ptr< CoordinateUpdater > updater2, auto f, auto grad_f )
{
    Coordinate position_1 = { -1.0, 2.50 };
    Coordinate position_2 = { -1.0, 2.50 };

    CoordinateArray steps_taken_1 =
        run_gradient_decent( f, grad_f, position_1, std::move( updater1 ) );

    CoordinateArray steps_taken_2 =
        run_gradient_decent( f, grad_f, position_2, std::move( updater2 ) );

    REQUIRE( coordinatesAreClose( position_1, position_2, 1e-8, 1e-3 ) );
}

void test_find_minimum( std::unique_ptr< CoordinateUpdater > updater )
{

    SECTION( "Function 1" )
    {
        test_gradient_decent( std::move( updater ), f_1, grad_f_1,
                              { 2.0, 1.0 } );
    }
    SECTION( "Function 2" )
    {
        test_gradient_decent( std::move( updater ), f_2, grad_f_2,
                              { 2.0, 4.0 } );
    }
}

TEST_CASE( "Gradient Decent Vanilla" )
{
    double step_size = 3e-1;
    auto updater = std::make_unique< VanillaUpdater >( step_size );
    test_find_minimum( std::move( updater ) );
}

TEST_CASE( "Gradient Decent Momentum" )
{
    double step_size = 3e-1;
    auto updater = std::make_unique< MomentumUpdater >( 0.1, step_size, 2 );
    test_find_minimum( std::move( updater ) );
}

TEST_CASE( "Momentum VS Vanilla" )
{
    double step_size = 3e-1;
    auto updater_momentum =
        std::make_unique< MomentumUpdater >( 0.1, step_size, 2 );
    auto updater_vanilla = std::make_unique< VanillaUpdater >( step_size );
    test_gradient_decent_implementation_against_each_other(
        std::move( updater_vanilla ), std::move( updater_momentum ), f_1, grad_f_1 );
}

TEST_CASE( "Test Momentum Updater" )
{

    Coordinate starting_pos = { 0, 0 };
    Coordinate current_position = starting_pos;
    auto grad_base = GENERATE( as< Coordinate >(), Coordinate{ -1, -1 },
                               Coordinate{ 0, -1 }, Coordinate{ -1, 0 } );
    auto gradient_magnitude = GENERATE( as< double >(), 1, 2, 3 );
    Coordinate grad = Coordinate{ grad_base[ 0 ] * gradient_magnitude,
                                  grad_base[ 1 ] * gradient_magnitude };
    Coordinate expected_pos = Coordinate{ grad[ 0 ] * -1, grad[ 1 ] * -1 };

    SECTION( "Step win a given direction : Test that it builds momentum" )
    {
        double step_size = 1.0;
        auto updater = std::make_unique< VanillaUpdater >( step_size );
        for ( int i = 0; i < 100; ++i )
        {
            updater->update_coordinates( current_position, grad );
        }
        // Reverse gradient direction
        Coordinate prev_pos = current_position;
        Coordinate reversed_grad = { grad[ 0 ] * -1, grad[ 1 ] * -1 };
        updater->update_coordinates( current_position, grad );
        // assert that the momentum has build up: meaning we still travel in
        // original direction original direction was positive (or 0)
        REQUIRE( current_position[ 0 ] >= prev_pos[ 0 ] );
        REQUIRE( current_position[ 1 ] >= prev_pos[ 1 ] );
    }
    SECTION( "Step win 0 step_size" )
    {
        double step_size = 0;
        auto updater = std::make_unique< VanillaUpdater >( step_size );
        for ( int i = 0; i < 100; ++i )
        {
            current_position = starting_pos;
            updater->update_coordinates( current_position, grad );
            REQUIRE( coordinatesAreClose( current_position, starting_pos ) );
        }
    }
}

TEST_CASE( "Test Implementations against each other" )
{

    Coordinate position_vanilla = { -1.0, 2.50 };
    Coordinate position_momentum = { -1.0, 2.50 };
    double step_size = 3e-1;

    auto updater_vanilla = std::make_unique< VanillaUpdater >( step_size );
    auto updater_momentum = std::make_unique< MomentumUpdater >(
        0.1, step_size, position_momentum.size() );

    CoordinateArray steps_taken_vanilla = run_gradient_decent(
        f_1, grad_f_1, position_vanilla, std::move( updater_vanilla ) );

    CoordinateArray steps_taken_momentum = run_gradient_decent(
        f_1, grad_f_1, position_momentum, std::move( updater_momentum ) );

    REQUIRE( coordinatesAreClose( position_vanilla, position_momentum, 1e-8,
                                  1e-3 ) );
}

TEST_CASE( "Test Implementations against each other function 2" )
{
    Coordinate position_vanilla = { -1.0, 2.50 };
    Coordinate position_momentum = { -1.0, 2.50 };
    double step_size = 3e-1;

    auto updater_vanilla = std::make_unique< VanillaUpdater >( step_size );
    auto updater_momentum = std::make_unique< MomentumUpdater >(
        0.1, step_size, position_momentum.size() );

    CoordinateArray steps_taken_vanilla = run_gradient_decent(
        f_2, grad_f_2, position_vanilla, std::move( updater_vanilla ) );

    CoordinateArray steps_taken_momentum = run_gradient_decent(
        f_2, grad_f_2, position_momentum, std::move( updater_momentum ) );

    REQUIRE( coordinatesAreClose( position_vanilla, position_momentum, 1e-8,
                                  1e-3 ) );
}