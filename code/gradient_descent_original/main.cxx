// clang-format off
// SPDX-FileCopyrightText: 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
// clang-format on
//
// SPDX-License-Identifier: MIT

#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "GradientDescent.h"
#include "Plotter.h"
#include "Types.h"
#include "Updater.h"

int main( int argc, char *argv[] )
{
    // the function to minimize
    auto f = []( std::vector< double > coords ) -> double
    {
        double const x = coords[ 0 ];
        double const y = coords[ 1 ];
        // 2 x y + 2 x - x^2 - 2 y^2
        return -( 2 * x * y + 2 * x - x * x - 2 * y * y );
    };
    // the derivative function
    auto grad_f = []( std::vector< double > coords ) -> std::vector< double >
    {
        double const x{ coords[ 0 ] };
        double const y{ coords[ 1 ] };
        return { -( 2 * y + 2 - 2 * x ), -( 2 * x - 4 * y ) };
    };

    Coordinate position = { -1.0, 2.50 };
    double step_size = 3e-1;

    // use command line argument to decide which version of gradient decent to
    // use
    int implementation_to_use = 0;
    if ( argc > 1 )
    {
        implementation_to_use = std::stoi( argv[ 1 ] );
    }

    std::unique_ptr< CoordinateUpdater > updater;
    switch ( implementation_to_use )
    {
    case 1:
        updater = std::make_unique< MomentumUpdater >( .25, step_size, 2 );
        break;

    case 0:
    default:
        updater = std::make_unique< VanillaUpdater >( step_size );
        break;
    }

    // we "move" the updater into the gradient decent function
    // meaning that the gradient decent function now has ownership
    // as the updater may hold some internal state, we can not use it again
    auto steps_taken =
        run_gradient_decent( f, grad_f, position, std::move( updater ) );
    assert( updater == nullptr ); // as we used std::move, we lost ownership

    std::cout << "Final coordinates: x = " << position[ 0 ]
              << ", y = " << position[ 1 ] << "\n";

    auto plotter = PlotterFactory();

    if ( plotter != nullptr )
    {
        plotter->plot_gradient_decent( f, steps_taken );
    }

    return 0;
}
