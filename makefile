# SPDX-FileCopyrightText: 2023 HPC Core Facility of the Justus-Liebig-University Giessen <marcel.giar@physik.jlug.de>
# SPDX-FileCopyrightText: © 2023 Competence Center for High Performance Computing in Hessen (HKHLR) <staff@hpc-hessen.de>
# 
# SPDX-License-Identifier: CC-BY-4.0

all:

SOURCE_DIRECTORY := ./code

REUSE_CMD        := conda run -n reuse reuse
CLANG_FORMAT_CMD := conda run --no-capture-output -n llvm-tools clang-format 

clang_format:
	find $(SOURCE_DIRECTORY) -type f -not -path "*build*" \
		\( -name "*.cxx" -o -name "*.h" \) \
		-exec $(CLANG_FORMAT_CMD) -i {} \; -print

licenses:
	$(MAKE) -f makefile.licenses $(TARGET)
