# Practical Introduction to Scientific Software Engineering and Design

## About this course

This course is part of the annual [HiPerCH workshop](https://www.hkhlr.de/en/events/hiperch-15-2023-09-11) offered by the Competence Center for High Performance Computing in Hessen ([HKHLR](https://www.hkhlr.de/)). For a detailed course description please refer to the [course website](https://www.hkhlr.de/en/articles/hiperch-15-module-2).

## Course material

### Getting a copy

The course material can be obtained by cloning this repository. E.g., from within a terminal emulator of your choice execute:

```shell
$ git clone https://git.rwth-aachen.de/hkhlr/practical-introduction-to-scientific-software-engineering-and-design.git
```

Please note that you need to have Git installed. This can e.g. be done via `sudo apt-get install git` on Debian-based Linux distros or `brew install git` on macOS with [Homebrew](https://brew.sh/) installed. Git for Windows can be obtained [here](https://git-scm.com/downloads).

Alternatively, you can download the repository as an archive file (`.zip`, `.tar.gz`, `tar.bz2`, or `.tar`) by clicking on the download button, immediately to the left of the 'Clone' button. When clicking the button you will also be offered several options to clone this repository from an IDE. Please refer to the options below 'Open in your IDE'.

### Content

The course content is comprised of slides (in PDF format), text files (in Markdown format) as well as source code examples.

* Slides are located in the [slides](./slides) directory.
* Code examples are located in the [code](./code) directory.

## Code examples

### Detailed information

For details on the code examples please refer to this [README](./code/README.md) file.

### Building

#### Requirements

In order to build all code samples run you will need 

* the build-system generator tool CMake (at least version 3.11), and 
* a recent C++ compiler that that supports at least C++17.
    * We have tested GCC v11, and GCC v12.

To visualise a path taken by the gradient descent algorithm we need a Python environment with `pybind11` and `matplotlib`. Refer to the [section below](#python-installation) for installation instructions.

#### Python installation

We assume that you have a Anaconda installation available and that the `conda` installation utility is on the `PATH`. There are several options:

* The [Anaconda Python](https://www.anaconda.com/products/distribution#Downloads) distribution.
* The [Miniconda Python](https://docs.conda.io/en/latest/miniconda.html) distribution.

Please download and [install](https://docs.anaconda.com/anaconda/install/) the distribution for your particular architecture and plattform.

The environment (with name `cpp-plotting`) can be installed with the command

```shell
$ conda env create --file environment.yml 
```

where the [`environment.yml`](./environment.yml) file can be found at this project's root directory. Afterwards activate the environment by typing `conda activate cpp-plotting` (or `source activate cpp-plotting` in case you shell is not activated).

#### Building the code

The following commands will configure a build system and build all relevant sources contained in the project.

*> Note*: Please make sure to activate the Python environment as described in the [previous section](#python-installation).

1. Navigate to the project root and create a build directory (make sure that you are in the directory containing the top-level [`CMakeLists.txt`](./CMakeLists.txt) file).

```bash
$ cmake -B build -DPython3_EXECUTABLE=$(which python3)
```

2. Build all sources:
```bash
$ cmake --build build # --parallel <NCPUS> # to build in parallel with <NCPUS> cores
```

To have a more fine-grained control over which target to actually build consider using an IDE that supports CMake.

## Licensing

Please refer to the [LICENSE](./LICENSE.md) file for details on the licenses at use.
